<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Composer\Command;

use Composer\Command\BaseCommand;
use Composer\IO\ConsoleIO;
use Raini\Core\Composer\RainiPlugin;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Composer command to generate or refresh environment and system files.
 *
 * Raini uses composer to pull in dependencies, provide an autoloader and
 * provide information about installed packages. Because of this dependency,
 * Raini needs information available from Composer to be available when run
 * from as a standalone console application.
 *
 * This command generates or refreshes the ".raini.env" and
 *  "<raini directory>/extensions.yml" with the package and environment
 * information from Composer.
 */
class RainiRebuild extends BaseCommand
{

    /**
     * {@inheritdoc}
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $composer = $this->requireComposer();

        $io = new ConsoleIO($input, $output, $this->getHelperSet());
        $plugin = new RainiPlugin();
        $plugin->rebuildSysFiles($composer, $io);

        $output->writeln('<info>Raini environment and setup files have been updated!</info>');

        return 0;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $helpText = <<<EOT
            Command to generate or refresh Raini environment variables, and Raini extension
            information. This data include Raini packages, installation paths and the
            extension provider class.
            EOT;

        $this
            ->setName('raini-rebuild')
            ->setDescription('Command to generate or refresh Raini environment and system files.')
            ->setHelp($helpText);
    }
}
