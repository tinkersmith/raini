<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Composer;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Factory as ComposerFactory;
use Composer\IO\IOInterface;
use Composer\Plugin\Capability\CommandProvider as CommandProviderInterface;
use Composer\Plugin\Capable;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use Raini\Core\Application;
use Raini\Core\Environment;
use Raini\Core\Extension\Exception\ExtensionNotFoundException;
use Raini\Core\Extension\ExtensionDefinition;
use Raini\Core\Settings;
use Raini\Core\Utility\EnvValues;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

/**
 * The Raini Composer plugin.
 *
 * Registers custom Raini Composer commands, and listens for composer events.
 */
class RainiPlugin implements PluginInterface, Capable, EventSubscriberInterface
{

    /**
     * The filesystem object.
     *
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    protected $fs;

    /**
     * An array of information about Raini extensions installed and available.
     *
     * Array has indexes "droplet" and "extension" under which it lists
     * information about Raini droplet and extensions keyed by their package
     * names.
     *
     * Each package has the installed directory, package version, and a boolean
     * to indication if a package has services to register (package has a
     * "raini.services.yml"). Droplets will also specify the extension provider
     * class which implements the \Raini\Extension\DropletInterface
     * interface.
     *
     * @var mixed[]|null
     */
    protected $extensions;

    /**
     * Constructor for the Raini Composer plugin.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritdoc}
     */
    public function getCapabilities()
    {
        return [CommandProviderInterface::class => CommandProvider::class];
    }

    /**
     * {@inheritdoc}
     */
    public function activate(Composer $composer, IOInterface $io)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function deactivate(Composer $composer, IOInterface $io)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function uninstall(Composer $composer, IOInterface $io)
    {
        // @todo clear the cache and remove Raini resources.
        $isClean = $io->askConfirmation('Remove all Raini generated project files?', false);

        if ($isClean) {
            $projectDir = $this->getProjectDir($composer);
            $this->fs->remove(Environment::getEnvFilePath($projectDir));
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            ScriptEvents::POST_INSTALL_CMD => [
                ['onProjectInstall', -10],
                ['onPackagesChanged', 100],
            ],
            ScriptEvents::POST_UPDATE_CMD => [
                ['onPackagesChanged', 100],
            ],
        ];
    }

    /**
     * Post-install event handler to run project installers.
     *
     * Project installers can be defined by Raini Droplets and are intended to
     * ensure project is setup. Installers should be used when Composer context
     * is needed or to perform preparations needed before any Raini commands
     * from this project are used.
     *
     * @param Event $event The Composer post-install event.
     */
    public function onProjectInstall(Event $event): void
    {
        $composer = $event->getComposer();
        $io = $event->getIO();
        $projectDir = $this->getProjectDir($composer);
        $environment = new Environment($projectDir);
        $settings = new Settings($environment->getRainiPath());

        // If a project has not been configured with a Droplet then exit.
        if (!($dropletName = $settings->getType())) {
            return;
        }

        try {
            $extensions = $this->getExtensions($composer);
            $droplet = $extensions['droplet'][$dropletName] ?? null;

            if (!$droplet) {
                throw new ExtensionNotFoundException($dropletName);
            }

            if (!empty($droplet['extra']['installer'])) {
                $installerClass = $droplet['extra']['installer'];
                $definition = new ExtensionDefinition($this->buildExtensionDefinition($droplet, $projectDir));

                // Ensure namespaces and classmap is loaded for the Droplet so
                // the installer class and other required object can be loaded.
                $autoloadGenerator = $composer->getAutoloadGenerator();
                $autoloads = $autoloadGenerator->parseAutoloads([
                    [$droplet['package'], $droplet['path']],
                ], $composer->getPackage());

                $autoloadGenerator->createLoader($autoloads)->register();

                /** @var InstallerInterface $installer */
                $installer = new $installerClass();
                $installer->install($definition, $settings, $composer, $io);
            }
        } catch (ExtensionNotFoundException $e) {
            $io->writeError($e->getMessage());
        }
    }

    /**
     * Composer script event handler for when packages are changed or updated.
     *
     * When Composer packages are updated Raini needs to update the extensions
     * information and flush application caches.
     *
     * @param Event $event Either a Compoesr post-install or post-update event.
     */
    public function onPackagesChanged(Event $event): void
    {
        $composer = $event->getComposer();
        $io = $event->getIO();

        $this->rebuildSysFiles($composer, $io);
    }

    /**
     * Rebuild the Raini configuration files and flush any application caches.
     *
     * @param Composer    $composer The Composer instance.
     * @param IOInterface $io       The IO for messages and user input.
     */
    public function rebuildSysFiles(Composer $composer, IOInterface $io): void
    {
        $projectDir = $this->getProjectDir($composer);
        $env = $this->loadEnv($projectDir, $composer);

        $env->write(Environment::getEnvFilePath($projectDir), <<<EOT
            Environment variables for the Raini CLI.

            This file is autogenerated and will get updated when Composer packages are
            updated, installed or removed. You can force a refresh of this file and
            Raini extensions data by running "composer raini-rebuild".

            It is safe to add variables as needed, but do not remove variables!
            EOT
        );

        // Set the Raini resources folder.
        $rainiDir = $projectDir.\DIRECTORY_SEPARATOR.($env[Environment::RAINI_DIR] ?? Environment::DEFAULT_RAINI_PATH);
        $this->fs->mkdir($rainiDir);

        // Write the list of Composer packages which are Raini extensions.
        $definitions = [];
        foreach ($this->getExtensions($composer) as $type => $packages) {
            foreach ($packages as $package) {
                $extensionId = $package['extra']['id'] ?? $package['name'];
                $definitions[$type][$extensionId] = $this->buildExtensionDefinition($package, $projectDir);
            }
        }

        // Write the extension info to a file for the CLI application to use.
        $extFilepath = $rainiDir.\DIRECTORY_SEPARATOR.'extensions.yml';
        $this->fs->dumpFile($extFilepath, Yaml::dump($definitions, 3, 2, YAML::DUMP_MULTI_LINE_LITERAL_BLOCK));
    }

    /**
     * Get the base project directory based on Composer settings.
     *
     * @param Composer $composer The composer instance.
     *
     * @return string The path of the base project directory.
     */
    protected function getProjectDir(Composer $composer): string
    {
        $composerFile = realpath(ComposerFactory::getComposerFile());
        $composerDir = dirname($composerFile);
        $extra = $composer->getPackage()->getExtra();

        // Prefer to use the project directory indicated in composer.json.
        return !empty($extra['raini']['project-dir'])
            ? realpath($composerDir.'/'.$extra['raini']['project-dir'])
            : $composerDir;
    }

    /**
     * Load and ensure system values for the Raini env file contents.
     *
     * Method will load the current values of the .raini.env file, while also
     * ensuring that Composer and Application defined values are updated. This
     * means that Composer vendor, bin and composer filepath is always populated
     * with values defined by Composer.
     *
     * @param string   $projectDir The path to the project's base directory.
     * @param Composer $composer   The Composer instance.
     *
     * @return EnvValues Values loaded and updated for the .raini.env file.
     */
    protected function loadEnv(string $projectDir, Composer $composer): EnvValues
    {
        $config = $composer->getConfig();
        $envFile = Environment::getEnvFilePath($projectDir);
        $extra = $composer->getPackage()->getExtra();

        if (file_exists($envFile)) {
            try {
                $values = EnvValues::createFromFile($envFile);
            } catch (IOExceptionInterface $e) {
              // Default just generated a new file.
            }
        }

        if (!isset($values)) {
            $values = new EnvValues();
        }

        // Force these values from Composer, because these values need to match
        // the current Composer configurations to work properly. All directories
        // are relative to the project base directory.
        $values[Environment::VERSION] = Application::VERSION;
        $values[Environment::COMPOSER_FILE] = $this->fs->makePathRelative(realpath(ComposerFactory::getComposerFile()), $projectDir);
        $values[Environment::VENDOR_DIR] = $this->fs->makePathRelative($config->get('vendor-dir'), $projectDir);
        $values[Environment::BIN_DIR] = $this->fs->makePathRelative($config->get('bin-dir'), $projectDir);

        // Remove the leading directory separator from the directory env vars.
        foreach ([Environment::COMPOSER_FILE, Environment::VENDOR_DIR, Environment::BIN_DIR] as $key) {
            $values[$key] = rtrim($values[$key], ' '.\DIRECTORY_SEPARATOR);
        }

        // If defined in the composer.json force the Raini resource directory.
        if (!empty($extra['raini']['raini-dir'])) {
            $values[Environment::RAINI_DIR] = rtrim($extra['raini']['raini-dir'], ' '.\DIRECTORY_SEPARATOR);
        }

        return $values;
    }

    /**
     * Build and report information about Raini extensions currently available.
     *
     * Method checks the local Composer repository for Raini extension packages
     * ('raini-droplet' or 'raini-particle) and collects information about the
     * package, where it's installed, and if it has a services.yml file.
     *
     * @param Composer $composer The Composer instance.
     *
     * @return mixed[] The package information for Raini extensions under the "droplet" and "extension" keys for
     *                 "raini-droplet" and "raini-extension" extension types respectively.
     *
     * @see RainiPlugin::$extensions
     */
    protected function getExtensions(Composer $composer): array
    {
        if (!isset($this->extensions)) {
            $this->extensions = [
                'droplet' => [],
                'extension' => [],
            ];

            $installManager = $composer->getInstallationManager();
            $packages = $composer
                ->getRepositoryManager()
                ->getLocalRepository()
                ->getCanonicalPackages();

            // Find package types which are Raini extensions.
            foreach ($packages as $name => $package) {
                if (preg_match('/raini-(droplet|extension)/', $package->getType(), $matches)) {
                    $info = [
                        'name' => $package->getName(),
                        'package' => $package,
                        'path' => $installManager->getInstallPath($package),
                        'extra' => $package->getExtra(),
                    ];

                    $extType = $matches[1];
                    $extId = $info['extra']['id'] ?? $info['name'];
                    $this->extensions[$extType][$extId] = $info;
                }
            }
        }

        return $this->extensions;
    }

    /**
     * Creates an extension definition from the Composer package information.
     *
     * @param mixed[] $package    The composer package definition.
     * @param string  $projectDir The project directory path.
     *
     * @return mixed[] The processed extension definition.
     */
    protected function buildExtensionDefinition(array $package, string $projectDir): array
    {
        $extensionPath = $this->fs->makePathRelative($package['path'], $projectDir);
        $definition = [
            'id' => $package['extra']['id'] ?? $package['name'],
            'name' => $package['extra']['name'] ?? $package['name'],
            'path' => rtrim(strtr($extensionPath, \DIRECTORY_SEPARATOR, '/'), '/'),
        ];

        if (!empty($package['extra']['class'])) {
            $definition['class'] = $package['extra']['class'];
        }

        return $definition;
    }
}
