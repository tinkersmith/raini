<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Raini\Core\Extension\ExtensionDefinitionInterface;
use Raini\Core\Settings;

/**
 * Interface for installer extensions to run during Composer post-install.
 *
 * Installers are a way for a Raini extensions to run setup code during the
 * Composer post-install event, after all the packages have been installed. This
 * allows an extension to scaffold files, or prepare the project in the Composer
 * context and needed before needing to run any Raini commands.
 */
interface InstallerInterface
{

    /**
     * Install method to call when the Composer post-install event is triggered.
     *
     * @param ExtensionDefinitionInterface $extensionDef The Droplet extension definition.
     * @param Settings                     $settings     The current Raini project settings (raini.project.yml).
     * @param Composer                     $composer     The Composer instance.
     * @param IOInterface                  $io           The IO to present prompts, output messages and get user input.
     */
    public function install(ExtensionDefinitionInterface $extensionDef, Settings $settings, Composer $composer, IOInterface $io): void;
}
