<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Composer;

use Composer\Plugin\Capability\CommandProvider as CommandProviderInterface;
use Raini\Core\Composer\Command\RainiRebuild;

/**
 * Provides commands the Raini package adds to Composer.
 */
class CommandProvider implements CommandProviderInterface
{

    /**
     * {@inheritdoc}
     */
    public function getCommands()
    {
        return [
            new RainiRebuild(),
        ];
    }
}
