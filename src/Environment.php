<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core;

use Raini\Core\Utility\EnvValues;

/**
 * Class for objects which maintain the Raini environment variables.
 *
 * Environment variables are used Raini settings and information that is used
 * in both the Raini application and useful for shell scripts to invoke the
 * application.
 *
 * The environment is responsible for keeping track of:
 *  - The project path.
 *  - The Raini files directory.
 *  - The Composer bin and vendor directory.
 *
 * The environment file is generated during Composer install, activation of
 * the Raini Composer plugin and when running "composer raini-rebuild".
 */
class Environment
{

    // Path to the project environment variables file (relative to project).
    public const ENV_FILE_PATH = '.raini.env';
    public const DEFAULT_RAINI_PATH = '.raini';

    // Define env variable names for each of the required Raini variables.
    public const VERSION = 'RAINI_VERSION';
    public const RAINI_DIR = 'RAINI_DIR';
    public const COMPOSER_FILE = 'COMPOSER_FILE';
    public const BIN_DIR = 'COMPOSER_BIN_DIR';
    public const VENDOR_DIR = 'COMPOSER_VENDOR_DIR';

    /**
     * Environment variables loaded from the Raini env file.
     *
     * @var \Raini\Core\Utility\EnvValues
     */
    protected EnvValues $envData;

    /**
     * Creates a new instance of the Environment variables.
     *
     * @param string $projectDir The path to the project directory.
     */
    public function __construct(protected string $projectDir)
    {
        $envFile = self::getEnvFilePath($projectDir);
        $this->envData = EnvValues::createFromFile($envFile);
    }

    /**
     * Get the location of the project environment variables file.
     *
     * This file is also a file that marks the project directory. This file
     * should always be placed at the base of the project.
     *
     * @param string $projectDir The project directory.
     *
     * @return string The path where the environment variable would be for the project path.
     */
    public static function getEnvFilePath(string $projectDir): string
    {
        return $projectDir.'/'.self::ENV_FILE_PATH;
    }

    /**
     * Get the path to the project directory.
     *
     * The project directory is what all paths are relative to, unless they
     * are absolute paths (start with "/"),
     *
     * @return string The base path of the project.
     */
    public function getProjectDir(): string
    {
        return $this->projectDir;
    }

    /**
     * Get the path to the Raini directory (relative to project path).
     *
     * Raini directory is where project code, custom/override commands and
     * cache is stored.
     *
     * @return string Get the path for Raini files and cache storage.
     */
    public function getRainiPath(): string
    {
        return $this->envData[self::RAINI_DIR] ?? self::DEFAULT_RAINI_PATH;
    }

    /**
     * Get the path to the Composer bin directory (relative to project path).
     *
     * @return string Get the path for the Composer bin directory.
     */
    public function getBinPath(): string
    {
        return $this->envData[self::BIN_DIR] ?? 'vendor/bin';
    }

    /**
     * Get the path to the Composer vendor directory (relative to project path).
     *
     * @return string Get the path for the Composer vendor directory.
     */
    public function getVendorDir(): string
    {
        return $this->envData[self::VENDOR_DIR] ?? 'vendor';
    }

    /**
     * Get values from the environment variables file by variable name.
     *
     * @param string $name The environment name to fetch the value for.
     *
     * @return mixed|null Gets the environment variable matching the providered key, or NULL if no value was available.
     */
    public function get(string $name)
    {
        return $this->envData[$name] ?? null;
    }
}
