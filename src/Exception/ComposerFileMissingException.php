<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Exception;

use Raini\Core\Project\Tenant;

/**
 * Requested tenant definition is not available with the current project setup.
 */
class ComposerFileMissingException extends SystemFileNotFoundException
{

    /**
     * @param Tenant          $tenant Name of the site being requested.
     * @param int             $code   An error code value.
     * @param \Throwable|null $prev   A previous exception to be used if chaining exceptions.
     */
    public function __construct(Tenant $tenant, int $code = 0, ?\Throwable $prev = null)
    {
        $name = $tenant->getName();
        $message = "Requested site '{$name}' is missing a composer.json file.";

        parent::__construct($message, $code, $prev);
    }
}
