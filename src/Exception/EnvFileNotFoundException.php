<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Exception;

/**
 * Exception for when project is unable to find an essential start-up file.
 */
class EnvFileNotFoundException extends SystemFileNotFoundException
{

    /**
     * @param string          $file The file searched for and could not be located.
     * @param string|null     $path The path searched for the file.
     * @param int             $code A number to represent the error code.
     * @param \Throwable|null $prev Previous exception if chaining exceptions.
     */
    public function __construct(string $file, ?string $path = null, int $code = 0, ?\Throwable $prev = null)
    {
        $path = $path ?? 'the project base';
        $message = sprintf('Unable to locate project file: %s at %s.', $file, $path);

        parent::__construct($message, $code, $prev);
    }
}
