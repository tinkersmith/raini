<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Exception;

/**
 * Exception for when the extensions.yml file can be located.
 */
class ExtensionFileNotFoundException extends SystemFileNotFoundException
{

    /**
     * @param string|null     $path The path searched for the file.
     * @param int             $code A number to represent the error code.
     * @param \Throwable|null $prev Previous exception if chaining exceptions.
     */
    public function __construct(?string $path = null, int $code = 0, ?\Throwable $prev = null)
    {
        $path = $path ?? 'the Raini directory';
        $message = sprintf('Unable to locate the extensions.yml file in the %s directory.', $path);

        parent::__construct($message, $code, $prev);
    }
}
