<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Raini\Core\DependencyInjection\ServiceInspectionInterface;
use Raini\Core\Project\Exception\BuildTaskException;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Interface for implementing project build task.
 *
 * Project build tasks registered to the project.builder service and executed
 * during the project:build command. When containerized dependency injection is
 * being used, builder tasks are registered as tagged services with the tag
 * "build_task".
 *
 * @see \Raini\Core\Project\ProjectBuilder
 */
interface BuildTaskInterface extends ServiceInspectionInterface
{

    /**
     * Gets the build task display name.
     *
     * The build task label is used when listing build tasks for the project
     * build command help text, and during sign posting of the commands as
     * the are being execute during the command execution.
     *
     * @return string The build task label.
     */
    public function label(): string;

    /**
     * Indicates if this build task is configured and available to be run.
     *
     * @param Tenant $tenant The tenant being built.
     *
     * @return bool Is this build task enabled to run by default.
     */
    public function enabled(Tenant $tenant): bool;

    /**
     * Executes this project build task.
     *
     * @param Tenant               $tenant  The tenant being built.
     * @param BuildOptions         $options The options to apply when running this build task.
     * @param OutputInterface|null $output  The terminal or stream to write task output to.
     *
     * @throws BuildTaskException
     */
    public function run(Tenant $tenant, BuildOptions $options, ?OutputInterface $output = null): void;
}
