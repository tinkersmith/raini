<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project\BuildTask;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\RuntimeException;
use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Project\BuildOptions;
use Raini\Core\Project\BuildTaskInterface;
use Raini\Core\Project\Exception\BuildTaskIncompleteException;
use Raini\Core\Project\NodeBuildTenantInterface;
use Raini\Core\Project\Tenant;
use Raini\Core\Settings;

/**
 * Build task which checks for and run `package.json` build scripts.
 *
 * Global configurations for the NodeJS build options exist, such as:
 *   - enabled: defaults to TRUE, but set to FALSE to disable node builds
 *   - packageManager: 'npm' or 'yarn' (defaults to 'npm')
 *   - command: defaults to 'build', can be any package.json script
 *   - arguments: Any additional script arguments to pass
 *
 * These settings can be overridden by tenants that implement
 * \Raini\Project\NodeBuildTenantInterface which provides these
 * option on a per tenant basis.
 */
class NodeBuildTask implements BuildTaskInterface
{

    /**
     * @param Settings            $settings
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected Settings $settings, protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getServiceId(): string
    {
        return 'nodejs';
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return 'NodeJS';
    }

    /**
     * {@inheritdoc}
     */
    public function enabled(Tenant $tenant): bool
    {
        $settings = $this->getBuildSettings($tenant);
        $packageFile = $tenant->getBasePath().'/package.json';

        return !empty($settings['enabled']) && is_readable($packageFile);
    }

    /**
     * {@inheritdoc}
     */
    public function run(Tenant $tenant, BuildOptions $options, ?OutputInterface $output = null): void
    {
        $settings = $this->getBuildSettings($tenant);
        $basePath = $tenant->getBasePath();
        $packageFile = $basePath.'/package.json';

        if ($contents = file_get_contents($packageFile)) {
            $definitions = json_decode($contents, true);

            try {
                if (empty($definitions['scripts'][$settings['command']])) {
                    $err = sprintf('Missing command "%s" for NodeJS builds for %s.', $settings['command'], $tenant->getName());
                    throw new BuildTaskIncompleteException($err);
                }

                // Try to determine the package manager by the lockfile.
                if (empty($settings['packageManager'])) {
                    $settings['packageManager'] = file_exists($basePath.'/yarn.lock') || !file_exists($basePath.'/package-lock.json')
                        ? 'yarn'
                        : 'npm';
                }

                // Setup the NodeJS commands CLI.
                $cli = $this->cliFactory
                    ->create($settings['packageManager'], $options->environment)
                    ->setCwd($tenant->getBasePath());

                if ($options->isSilent) {
                    $output = null;
                } else {
                    $cli->setTty(true);
                }

                $installArgs = ['install'];
                if ($options->isProduction) {
                    $installArgs[] = $settings['packageManager'] === 'npm'
                        ? '--omit=dev'
                        : '--production';
                }

                // Install packages from NodeJS, and execute the build commands.
                $cli->execute($installArgs, $output);
                $cli->execute(array_merge(
                    ['run', $settings['command']],
                    $settings['arguments']
                ), $output);

                if ($output) {
                    $output->writeln('');
                }
            } catch (RuntimeException $e) {
                // The NodeJS unable to complete or has thrown an error, throw
                // exception to indicate that the build task did not complete.
                $err = sprintf('Unable to complete the NodeJS builds for %s.', $tenant->getName());
                throw new BuildTaskIncompleteException($err, 0, $e);
            }
        }
    }

    /**
     * Get the NodeJS build settings for the tenant.
     *
     * This method ensures defaults are applied and checks if per tenant build
     * settings are available and can be applied.
     *
     * @param Tenant $tenant The tenant that the the NodeJS build task is being run for.
     *
     * @return mixed[] The build settings to use for this tenant.
     */
    protected function getBuildSettings(Tenant $tenant): array
    {
        // Apply the NodeJS build setting defaults.
        $settings = $this->settings->nodeBuild ?: [];
        $settings += [
            'enabled' => true,
            'packageManager' => null,
            'command' => 'build',
            'arguments' => [],
        ];

        // Apply tenant overrides if available.
        if ($tenant instanceof NodeBuildTenantInterface) {
            $settings = $tenant->getNodeBuildSettings() + $settings;
        }

        return $settings;
    }
}
