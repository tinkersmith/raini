<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project\BuildTask;

use Symfony\Component\Console\Output\OutputInterface;
use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Project\BuildOptions;
use Raini\Core\Project\BuildTaskInterface;
use Raini\Core\Project\Tenant;

/**
 * Build task which installs composer dependencies.
 */
class ComposerBuildTask implements BuildTaskInterface
{

    /**
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getServiceId(): string
    {
        return 'composer';
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return 'Composer';
    }

    /**
     * {@inheritdoc}
     */
    public function enabled(Tenant $tenant): bool
    {
        // Composer install should always be part of the builds.
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Tenant $tenant, BuildOptions $options, ?OutputInterface $output = null): void
    {
        $path = $tenant->getBasePath();
        $args = ['install', '-o'];
        $cli = $this->cliFactory
            ->create('composer', $options->environment)
            ->setCwd($path);

        if ($options->isProduction) {
            $args[] = '--no-dev';
        }

        // Set the output handler to NULL if set to use silent mode.
        if ($options->isSilent) {
            $output = null;
        } else {
            // Prefer TTY if available, but will still utilize the console
            // $output provided from the build command if TTY is not available.
            $cli->setTty(true);
        }

        $cli->execute($args, $output);
    }
}
