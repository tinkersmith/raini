<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Raini\Core\Environment\EnvironmentInterface;

/**
 * Options for how the project should be generated.
 */
class GenerateOptions
{
    /**
     * Project files are replaced and overwritten.
     */
    const MODE_REPLACE = 'replace';

    /**
     * Attempt to append, and alter existing files.
     *
     * Relies on the Containerizer service's ability read current settings or
     * alter existing project files to handle configuration differences.
     */
    const MODE_MERGE = 'merge';

    /**
     * Keep all project files that already exists without altering them.
     *
     * This mode only generates missing configuration files, or adds completely
     * new configurations, like if a new particle is added and is only appending
     *  new project files.
     */
    const MODE_SKIP = 'skip';

    /**
     * Mode determines how project generation treats existing project files.
     *
     * @var string
     */
    public string $mode = self::MODE_REPLACE;

    /**
     * Flag to indicate that generation is targeting a production environment.
     *
     * @var bool
     */
    public bool $isProduction = false;

    /**
     * The  identifier to use to generate the container files.
     *
     * If NULL (the default) then the generator will user the default
     * containerizer defined for the project. If an identifier is specified
     * here it will override the project settings, and force the generation
     * the use of the specified containerizer.
     *
     * This allows for project generation to target other container types and
     * environments. An example might be to generate no Docker files, or Ansible
     * definition files for a target deployment environment.
     *
     * @var EnvironmentInterface|null
     */
    public EnvironmentInterface|null $environment;
}
