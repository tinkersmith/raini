<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

/**
 * Interface for tenants which can have overrides to the NodeJS build settings.
 *
 * Implement this interface when tenants have the ability to provide per tenant
 * build options for the NodeJS build. This can allow a tenant to disable the
 * NodeJS build operations, but keep general build settings for the rest of the
 * project.
 */
interface NodeBuildTenantInterface
{

    /**
     * Get the tenant specific NodeJS build settings.
     *
     * The override settings should match the available options from the
     * NodeBuild build task. This can also be used to just disable this build
     * task for a tenant, or alter the build arguments.
     *
     * @return mixed[]|null
     *   If an array returned, these are the NodeJS build settings overrides for
     *   this tenant. If NULL, there are no overrides and the global
     *   configuration should be used.
     *
     * @see \Raini\Project\BuildTask\NodeBuild
     */
    public function getNodeBuildSettings(): ?array;
}
