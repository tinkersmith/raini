<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Raini\Core\Exception\ComposerFileMissingException;
use Raini\Core\Extension\DropletInterface;
use Raini\Core\File\PathHelper;

/**
 * Configurations and information about the project's tenant.
 *
 * A tenant would be a application installation, which is part of the current
 * project.
 *
 * An example of this for an Apache HTTP based project would be a website at
 * "./mywebsite/docroot" and another tenant would be a website at
 * "./other/docroot". These each have a separate docroot (separate codebases)
 * each running on the same Apache server.
 *
 * The Drupal Droplet also supports having sites under each tenant to support
 * the Drupal multisite. This is still a single tenant (1 docroot + 1 app code).
 *
 * @phpstan-consistent-constructor
 */
class Tenant
{

    /**
     * @var mixed[]|null
     */
    protected ?array $composerDir;

    /**
     * @param DropletInterface $droplet The Droplet which this tenant belong to.
     * @param string           $name    The site name identifier (unique for project).
     * @param mixed[]          $data    The site information (directories, and Droplet related settings).
     */
    public function __construct(protected DropletInterface $droplet, protected string $name, protected array $data)
    {
        // Apply fallback tenant directory default paths.
        $this->data += [
            'dir' => './',
            'docroot' => './',
        ];

        // Normalize the docroot to make sure it is either absolute or relative
        // to the project root.
        $this->data['docroot'] = $this->normalizePath($this->data['docroot'], $this->data['dir']);
    }

    /**
     * Get the Droplet extension which dictates the behavior of this tenant.
     *
     * @return DropletInterface Get the Droplet that controls the behavior or this tenant.
     */
    public function getDroplet(): DropletInterface
    {
        return $this->droplet;
    }

    /**
     * Ensure paths in tenants are either inside the tenant path or absolute.
     *
     * Also makes sure that the paths are transformed to be relative to the
     * project path, so project generators or other functionality has a common
     * reference directoy. The source paths can be either relative to the tenant
     * or should be relative to the project path.
     *
     * @param string      $path     Path should be absolute, relative to the project, or relatived to the tenant.
     * @param string|null $basePath The base path of the tenant.
     *
     * @return string The path of the resource as either absolute or relative to the project path.
     */
    public function normalizePath(string $path, ?string $basePath = null): string
    {
        // Do not alter absolute paths.
        if (str_starts_with($path, '/')) {
            return $path;
        }

        // Normalize the paths first before comparing them.
        $basePath = PathHelper::reducePath($basePath ?? $this->getBasePath());
        $reduced = PathHelper::reducePath($path);

        // The docroot path must be inside the tenant's base path or absolute.
        // If the path doesn't start with "$data[dir]" and isn't absolute, treat
        // the path is relative to the tenant directory, and convert the path
        // to be relative to the project path so the rest of the app can treat
        // the tenant paths as relative to the project path.
        if (empty($basePath)) {
            $basePath = './';
        } elseif (preg_match('#^'.preg_quote($basePath, '#').'#', $path)) {
            return $path;
        }
        $reduced = rtrim($basePath, " /\n\r\t\v\0")."/{$reduced}";

        return ltrim($reduced, " /\n\r\t\v\0");
    }

    /**
     * Gets the value of a site property.
     *
     * @param string $name The property name to find a value for.
     *
     * @return mixed|null The value of the matching site definition value. NULL if the property doesn't exist.
     */
    public function get(string $name): mixed
    {
        if ('name' === $name) {
            return $this->name;
        }

        return $this->data[$name] ?? null;
    }

    /**
     * @return string The site name identifier.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string Get the path relative to the project's base path.
     */
    public function getBasePath(): string
    {
        return $this->data['dir'];
    }

    /**
     * {@inheritdoc}
     */
    public function getComposerPath(): string
    {
        // Prepare and normalize the tenant's base path.
        $basePath = $this->getBasePath();
        $basePath = empty($basePath) ? '.' : rtrim($basePath, '/');

        return $basePath.'/composer.json';
    }

    /**
     * @return string Get the docroot path relative to the project's basepath.
     */
    public function getDocroot(): string
    {
        return $this->data['docroot'];
    }

    /**
     * @return string Get the Composer bin directory path for this tenant relative to the tenant base path.
     */
    public function getBinDir(): string
    {
        return $this->getComposerData()['binDir'];
    }

    /**
     * @return string Get the Composer vendor directory path for this tenant relative to the tenant base path.
     */
    public function getVendorDir(): string
    {
        return $this->getComposerData()['vendorDir'];
    }

    /**
     * Get the composer.json file contents for this tenant.
     *
     * This could be the same as the project Composer settings if the tenant
     * is the project default and resides at the base project directory.
     *
     * @return mixed[] Loaded composer.json file data for this tenant.
     */
    public function getComposerData(): array
    {
        if (!isset($this->composerDir)) {
            // Typically needed for remote environments where the files are
            // not local and therefore can't be read.
            if (!empty($this->data['vendorDir']) || !empty($this->data['binDir'])) {
                $this->composerDir['vendorDir'] = $this->data['vendorDir'] ?? 'vendor';
                $this->composerDir['binDir'] = $this->data['binDir'] ?? "{$this->composerDir['vendorDir']}/bin";
            } else {
                // Attempt to discover the composer directories from the composer.json file.
                $composerPath = $this->getComposerPath();
                $json = file_get_contents($composerPath);

                if ($json && $info = json_decode($json, true, 64, JSON_INVALID_UTF8_IGNORE)) {
                    $config = $info['config'] ?? [];
                    $this->composerDir['vendorDir'] = $config['vendor-dir'] ?? 'vendor';
                    $this->composerDir['binDir'] = $config['bin-dir'] ?? "{$this->composerDir['vendorDir']}/bin";
                }

                // Missing composer.json file for this project tenant.
                if (!$this->composerDir) {
                    throw new ComposerFileMissingException($this);
                }
            }
        }

        return $this->composerDir;
    }

    /**
     * Generate an environment overridden version of the tenant.
     *
     * This method creates a new copy with the environment overrides applied to
     * it. This means the original tenant is not altered and can be used to
     * create other overrides for environments.
     *
     * @param string  $environment The name of the environment the override is being created for.
     * @param mixed[] $overrides   Override values provided by the environment.
     *
     * @return Tenant A new tenant definition with the environment overrides applied.
     */
    public function createOverridden(string $environment, array $overrides): Tenant
    {
        $overrides += $this->data;

        // Create a new instance with the environment overrides applied to it.
        // Maintain the original tenant, since it might be needed, or used
        // in conjunction with another environment (e.g. syncing environments).
        return new static(
            $this->droplet,
            $this->getName(),
            $overrides
        );
    }
}
