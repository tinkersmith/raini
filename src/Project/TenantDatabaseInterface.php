<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

/**
 * Interface for tenants that include database functionality and management.
 *
 * Tenants can have multiple site or just one and should indictate that with
 * the self::isMultiSite() method.
 */
interface TenantDatabaseInterface extends DatabaseProviderInterface
{

    /**
     * Apply the command options to the command operation data.
     *
     * @param string  $op      The name of the database operation being performed.
     * @param mixed[] $values  The raw command option values.
     * @param mixed[] $options Reference to the operation option values.
     */
    public function applyDbOptions(string $op, array $values, array &$options): void;
}
