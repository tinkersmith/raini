<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Raini\Core\DependencyInjection\PrioritizedCollectorTrait;
use Raini\Core\Project\Exception\TenantNotFoundException;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Service for generating project files for the entire project or each tenant.
 *
 * The project is generated based on generator options and the project settings
 * (raini.project.yml).
 */
class ProjectGenerator
{
    use PrioritizedCollectorTrait {
        addHandler as private collectorAddHandler;
    }

    /**
     * @param TenantManagerInterface   $tenantManager
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(protected TenantManagerInterface $tenantManager, protected EventDispatcherInterface $eventDispatcher)
    {
    }

    /**
     * @param Tenant $tenant The tenant which is being project generation.
     *
     * @return string The status message to display at the start of the project generation.
     */
    public function getStatusMessage(Tenant $tenant): string
    {
        return sprintf("Generating project files for: %s", $tenant->getName());
    }

    /**
     * Adds a new generator handler instance to this project generator.
     *
     * Overrides the typical PrioritizedCollectorTrait by checking for and
     * registering the event subscribers, if the handler subscribes to generator
     * events.
     *
     * Note: That generators are subscribed when the "project.generator"
     * service is being initialized, so only generator related events should
     * be subscribed through a generator.
     *
     * @param GeneratorInterface|TenantGeneratorInterface $handler
     * @param int                                         $priority
     */
    public function addHandler($handler, int $priority = 0): void
    {
        $this->collectorAddHandler($handler, $priority);

        // If generator is an event subscriber, subscribe to generator events.
        if ($handler instanceof EventSubscriberInterface) {
            $this->eventDispatcher->addSubscriber($handler);
        }
    }

    /**
     * Generate project files for the Raini project.
     *
     * @param GenerateOptions      $options Object with current options to use when generating the project files.
     * @param OutputInterface|null $output  The output for messaging and user prompts.
     */
    public function run(GenerateOptions $options, ?OutputInterface $output = null): void
    {
        /** @var GeneratorInterface[] $generators */
        $generators = $this->getSortedHandlers();

        // Run generators at the project scope.
        foreach ($generators as $generator) {
            if ($generator instanceof GeneratorInterface) {
                $generator->run($options, $output);
            }
        }

        // Generate files for each of the tenants.
        foreach ($this->tenantManager->getTenants() as $tenant) {
            try {
                $message = $this->getStatusMessage($tenant);
                $output instanceof SymfonyStyle ? $output->title($message) : $output->writeln("<info>-------\n{$message}</>");

                // Run generators for each of the extensions that defined one.
                foreach ($generators as $generator) {
                    if ($generator instanceof TenantGeneratorInterface && $generator->isApplicable($tenant, $options)) {
                        $generator->runForTenant($tenant, $options, $output);
                    }
                }
            } catch (TenantNotFoundException $e) {
                $output->writeln('<error>'.$e->getMessage().'</error>');
            }
        }
    }
}
