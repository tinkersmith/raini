<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Raini\Core\DependencyInjection\PrioritizedCollectorTrait;
use Raini\Core\Project\BuildTaskInterface;
use Raini\Core\Project\Exception\BuildTaskIncompleteException;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class which provides the project builder tasks manager.
 *
 * Collects and executes project builder tasks which can be registered by
 * extensions. Extensions can defined tagged services with the "build_task" tag
 * and implement the \Raini\Project\BuilderTaskInterface.
 *
 * The priority value for the tag helps to determine the order which the task
 * is run and is optional. Tasks run in ascending order of priority, with '0'
 * being used when a value is not specified.
 *
 * @code
 * MyBuildTask:
 *   class: \namespace\Build\MyBuildTask
 *   arguments:
 *     - settings
 *   tags:
 *     - { name: build_task, priority: 10 }
 * @endcode
 *
 * Builder task can also be registered directly by using the
 * static::addHandler() method, though service injection is preferred if you
 * need to use container service dependencies.
 */
class ProjectBuilder
{

    use PrioritizedCollectorTrait;

    /**
     * Interface required for build task handlers.
     *
     * @var string
     */
    protected string $interface = BuildTaskInterface::class;

    /**
     * @param string[]|null $defaultTasks The list of default task to perform if no task list is provided.
     */
    public function __construct(protected ?array $defaultTasks = null)
    {
    }

    /**
     * @return string[] The build task labels keyed by their service ID values.
     */
    public function getTaskLabels(): array
    {
        $labels = [];
        foreach ($this->getSortedHandlers() as $id => $task) {
            $labels[$id] = $task->label();
        }

        return $labels;
    }

    /**
     * Executes all enabled build tasks.
     *
     * @param Tenant               $tenant  The tenant to run build tasks for.
     * @param BuildOptions         $options Options to use with build tasks.
     * @param OutputInterface|null $output  The console or terminal to write feedback to.
     */
    public function run(Tenant $tenant, BuildOptions $options, ?OutputInterface $output = null): void
    {
        $tasks = $this->getSortedHandlers();

        // If a limited list of tasks have been specified, then filter the list.
        if ($allowedTasks = $options->tasks ?? $this->defaultTasks) {
            $tasks = array_intersect_key($tasks, array_fill_keys($allowedTasks, true));
        }

        // Also remove the tasks that have appeared in the skip list.
        if ($options->skip) {
            $tasks = array_diff_key($tasks, array_fill_keys($options->skip, true));
        }

        /** @var BuildTaskInterface $task */
        foreach ($tasks as $task) {
            try {
                if ($task->enabled($tenant)) {
                    if ($output && !$options->isSilent) {
                        $msg = 'Starting build task: '.$task->label();
                        $output instanceof SymfonyStyle ? $output->section($msg) : $output->writeln("<info>.$msg</>\n");
                    }

                    $task->run($tenant, $options, $output);
                }
            } catch (BuildTaskIncompleteException $e) {
                if ($options->isStrict) {
                    throw $e;
                }
                $output->writeln("<error>".$e->getMessage()."</error>");
            }
        }
    }

    /**
     * @return array<string, BuildTaskInterface> An array of build task handler based on priority ordering.
     */
    protected function getSortedHandlers(): array
    {
        if (!isset($this->sorted)) {
            uasort($this->handlers, static::class.'::compareHandlerPriority');

            $this->sorted = [];
            foreach ($this->handlers as $handler) {
                /** @var BuildTaskInterface $task */
                $task = $handler['handler'];
                $this->sorted[$task->getServiceId()] = $task;
            }
        }

        return $this->sorted;
    }
}
