<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Raini\Core\Project\Exception\SiteNotFoundException;

/**
 * Interface for tenants that provide sites under this tenant.
 *
 * Tenants can have multiple site or just one and should indictate that with
 * the self::isMultiSite() method.
 */
interface SiteTenantInterface
{

    /**
     * Tenants can be single or multi-site.
     *
     * Most tenants are going to be be single site even if they support the
     * ability to be multi-site, this let's the tenant report if it has multiple
     * sites or not.
     *
     * @return bool Is this tenant a multi-site? TRUE if it is, FALSE otherwise.
     */
    public function isMultiSite(): bool;

    /**
     * Get site information for a single site.
     *
     * @param string|null $name      The name of the site info to fetch. If null, fetch the default site.
     * @param bool        $exception Should we throw and exception if site is not found?
     *
     * @return SiteInterface|null The site info matching the requested site.
     *
     * @throws SiteNotFoundException If the requested site is not available.
     */
    #[\ReturnTypeWillChange]
    public function getSite(?string $name = null, bool $exception = true): ?SiteInterface;

    /**
     * Get a list of sites that reside in this tenant.
     *
     * Each site contains information about the URL domain for the site,
     * application specific data, and any site specific connection information.
     *
     * @return SiteInterface[] Get an array of the sites that are part of this tenant. Values contains URL and other site info.
     */
    public function getSites(): array;
}
