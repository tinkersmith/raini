<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Raini\Core\Environment\EnvironmentInterface;

/**
 * Options to pass to build tasks during project builds.
 */
class BuildOptions
{

    /**
     * Flag to determine if the run operation is being performed in strict mode.
     *
     * When a build is running in strict mode, if fails as soon as any of the
     * build tasks fail. With normal run mode (non-strict) a task can throw a
     * \Tinkersmith\Project\Core\Exception\BuildTaskIncompleteException to indicate
     * that it did not complete, however, execution of other build task will
     * continue if not in strict mode.
     *
     * Other build errors will halt the build tasks regardless of this flag.
     *
     * @var bool
     */
    public bool $isStrict = true;

    /**
     * Flag to indicate that project is being build for a production environment.
     *
     * @var bool
     */
    public bool $isProduction = false;

    /**
     * Flag to indicate that the build is operating in silent mode.
     *
     * Indicates that build tasks should attempt to display minimal output.
     * Error statuses should always be displayed, but regular terminal output
     * should be suppressed if not critical.
     *
     * @var bool
     */
    public bool $isSilent = false;

    /**
     * The environment to target for the project build process.
     *
     * The environment value can either be a string that identifies the
     * environment to used with the EnvironmentManager or a loaded environment
     * instance.
     *
     * @var string|EnvironmentInterface|null
     */
    public string|EnvironmentInterface|null $environment;

    /**
     * The target site to execute the builds for if one was provided.
     *
     * Not all build tasks have site specific functionality. Those build tasks
     * that don't will ignore this value.
     *
     * @var SiteInterface|null
     */
    public SiteInterface|null $targetSite;

    /**
     * List of build tasks to skip.
     *
     * @var string[]
     */
    public array $skip = [];

    /**
     * List of build tasks. If empty then, all available task are run.
     *
     * Build tasks only run if they aren't in the $skip list and are enabled
     * (BuildTaskInterface::enable() returns true).
     *
     * @var string[]
     */
    public ?array $tasks = null;
}
