<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

/**
 * Service for generating project files for the entire project or each tenant.
 *
 * The project is generated based on generator options and the project settings
 * (raini.project.yml).
 */
class ProjectInitializer extends ProjectGenerator
{

    /**
     * @param Tenant $tenant The tenant which is being initialized.
     *
     * @return string The status message to display at the start of the project initialization.
     */
    public function getStatusMessage(Tenant $tenant): string
    {
        return sprintf("Initializing local settings: %s", $tenant->getName());
    }
}
