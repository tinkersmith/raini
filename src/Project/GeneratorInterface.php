<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Base interface for Raini extension project generators.
 *
 * If a generator wants to listen for project generation events the implement
 * the \Symfony\Component\EventDispatcher\EventSubscriberInterface interface.
 *
 * Generators which implement this interface are subscribed to the event
 * dispatcher during project geneneration or initialization.
 *
 * @see ProjectGenerator::run()
 */
interface GeneratorInterface
{

    /**
     * Generate project files and assets at the project scope.
     *
     * @param GenerateOptions $options Generator options to direct the project generation.
     * @param OutputInterface $output  The output object to display status and messages to.
     */
    public function run(GenerateOptions $options, ?OutputInterface $output = null): void;
}
