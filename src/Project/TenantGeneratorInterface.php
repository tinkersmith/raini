<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Base interface for project generators that are aware of tenants.
 *
 * If a generator wants to listen for project generation events then they should
 * implement the \Symfony\Component\EventDispatcher\EventSubscriberInterface
 * interface.
 *
 * Generators which implement this interface are subscribed to the event
 * dispatcher during project geneneration or initialization.
 *
 * @see ProjectGenerator::run()
 */
interface TenantGeneratorInterface
{

    /**
     * Is this tenant generator relevant for the target tenant?
     *
     * @param Tenant          $tenant
     * @param GenerateOptions $options
     *
     * @return bool TRUE iff this generator task is applicable to the target tenant.
     */
    public function isApplicable(Tenant $tenant, GenerateOptions $options): bool;

    /**
     * Generate project files and assets for a specific tenant.
     *
     * Creates or regenerates project files using the current project settings
     * for the specified project tenant.
     *
     * @param Tenant          $tenant  The tenant to run the generator on.
     * @param GenerateOptions $options Generator options to direct the project generation.
     * @param OutputInterface $output  The output object to display status and messages to.
     */
    public function runForTenant(Tenant $tenant, GenerateOptions $options, ?OutputInterface $output = null): void;
}
