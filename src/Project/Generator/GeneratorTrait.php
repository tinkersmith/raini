<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project\Generator;

use Raini\Core\Environment;

/**
 * Trait for common project generation and project initialization classes.
 */
trait GeneratorTrait
{

    /**
     * Get the raini environments variables object.
     *
     * @return Environment
     *   The raini environment variables definitions.
     */
    abstract public function getEnv(): Environment;

    /**
     * Gets the template path for the file matchining the $filename.
     *
     * Template path first checks the Raini project directory (defined by
     * "raini.env" at project root) or by the extension.
     *  - <raini>/templates/$filename
     *  - <extension>/templates/$filename
     *
     * The default template is NULL if neither of these paths exists.
     *
     * @param string $filename Name of the template file to look for.
     *
     * @return string|null The full path to a template file matching the requested file. If no template, then NULL.
     */
    protected function getTemplatePath(string $filename): ?string
    {
        $customTpl = $this->getEnv()->getRainiPath().'/templates/'.$filename;
        if (\file_exists($customTpl)) {
            return $customTpl;
        }

        $classDef = new \ReflectionClass($this);
        $tplFile = \dirname($classDef->getFileName()).'/../templates/'.$filename;

        return \file_exists($tplFile) ? \realpath($tplFile) : null;
    }

    /**
     * Create a .gitkeep file at the specified directory.
     *
     * @param string $directory The path to the directory to create the .gitkeep file at.
     */
    protected function addGitKeep(string $directory): void
    {
        // Create .gitkeep file so the repository will keep the folder if empty.
        if ($handle = fopen($directory.'/.gitkeep', 'w')) {
            fclose($handle);
        }
    }
}
