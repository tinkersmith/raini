<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project\Generator;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Event\ComposerGenerateEvent;
use Raini\Core\Event\GeneratorEvents;
use Raini\Core\Extension\ComposerExtensionInterface;
use Raini\Core\Extension\ExtensionManager;
use Raini\Core\Project\GenerateOptions;
use Raini\Core\Project\TenantGeneratorInterface;
use Raini\Core\Project\Tenant;
use Raini\Core\Utility\ComposerBuilder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Generator for creating or updating Composer files from active extensions.
 *
 * Active extensions and Droplets can define Composer packages, and settings,
 * which are accumulated and can be altered through generator events before
 * being written and updated.
 *
 * @see \Raini\Event\GeneratorEvents
 */
class ComposerFileGenerator implements TenantGeneratorInterface
{

    /**
     * @param ExtensionManager         $extensions
     * @param CliFactoryInterface      $cliFactory
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(protected ExtensionManager $extensions, protected CliFactoryInterface $cliFactory, protected EventDispatcherInterface $eventDispatcher)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function isApplicable(Tenant $tenant, GenerateOptions $options): bool
    {
        // Composer files should always be generated for all tenants.
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function runForTenant(Tenant $tenant, GenerateOptions $options, ?OutputInterface $output = null): void
    {
        $droplet = $tenant->getDroplet();
        $composerDef = $this->getComposerDefinitions($tenant);

        $composerFile = $tenant->getComposerPath();
        $composer = file_exists($composerFile)
            ? ComposerBuilder::createFromFile($composerFile)
            : $droplet->createComposerFile($tenant);

        $composer->setOutput($output);
        $composer->addRepositories($composerDef['repositories']);

        $composerEvent = new ComposerGenerateEvent($tenant, $composer);
        $this->eventDispatcher->dispatch($composerEvent, GeneratorEvents::PRE_COMPOSER);

        $composer->addPackages($composerDef['require']);
        $composer->addPackages($composerDef['require-dev'], true);

        $this->eventDispatcher->dispatch($composerEvent, GeneratorEvents::POST_COMPOSER);
        $composer->writeFile();

        $this->cliFactory
            ->create('composer')
            ->setTty(true)
            ->setCwd(dirname($composerFile))
            ->execute(['install'], $output);

        if ($output) {
            $output->writeln('');
        }
    }

    /**
     * Gather the composer definitions from all the active extensions.
     *
     * @param Tenant $tenant The target tenant to get composer definitions for.
     *
     * @return mixed[] The accumulation for extension defined Composer definitions.
     */
    protected function getComposerDefinitions(Tenant $tenant): array
    {
        $composerDef = [
          'repositories' => [],
          'require' => [],
          'require-dev' => [],
        ];

        $extensions = $this->extensions->getExtensions();
        array_unshift($extensions, $tenant->getDroplet());

        // Accumulate the Composer definitions from all the extensions for
        // the extensions that define Composer data.
        foreach ($extensions as $ext) {
            if ($ext instanceof ComposerExtensionInterface) {
                $composerDef['repositories'] += $ext->getRepositories();
                $composerDef['require'] = array_merge($ext->getPackages(), $composerDef['require']);
                $composerDef['require-dev'] = array_merge($ext->getDevPackages(), $composerDef['require-dev']);
            }
        };

        return $composerDef;
    }
}
