<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project\Generator;

use Raini\Core\Environment\EnvironmentManagerInterface;
use Raini\Core\Event\ContainerizationAlterEvent;
use Raini\Core\Event\RainiEvents;
use Raini\Core\Extension\ExtensionManager;
use Raini\Core\Project\GeneratorInterface;
use Raini\Core\Project\GenerateOptions;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Generator for creating the Docker containerizer files.
 *
 * Gathers the container settings and definitions from the project Droplet and
 * all active extensions and builds the configuration files through the
 * ContainerizerManager.
 *
 * This would define the general project level containerization settings.
 *
 * @see \Raini\Event\RainiEvents
 */
class ContainerGenerator implements GeneratorInterface
{

    /**
     * @param ExtensionManager            $extensions
     * @param EnvironmentManagerInterface $environmentManager
     * @param EventDispatcherInterface    $eventDispatcher
     */
    public function __construct(protected ExtensionManager $extensions, protected EnvironmentManagerInterface $environmentManager, protected EventDispatcherInterface $eventDispatcher)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function run(GenerateOptions $options, ?OutputInterface $output = null): void
    {
        $containers = [];
        $volumes = [];
        $networks = [];

        $extensions = $this->extensions->getExtensions();
        array_unshift($extensions, $this->extensions->getDroplet());

        // Add container definition from any of the extensions. To alter
        // definitions, subscribe to the RainiEvents::CONTAINERIZATION_ALTER and
        // make changes in the event. The container definitions here are only
        // for defining new containers, volumes or networks.
        foreach ($extensions as $ext) {
            $definitions = $ext->containerDefinitions();

            if (!empty($definitions['containers'])) {
                $containers += $definitions['containers'];
            }
            if (!empty($definitions['networks'])) {
                $networks += $definitions['networks'];
            }
            if (!empty($definitions['volumes'])) {
                $volumes += $definitions['volumes'];
            }
        }

        // Only generate files if containers are defined.
        if ($containers) {
            $output->writeln('<info>Generating:</info> Docker container files...');

            $event = new ContainerizationAlterEvent($containers, $volumes, $networks);
            $this->eventDispatcher->dispatch($event, RainiEvents::CONTAINERIZATION_ALTER);
/*
            $containerizer = $this->containerizerManager->getContainerizer($options->containerizer);
            $containerizer->createFiles($containers, $volumes, $networks);
            */
        }
    }
}
