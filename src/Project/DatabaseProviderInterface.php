<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

/**
 * Interface for tenants or sites that provide database configurations.
 */
interface DatabaseProviderInterface
{

    /**
     * Fetches the name of the database provider.
     *
     * This name is useful for creating a name for the database file or
     * generating an identifying name.
     *
     * @return string Name of the database provider.
     */
    public function getName(): string;

    /**
     * Get a list of all the databases for this tenant.
     *
     * Each site can have multiple databases, and this tenant can be a
     * multi-site. All these database definitions are collected and keyed by
     * the database name.
     *
     * @return array<string, string[]> Connection information for all database (including all sites) for this tenant
     *                                 keyed by the database name.
     */
    public function getDatabases(): array;

    /**
     * Get the database connection information matching the requested $name ID.
     *
     * @param string $name The database name of the connection info to retrieve
     *
     * @return string[] The database connection information for the target database.
     *
     * @throws \InvalidArgumentException When no matching database connection info is available.
     */
    public function getDatabase(string $name): array;
}
