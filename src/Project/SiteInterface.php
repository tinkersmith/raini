<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

/**
 * Interface for site definitions.
 *
 * Sites are provided by project tenant which implement the SiteTenantInterface.
 *
 * @phpstan-consistent-constructor
 */
interface SiteInterface
{

    /**
     * @param Tenant  $tenant The tenant that this site belongs to.
     * @param string  $name   The site name (identifier)
     * @param mixed[] $values The site definition and properties.
     */
    public function __construct(Tenant $tenant, string $name, array $values);

    /**
     * Get the tenant that is the parent to this site.
     *
     * @return Tenant The site's parent tenant.
     */
    public function getTenant(): Tenant;

    /**
     * Get the site identifier.
     *
     * @return string The site name.
     */
    public function getName(): string;

    /**
     * Get the default domain.
     *
     * @return string Gets the default domain.
     */
    public function getDefaultDomain(): string;

    /**
     * Get the site domain(s).
     *
     * The domains can be expressed as a pattern, but only the left most part
     * of the domain can be a wildcard "*" and there can only be the single
     * wildcard.
     *
     * For example "*.testkit.localhost" or "*".
     *
     * @return string|string[] Either a single site domain or multiple domains.
     */
    public function getDomain(): string|array;
}
