<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Raini\Core\Extension\DropletInterface;
use Raini\Core\Extension\ExtensionList;
use Raini\Core\Extension\ExtensionManager;
use Raini\Core\Project\Exception\TenantNotFoundException;
use Raini\Core\Settings;

/**
 * Default tenant manager implementation.
 *
 * Tenant manager is responsible for fetching tenant instances and provided
 * general information about available tenants.
 */
class TenantManager implements TenantManagerInterface
{

    /**
     * The default tenant type.
     *
     * This would be the name of the default project tenant, which is specified
     * by setting the Droplet extension name as teh "type" value of the
     * raini.project.yml file.
     *
     * @var string
     */
    protected string $defaultType;

    /**
     * The total number of tenant definitions.
     *
     * @var int
     */
    protected int $count;

    /**
     * The tenant definitions as they are described in raini.project.yml file.
     *
     * @var array<string, mixed>
     */
    protected array $definitions;

    /**
     * Initialized tenant instances.
     *
     * These are lazy loaded as they are requested. Use the self::$definitions
     * to determine if a tenant exists and hasn't been initialized yet.
     *
     * @var Tenant[]
     */
    protected array $tenants = [];

    /**
     * @param Settings         $settings
     * @param ExtensionList    $extensionList
     * @param ExtensionManager $extensions
     */
    public function __construct(Settings $settings, protected ExtensionList $extensionList, protected ExtensionManager $extensions)
    {
        // Store the definitions, and only lazy load the tenants as needed.
        $this->definitions = $settings->getTenants();
        $this->defaultType = $settings->getType() ?? key($extensionList->getDroplets());
        $this->count = count($this->definitions);
    }

    /**
     * {@inheritdoc}
     */
    public function rebuild(Settings $settings): self
    {
        // Store the definitions, and only lazy load the tenants as needed.
        $this->definitions = $settings->getTenants();
        $this->defaultType = $settings->getType() ?? $this->defaultType;
        $this->count = count($this->definitions);
        $this->tenants = [];

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isMultiTenant(): bool
    {
        return $this->count > 1;
    }

    /**
     * {@inheritdoc}
     */
    public function getTenantClasses(): array
    {
        $classes = [];
        foreach ($this->extensionList->getDroplets() as $def) {
            /** @var class-string<DropletInterface> $dropletClass */
            $dropletClass = $def->getClass();
            $classes[$def->getName()] = $dropletClass::TENANT_CLASS;
        }

        return $classes;
    }

    /**
     * {@inheritdoc}
     */
    public function getTenantNames(): array
    {
        return array_keys($this->definitions);
    }

    /**
     * {@inheritdoc}
     */
    public function getTenant(?string $name = null): Tenant
    {
        if (empty($name)) {
            $name = key($this->definitions);
            // Find the first tenant from the default Droplet type.
            foreach ($this->definitions as $id => $def) {
                if (empty($def['type']) || $this->defaultType === $def['type']) {
                    $name = $id;
                    break;
                }
            }
        }

        if (!isset($this->tenants[$name])) {
            if ($def = $this->definitions[$name] ?? null) {
                $this->tenants[$name] = $this->createTenant($name, $def);
            } else {
                throw new TenantNotFoundException($name);
            }
        }

        return $this->tenants[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function getTenants(?string $type = null): array
    {
        if (count($this->tenants) < $this->count) {
            foreach (array_diff_key($this->definitions, $this->tenants) as $name => $def) {
                $this->tenants[$name] = $this->createTenant($name, $def);
            }
        }

        if (!empty($type)) {
            return array_filter($this->tenants, function ($value) use ($type) {
                return $value->getDroplet()->getName() === $type;
            });
        }

        return $this->tenants;
    }

    /**
     * Creates a tenant instance from the definition data.
     *
     * @param string  $name       The tenant name.
     * @param mixed[] $definition The tenant unprocessed definition.
     *
     * @return Tenant The tenant instance.
     */
    protected function createTenant(string $name, array $definition): Tenant
    {
        $type = @$definition['type'] ?: $this->defaultType;

        return $this->extensions
            ->getDroplet($type)
            ->buildTenant($name, $definition);
    }
}
