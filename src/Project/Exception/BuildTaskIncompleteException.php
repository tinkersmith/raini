<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project\Exception;

/**
 * The exception for project build tasks that are unable to complete.
 *
 * Normally these errors are when a self contained build task was not able to
 * complete. Depending on the environment build target and if it is setup for
 * a no fail/strict build this might terminate the entire build process, or
 * just get skipped.
 */
class BuildTaskIncompleteException extends BuildTaskException
{
}
