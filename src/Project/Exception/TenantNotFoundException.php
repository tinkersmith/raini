<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project\Exception;

/**
 * Requested tenant definition is not available with the current project setup.
 */
class TenantNotFoundException extends \Exception
{

    /**
     * @param string          $tenantName Name of the site being requested.
     * @param int             $code       An error code value.
     * @param \Throwable|null $prev       A previous exception to be used if chaining exceptions.
     */
    public function __construct(string $tenantName, int $code = 0, ?\Throwable $prev = null)
    {
        $message = "Requested site '{$tenantName}' is not configure and not available.";

        parent::__construct($message, $code, $prev);
    }
}
