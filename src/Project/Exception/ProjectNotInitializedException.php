<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project\Exception;

/**
 * Functionality is being called which requires a full configured project.
 */
class ProjectNotInitializedException extends \Exception
{

    /**
     * @param \Throwable|null $previous An exception to chain if a previous exception was thrown.
     */
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct('Functionality cannot be used before project is configured. Call "raini create" first.', 0, $previous);
    }
}
