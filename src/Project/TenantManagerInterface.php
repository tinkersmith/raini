<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Project;

use Raini\Core\Project\Exception\TenantNotFoundException;
use Raini\Core\Settings;

/**
 * Interface for services that manager and provide info about project tenants.
 */
interface TenantManagerInterface
{

    /**
     * Reset the tenant definitions, and created tenant instances.
     *
     * This is for commands that create or alter project configurations and
     * want to ensure that project files are being created with the updated
     * information.
     *
     * @param Settings $settings The project settings to use for rebuilding the tenant definitions with.
     *
     * @return self
     */
    public function rebuild(Settings $settings): self;

    /**
     * Does this project have multiple tenants.
     *
     * @return bool Does this project have multiple tenants? TRUE if this project has more than one tenant.
     */
    public function isMultiTenant(): bool;

    /**
     * Get a list of the fully namespaced classes types of the defined tenants.
     *
     * The tenant classes are keyed by the Droplet name that utilizes that
     * tenant type.
     *
     * @return array<string, class-string<Tenant>>
     */
    public function getTenantClasses(): array;

    /**
     * @return string[] List of tenant identifiers for available tenants.
     */
    public function getTenantNames(): array;

    /**
     * Fetch a specific tenant by name.
     *
     * @param string $name Name of the tenant to fetch.
     *
     * @return Tenant The tenant matching the requested name.
     *
     * @throws TenantNotFoundException
     */
    public function getTenant(string $name): Tenant;

    /**
     * @param string|null $type When provided, only fetch tenants which match the droplet type specified.
     *
     * @return Tenant[] List of all available tenants.
     */
    public function getTenants(?string $type = null): array;
}
