<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event\Database;

use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\Project\DatabaseProviderInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\EventDispatcher\Event;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * Event for collecting regex patterns to identify structure only DB tables.
 *
 * This is called when command want to export or know which database tables can
 * be exported with just their structure and without their data. This allows
 * the extensions to identify the database, and knowing how it should be
 * structured (like in the case of Raini Drupal) can determine which tables
 * should not be included in database exports.
 *
 * @see DataBaseEvents::PRE_IMPORT
 * @see DatabaseEvents::POST_IMPORT
 */
class DbImportEvent extends Event
{

    /**
     * @param DatabaseProviderInterface                           $dbProvider     The object which owns the database
     * @param string                                              $targetDb       The name of the database being targeted.
     * @param bool                                                $useScripts     If the import operation allows running of post deploy scripts.
     * @param mixed[]                                             $options        Additional options provided by Droplets and extensions.
     * @param ExecutionContextInterface|EnvironmentInterface|null $executeContext The context event listeners should execute in.
     * @param OutputInterface|null                                $output         The console output stream.
     */
    public function __construct(public readonly DatabaseProviderInterface $dbProvider, public readonly string $targetDb, public readonly bool $useScripts, public readonly array $options = [], public ExecutionContextInterface|EnvironmentInterface|null $executeContext = null, public ?OutputInterface $output = null)
    {
    }

    /**
     * The object that is respoonsible for the target database.
     *
     * In general this object is normally the tenant or site that this owns
     * the targeted database.
     *
     * @return DatabaseProviderInterface The database provider object.
     */
    public function getDbProvider(): DatabaseProviderInterface
    {
        return $this->dbProvider;
    }

    /**
     * Get the name of the database to build the structure table patterns for.
     *
     * @return string The name of the target database.
     */
    public function getTargetDb(): string
    {
        return $this->targetDb;
    }

    /**
     * Get the environment or execution context to execute commands with.
     *
     * @return ExecutionContextInterface|EnvironmentInterface|null The execution to run commands with.
     */
    public function getContext(): ExecutionContextInterface|EnvironmentInterface|null
    {
        return $this->executeContext;
    }

    /**
     * Flag to indicate that listeners should run all import optional scripts.
     *
     * Normally this would be optional deploy scripts, or used to indicate if
     * the full suite of scripts should be run for an import.
     *
     * @return bool TRUE if the event listeners should execute their full deployment scripts.
     */
    public function useScripts(): bool
    {
        return $this->useScripts;
    }

    /**
     * Additional database import options that can be provided by extensions.
     *
     * @return mixed[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * The output stream to write any output to.
     *
     * @return OutputInterface|null The console output stream if there is one.
     */
    public function getOutput(): ?OutputInterface
    {
        return $this->output;
    }
}
