<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Allow event subscribers to alter database command options and parameters.
 *
 * This allows extensions to add more options to the database pre and post
 * commands. Droplets should implement TenantDatabaseInterface and allow
 * the tenant to interpret those options.
 *
 * This event is called during the Command::configure() for database commands.
 *
 * @see DatabaseEvents::ALTER_COMMAND
 * @see \Raini\Core\Project\TenantDatabaseInterface
 */
class AlterDbCommandEvent extends Event
{

    /**
     * @param string  $name    The name of the database being targeted.
     * @param Command $command The console command being altered.
     */
    public function __construct(public readonly string $name, public readonly Command $command)
    {
    }

    /**
     * @return string The console command name.
     */
    public function getCommandName():string
    {
        return $this->name;
    }

    /**
     * @return Command The Symfony console command being configured.
     */
    public function getCommand(): Command
    {
        return $this->command;
    }
}
