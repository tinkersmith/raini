<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event\Database;

use Raini\Core\Project\DatabaseProviderInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event for collecting regex patterns to identify structure only DB tables.
 *
 * This is called when command want to export or know which database tables can
 * be exported with just their structure and without their data. This allows
 * the extensions to identify the database, and knowing how it should be
 * structured (like in the case of Raini Drupal) can determine which tables
 * should not be included in database exports.
 *
 * @see DatabaseEvents::STRUCTURE_TABLE_REGEX
 */
class StructureTableEvent extends Event
{

    /**
     * @param DatabaseProviderInterface $dbProvider  The object which owns the database
     * @param string                    $targetDb    The name of the database being targeted.
     * @param bool                      $isBackup    Is the database export or usage for backup purposes?
     * @param string[]                  $tablesRegex Initial list of regex patterns to identity struture only tables.
     */
    public function __construct(protected DatabaseProviderInterface $dbProvider, protected readonly string $targetDb, public readonly bool $isBackup = false, public array $tablesRegex = [])
    {
    }

    /**
     * The object that is respoonsible for the target database.
     *
     * In general this object is normally the tenant or site that this owns
     * the targeted database.
     *
     * @return DatabaseProviderInterface The database provider object.
     */
    public function getDbProvider(): DatabaseProviderInterface
    {
        return $this->dbProvider;
    }

    /**
     * Get the name of the database to build the structure table patterns for.
     *
     * @return string The name of the target database.
     */
    public function getTargetDb(): string
    {
        return $this->targetDb;
    }

    /**
     * Get the current list of regex patterns for matching structure tables.
     *
     * @return string[] The list of regex patterns to match structure tables.
     */
    public function getTablesRegex(): array
    {
        return $this->tablesRegex;
    }

    /**
     * Adds more regex expressions to used to match structure only DB tables.
     *
     * @param string[] $add Additional structure only table patterns to add.
     */
    public function addTableRegex(array $add): void
    {
        $this->tablesRegex = array_merge($this->tablesRegex, $add);
        $this->tablesRegex = array_unique($this->tablesRegex);
    }
}
