<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event\Database;

/**
 * Events pertaining to the database operations and commands.
 */
final class DatabaseEvents
{

    /**
     * Alter the Symfony command options and parameter definitions.
     *
     * This allows extensions to add more options to the database pre and post
     * commands. Droplets should implement TenantDatabaseInterface and allow
     * the tenant to interpret those options.
     *
     * This event is called during the Command::configure() for database
     * commands.
     */
    public const ALTER_COMMAND = 'db:alter_command';

    /**
     * Event for getting a list of patterns to identify db structure tables.
     *
     * There are many reasons that a database should avoid exporting table
     * values (security, ephemeral data, etc...) and this event allows
     * extensions that utilize the databases to provide patterns for database
     * processes to identify and exclude these tables.
     *
     * @see \Raini\Event\Database\StructureTableEvent
     */
    public const STRUCTURE_TABLE_REGEX = 'db:struct_table_regex';

    /**
     * Event for subscribers to execute before the database is imported.
     *
     * This event is dispatched before the data tables are cleared and the
     * database import is executed.
     *
     * @see \Raini\Event\Database\DbImportEvent
     */
    public const PRE_IMPORT = 'db:pre_import';

    /**
     * Event for listeners to execute after database has been imported.
     *
     * @see \Raini\Event\Database\DbImportEvent
     */
    public const POST_IMPORT = 'db:post_import';
}
