<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event;

use Raini\Core\Settings;
use Symfony\Component\Console\Application;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Base event class for Raini events.
 *
 * Raini events have references to the current project settings and application
 * state. The Raini event dispatcher can use the RainiEventInterface to detect
 * this type of event and inject the application and settings into the event
 * instance before it is dispatched to the event subscribers.
 *
 * @see \Raini\Event\EventDispatcher
 */
class RainiEvent extends Event implements RainiEventInterface
{

    /**
     * Reference to the Raini application if available.
     *
     * @var Application
     */
    protected $app;

    /**
     * Reference to the Raini project settings.
     *
     * @var Settings
     */
    protected $settings;

    /**
     * {@inheritdoc}
     */
    public function setApplication(Application $application): void
    {
        $this->app = $application;
    }

    /**
     * {@inheritdoc}
     */
    public function getApplication(): ?Application
    {
        return $this->app;
    }

    /**
     * {@inheritdoc}
     */
    public function setSettings(Settings $settings): void
    {
        $this->settings = $settings;
    }

    /**
     * {@inheritdoc}
     */
    public function getSettings(): ?Settings
    {
        return $this->settings;
    }
}
