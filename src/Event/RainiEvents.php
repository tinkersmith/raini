<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event;

/**
 * Core Raini event names and descriptions.
 */
final class RainiEvents
{
    /**
     * Event to allow extensions to alter the containerization definitions.
     *
     * This event uses \Raini\Event\ContainerizationAlterEvent as
     * the event class and allows extensions to alter the containerization
     * definitions before they are written to project files and used to manage
     * containers.
     *
     * @see \Raini\Event\ContainerizationAlterEvent
     */
    public const CONTAINERIZATION_ALTER = 'raini:containerization_alter';
}
