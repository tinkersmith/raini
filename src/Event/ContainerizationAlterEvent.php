<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event;

/**
 * Event class for the RainiEvents::CONTAINERIZATION_ALTER event.
 *
 * @see \Raini\Event\RainiEvents::CONTAINERIZATION_ALTER
 */
class ContainerizationAlterEvent extends RainiEvent
{

    /**
     * @param mixed[]  $containers The containerization container definitions.
     * @param string[] $volumes    The storage volume definitions.
     * @param mixed[]  $networks   The containerization network definitions.
     */
    public function __construct(protected array &$containers, protected array &$volumes = [], protected array &$networks = [])
    {
    }

    /**
     * @return mixed[] Reference to the containerization container definitions.
     */
    public function &getContainers(): array
    {
        return $this->containers;
    }

    /**
     * @return string[] Reference to the storage volume definitions.
     */
    public function &getVolumes(): array
    {
        return $this->volumes;
    }

    /**
     * @return mixed[] Reference to the container network definitions.
     */
    public function &getNetworks(): array
    {
        return $this->networks;
    }
}
