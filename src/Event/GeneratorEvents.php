<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event;

/**
 * Core Raini project generation event names and descriptions.
 */
final class GeneratorEvents
{
    /**
     * Event to alter a composer.json file before adding Composer packages.
     *
     * This event uses \Raini\Event\ComposerGenerateEvent as the
     * event class and allows project generators to alter the Composer.json
     * (via a \Raini\Utility\ComposerBuilder). This method is called
     * after repositories are added but before "require" and "require-dev"
     * packages are added.
     *
     * @see \Raini\Event\ComposerGenerateEvent
     * @see \Raini\Utility\ComposerBuilder
     */
    public const PRE_COMPOSER = 'raini:pre_composer_generate';

    /**
     * Event to alter a site composer.json file after adding Composer packages.
     *
     * This event uses \Raini\Event\ComposerGenerateEvent as the
     * event class and allows project generators to alter the Composer.json
     * (via a \Raini\Utility\ComposerBuilder). This method is called
     * after Composer "require" and "require-dev" packages are added.
     *
     * @see \Raini\Event\ComposerGenerateEvent
     * @see \Raini\Utility\ComposerBuilder
     */
    public const POST_COMPOSER = 'raini:post_composer_generate';
}
