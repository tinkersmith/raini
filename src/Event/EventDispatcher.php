<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event;

use Raini\Core\Settings;
use Symfony\Component\Console\Application;
use Symfony\Component\EventDispatcher\EventDispatcher as EventDispatcherBase;

/**
 * Event dispatcher which is aware of the Raini settings and application.
 *
 * Event dispatcher is aware of Raini project settings and the application
 * instance. This allows the event dispatcher to apply Raini project contexts
 * to the Raini events just before they are dispatched. Raini events can then
 * be aware of the current project settings without forcing the caller to build
 * the project context.
 */
class EventDispatcher extends EventDispatcherBase
{

    /**
     * @param Settings    $settings
     * @param Application $app
     */
    public function __construct(protected Settings $settings, protected Application $app)
    {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(object $event, ?string $eventName = null): object
    {
        // If a Raini event add project settings and the current application.
        if ($event instanceof RainiEventInterface) {
            $event->setSettings($this->settings);
            $event->setApplication($this->app);
        }

        return parent::dispatch($event, $eventName);
    }
}
