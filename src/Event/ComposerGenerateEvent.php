<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event;

use Raini\Core\Extension\DropletInterface;
use Raini\Core\Project\Tenant;
use Raini\Core\Utility\ComposerBuilder;

/**
 * Event class for GeneratorEvents::PRE_COMPOSER and GeneratorEvents::POST_COMPOSER events.
 *
 * @see \Raini\Event\GeneratorEvents::PRE_COMPOSER
 * @see \Raini\Event\GeneratorEvents::POST_COMPOSER
 */
class ComposerGenerateEvent extends RainiEvent
{

    /**
     * @param Tenant          $tenant
     * @param ComposerBuilder $composer
     */
    public function __construct(protected Tenant $tenant, protected ComposerBuilder $composer)
    {
    }

    /**
     * @return string The identifier for the site which the Composer file is being generated for.
     */
    public function getTenantName(): string
    {
        return $this->tenant->getName();
    }

    /**
     * @return Tenant The site definition (minimally the base "dir" and "docroot") the composer file is
     *                        generated for.
     */
    public function getTenant(): Tenant
    {
        return $this->tenant;
    }

    /**
     * @return DropletInterface The droplet instance, which manages the site building built.
     */
    public function getDroplet(): DropletInterface
    {
        return $this->tenant->getDroplet();
    }

    /**
     * @return ComposerBuilder The builder instance to use to construct or alter the composer.json file values.
     */
    public function getComposerBuilder(): ComposerBuilder
    {
        return $this->composer;
    }
}
