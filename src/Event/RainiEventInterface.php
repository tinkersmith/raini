<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Event;

use Raini\Core\Settings;
use Symfony\Component\Console\Application;

/**
 * Interface for event objects that are aware of Raini application and settings.
 */
interface RainiEventInterface
{

    /**
     * @param Application $application The Raini application instance.
     */
    public function setApplication(Application $application): void;

    /**
     * @return Application Gets the reference to the console application.
     */
    public function getApplication(): ?Application;

    /**
     * @param Settings $settings The overall project settings.
     */
    public function setSettings(Settings $settings): void;

    /**
     * @return Settings Get the Raini project settings.
     */
    public function getSettings(): ?Settings;
}
