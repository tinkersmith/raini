<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Configurator;

use Tinkersmith\Configurator\SchemaConfigurator;

/**
 * A schema configurator that is aware of Raini extension paths.
 *
 * Designed to be compatible with the Configurator attribute declaration. Only
 * simple values can be used in an attribute declaration since their
 * instantiation is deferred to when the attribute is fetched through reflect
 * and an instance is explicitly requested.
 *
 * @see \Tinkersmith\Configurator\Attribute\Configurator
 */
class ExtensionSchemaConfigurator extends SchemaConfigurator
{

    /**
     * @param string             $extensionId The identifier for the extension to make the path relative to.
     * @param string|\Stringable $path        Path of the schema file relative to the extension path.
     * @param SchemaBuilder|null $builder     The schema builder instance for processing definitions.
     */
    public function __construct(string $extensionId, string|\Stringable $path = '', ?SchemaBuilder $builder = null)
    {
        parent::__construct($builder->extensionPath($extensionId, $path), $builder);
    }
}
