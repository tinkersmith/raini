<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Configurator;

use Raini\Core\Environment\EnvironmentManagerInterface;
use Raini\Core\Extension\DropletInterface;
use Raini\Core\Extension\ExtensionList;
use Raini\Core\Settings;
use Tinkersmith\Configurator\Configurator;
use Tinkersmith\Configurator\ConfiguratorInterface;
use Tinkersmith\Configurator\DependentProperty;
use Tinkersmith\Configurator\Property;
use Tinkersmith\Configurator\Type\OptionType;
use Tinkersmith\Configurator\Type\StringType;
use Tinkersmith\Configurator\Validator\IdValidator;

/**
 * Configurator for setting up the Raini project settings.
 *
 * The project configurator will manage the configuration of the Droplet
 * settings, tenant settings and the extension settings.
 */
class ProjectConfigurator extends Configurator implements ConfiguratorInterface
{

    /**
     * @param Settings                    $settings
     * @param ExtensionList               $extensions
     * @param EnvironmentManagerInterface $environmentManager
     */
    public function __construct(protected Settings $settings, protected ExtensionList $extensions, protected EnvironmentManagerInterface $environmentManager)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getSchema(array $values = []): array
    {
        if (!isset($this->schema)) {
            /** @var array{label: string, class: class-string<DropletInterface>}[] $droplets */
            $droplets = [];
            $types = [];

            foreach ($this->extensions->getDroplets() as $id => $droplet) {
                $types[$id] = $droplet->getName();

                /** @var class-string<DropletInterface> $dropletClass */
                $dropletClass = $droplet->getClass();
                $droplets[$id] = [
                    'label' => $types[$id],
                    'class' => $dropletClass,
                ];
            }

            $schema = [
                'type' => new Property('type', new OptionType($types), null, required: true, label: 'Project type'),
                'id' => new Property('id', new StringType(), new IdValidator(), true, label: 'Project ID'),
                'name' => new Property('name', new StringType(), null, true, label: 'Project Name'),
                'environment' => new Property('environment', new EnvironmentConfigurator($this->environmentManager)),
                'settings' => new DependentProperty('settings', $droplets, label: '<type> Settings'),
            ];

            $this->schema = $schema;
        }

        return $this->schema;
    }
}
