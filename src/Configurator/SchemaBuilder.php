<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Configurator;

use Raini\Core\Extension\ExtensionList;
use Raini\Core\File\ExtensionPathResolver;
use Raini\Core\Value\ValueResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\Yaml\Tag\TaggedValue;
use Tinkersmith\Configurator\Property;
use Tinkersmith\Configurator\SchemaBuilder as ConfiguratorSchemaBuilder;
use Tinkersmith\Configurator\Type\IntegerType;
use Tinkersmith\Configurator\Type\StringType;

/**
 * Configurator schema builder which is aware of Raini services and extensions.
 *
 * Allows configurator schemas to include calls to service methods or access
 * container services.
 */
class SchemaBuilder extends ConfiguratorSchemaBuilder
{

    /**
     * List of tag names supported by the value resolvers.
     *
     * @var string[]|null
     */
    protected ?array $valueTags;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(protected ContainerInterface $container)
    {
    }

    /**
     * Resolve a path relative to a Raini extension.
     *
     * Use "raini" or "core" as the $extensionId for the Raini core package
     * itself.
     *
     * @param string $extensionId The name of the extension to build the path relative to.
     * @param string $path        The path relative
     *
     * @return string|\Stringable
     */
    public function extensionPath(string $extensionId, string|\Stringable $path = ''): string|\Stringable
    {
        return $this->container
            ->get(ExtensionPathResolver::class)
            ->extensionPath($extensionId, $path);
    }

    /**
     * {@inheritdoc}
     */
    protected function createInstance(string $classname, array $values = []): mixed
    {
        // Allow the use of value resolvers to validate for configurator
        // properties if they are string or integer based types.
        if (!empty($values['resolvable']) || (Property::class === $classname && !empty($values['type'])
            && ($values['type'] instanceof StringType || $values['type'] instanceof IntegerType))
        ) {
            $instance = parent::createInstance(ResolverAwareProperty::class, $values);

            return $instance->setAllowedTags($this->getResolverTags());
        }

        return parent::createInstance($classname, $values);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException
     * @throws ServiceNotFoundException
     */
    protected function resolveTaggedValue(TaggedValue $value): mixed
    {
        switch ($value->getTag()) {
            case 'service':
                @[$name, $method] = explode('::', $value->getValue(), 2);
                $service = $this->container->get($name, ContainerInterface::NULL_ON_INVALID_REFERENCE);

                return $service && $method ? call_user_func([$service, $method]) : $service;

            case 'extensionExists':
                /** @var \Raini\Core\Extension\ExtensionList $extList */
                $extList = $this->container->get(ExtensionList::class);

                return (bool) $extList->getExtensionDefinition($value->getValue());

            default:
                return parent::resolveTaggedValue($value);
        }
    }

    /**
     * Value resolver tags supported by the current settings.value_resolver.
     *
     * Value resolvers are provided by services that implement the
     * "value_resolver" tagged service name.
     *
     * @return string[] The list of supported value resolution tags.
     */
    protected function getResolverTags(): array
    {
        if (!isset($this->valueTags)) {
            /** @var ValueResolverInterface $resolver */
            $resolver = $this->container->get('settings.value_resolver');
            $this->valueTags = $resolver->getTags();
        }

        return $this->valueTags;
    }
}
