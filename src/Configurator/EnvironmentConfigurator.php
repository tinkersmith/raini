<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Configurator;

use Raini\Core\Environment\EnvironmentManagerInterface;
use Tinkersmith\Configurator\TypedConfigurator;

/**
 * Project configurator for managing the environment settings.
 */
class EnvironmentConfigurator extends TypedConfigurator
{

    /**
     * @param EnvironmentManagerInterface $environmentManager
     */
    public function __construct(EnvironmentManagerInterface $environmentManager)
    {
        $envOpts = array_map(function ($factory) {
            return $factory->getClass();
        }, $environmentManager->getFactories());

        parent::__construct($envOpts);
    }
}
