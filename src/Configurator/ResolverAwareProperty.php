<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Configurator;

use Symfony\Component\Yaml\Tag\TaggedValue;
use Tinkersmith\Configurator\Property;

/**
 * A configurator property that is aware of tagged value resolvers.
 *
 * This allows Raini to create values that are resolved by the current context
 * such as "env" for environment variables, or values that can be resolved by
 * other extensions such as "lando" or "awsSecret" value resolvers.
 */
class ResolverAwareProperty extends Property
{

    const RESOLVE_VALUE_PAT = '/^!([a-zA-Z]+)\s*(.+)$/';

    /**
     * List of allowed value tags that Raini knows how to resolve.
     *
     * If empty or NULL, all value tags are allowed.
     *
     * @var string[]|null
     */
    protected ?array $allowedTags;

    /**
     * Set the allowed value resolver tags this property will accept.
     *
     * @param string[]|null $tags List of value tags to accept.
     *
     * @return self
     */
    public function setAllowedTags(?array $tags = null): self
    {
        $this->allowedTags = $tags;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function validateProperty(mixed $value, ?string $label = null): true|string
    {
        if ($value instanceof TaggedValue) {
            if ($this->allowedTags && !in_array($value->getTag(), $this->allowedTags)) {
                return sprintf('Invalid value tag type "%s" is unknown (allowed: %s).', $value->getTag(), implode(', ', $this->allowedTags));
            }

            return true;
        }
        if ($value && preg_match(self::RESOLVE_VALUE_PAT, $value, $matches)) {
            if ($this->allowedTags && !in_array($matches[1], $this->allowedTags)) {
                return sprintf('Invalid value tag type "%s" is unknown (allowed: %s).', $matches[1], implode(', ', $this->allowedTags));
            }

            return true;
        }

        return parent::validateProperty($value, $label);
    }

    /**
     * {@inheritdoc}
     */
    protected function extractProperty(mixed $value): mixed
    {
        if ($value instanceof TaggedValue) {
            return $value;
        }
        if ($value && preg_match(self::RESOLVE_VALUE_PAT, $value, $matches)) {
            return new TaggedValue($matches[1], $matches[2]);
        }

        return parent::extractProperty($value);
    }
}
