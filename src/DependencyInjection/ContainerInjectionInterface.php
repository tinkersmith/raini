<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This interface gives classes who need a services a factory method.
 */
interface ContainerInjectionInterface
{

    /**
     * Instantiates a new instance of this class.
     *
     * This is a factory method that returns a new instance of this class. The
     * factory should pass be able to pass any needed service dependencies into
     * the constructor of this class with a matching container service.
     *
     * @param ContainerInterface $container The service container to provide service dependencies.
     * @param mixed              ...$args   Additional argument to be passed.
     *
     * @return static An instance of this class with the dependencies injected from the container.
     */
    public static function create(ContainerInterface $container, ...$args): static;
}
