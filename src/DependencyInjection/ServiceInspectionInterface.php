<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\DependencyInjection;

/**
 * Container services that are inspectable to find info like their service ID.
 */
interface ServiceInspectionInterface
{
    /**
     * Get the machine ID of the service.
     *
     * Can be used by a service collector to identify the service, instead of
     * the container name. This helps since the class or interface name is
     * used for autowiring, and also allows the service to identify itself.
     *
     * @return string The machine identifier of the service.
     */
    public function getServiceId(): string;
}
