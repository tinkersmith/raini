<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates instances of classes by matching constructor parameters to services.
 */
class ClassResolver
{

    /**
     * @param ContainerInterface $container The dependency injection services container. Used to resolve services for
     *                                      class constructors.
     */
    public function __construct(protected ContainerInterface $container)
    {
    }

    /**
     * Creates a new instance of the class specified by $className.
     *
     * The class instance is created using the constructor of the $className
     * by passing the $args supplied by the caller and resolving all the
     * remaining constructor parameters by matching the container services by
     * the parameter type hinted types.
     *
     * @param string  $className The fully namespaced class to create a new instance for.
     * @param mixed[] $args      Array of arguments to pass to the constructor. Arguments passed this way are matched
     *                           by the array key and constructor parameter name. Other parameters are discovered
     *                           using services from the dependency injection container.
     *
     * @return object An instance of the requested class type.
     *
     * @throws \InvalidArgumentException
     * @throws \ReflectionException
     */
    #[\ReturnTypeWillChange]
    public function createInstance(string $className, array $args = []): object
    {
        if (is_a($className, ContainerInjectionInterface::class, true)) {
            return $className::create($this->container, ...$args);
        }

        $reflect = new \ReflectionClass($className);
        $constructor = $reflect->getConstructor();

        foreach ($constructor->getParameters() as $param) {
            $name = $param->getName();
            $type = $param->getType();

            // If the argument is supplied directly, use this argument and skip.
            if (isset($args[$name])) {
                continue;
            } elseif ('appId' === $name) {
                $args['appId'] = $this->container->getParameter('app.project_id');
            }

            $service = null;
            if ($type instanceof \ReflectionNamedType) {
                $service = !$type->isBuiltin() && $type->getName()
                    ? $this->container->get($type->getName(), ContainerInterface::NULL_ON_INVALID_REFERENCE)
                    : null;
            } elseif ($type) {
                // @todo support union or intersection parameters
                $errorMsg = sprintf('Constructor has a Union or Intersection parameter for %s. The class resolver is not able to resolve these dependencies at the moment.', $name);
                throw new \InvalidArgumentException($errorMsg);
            }

            if (null === $service) {
                if ($param->isDefaultValueAvailable()) {
                    $service = $param->getDefaultValue();
                } elseif (!$param->allowsNull()) {
                    $errorMsg = sprintf('Constructor for %s class parameter %s cannot be resolved with a container service.', $className, $name);
                    throw new \InvalidArgumentException($errorMsg);
                }
            }
            $args[$name] = $service;
        }

        return $reflect->newInstanceArgs($args);
    }
}
