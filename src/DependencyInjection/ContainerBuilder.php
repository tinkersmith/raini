<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder as SymfonyContainerBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * The container service builder class.
 *
 * Raini has some parameters that it would like to allow extensions add more
 * values to, instead of replace them like the default Symfony ContainerBuilder
 * class does.
 *
 * An example of this is the <em>cli_options</em> parameter. Extensions should
 * be able to add more cli command options, such as the Drupal droplet that
 * wants to be able to add <em>drush</em> cli_options, without losing the
 * <em>composer</em> command options from Raini core.
 */
class ContainerBuilder extends SymfonyContainerBuilder
{

    /**
     * @param ParameterBagInterface|null $parameterBag The parameter bag instance to store parameter to.
     * @param array<string, int>         $mergeParams  List of parameter keys that should be merge and the max merge
     *                                                 depth as the array value.
     */
    public function __construct(?ParameterBagInterface $parameterBag = null, protected array $mergeParams = [])
    {
        parent::__construct($parameterBag);
    }

    /**
     * @param string                                       $name  The name of the parameter being set.
     * @param mixed[]|bool|string|int|float|\UnitEnum|null $value The value to set for the parameter.
     */
    public function setParameter(string $name, array|bool|string|int|float|\UnitEnum|null $value): void
    {
        if (isset($this->mergeParams[$name]) && $this->hasParameter($name)) {
            $maxDepth = $this->mergeParams[$name];
            $value = static::mergeParameter($value, $this->getParameter($name), $maxDepth);
        }

        parent::setParameter($name, $value);
    }

    /**
     * Merge nested array parameters up to a maximum set depth.
     *
     * Method is meant to support merging nested arrays through recursive calls
     * to itself with the depth parameter successively reduced.
     *
     * @param mixed $value Value being merged into the source value. Values here take precedence over $src values.
     * @param mixed $src   The source value, that the new values are being merged into.
     * @param int   $depth The maximum depth to merge items.
     *
     * @return mixed The merged value result.
     */
    protected static function mergeParameter(mixed $value, mixed $src, int $depth = 1): mixed
    {
        if ($depth > 0 && is_array($value) && is_array($src)) {
            foreach (array_intersect_key($src, $value) as $key => $srcItem) {
                // Recursively merge until reaching the merge depth limit.
                $value[$key] = static::mergeParameter($value[$key], $srcItem, --$depth);
            }

            $value += $src;
        }

        return $value;
    }
}
