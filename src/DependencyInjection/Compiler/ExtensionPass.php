<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\DependencyInjection\Compiler;

use Raini\Core\Extension\ExtensionList;
use Raini\Core\Extension\ExtensionManager;
use Raini\Core\Extension\ExtensionType;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Compiler pass to add the droplets and extension classes as services.
 *
 * This allows injection of extensions and droplets through the container
 * service directly instead of having services find and load the extension
 * manager to get the extension service.
 */
class ExtensionPass implements CompilerPassInterface
{
    /**
     * @param ExtensionList $extensions Available list of installed Raini extensions.
     */
    public function __construct(protected ExtensionList $extensions)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container): void
    {
        $managerRef = new Reference(ExtensionManager::class);

        foreach ($this->extensions->getAllDefinitions() as $name => $extInfo) {
            if ($extensionClass = $extInfo->getClass()) {
                switch ($extInfo->getType()) {
                    case ExtensionType::Droplet:
                        $factory = [$managerRef, 'getDroplet'];
                        $type = 'droplet';
                        break;

                    case ExtensionType::Extension:
                        $factory = [$managerRef, 'getExtension'];
                        $type = 'extension';
                        break;

                    default:
                        continue 2;
                }

                $definition = new Definition($extensionClass, [$name]);
                $definition->setFactory($factory);
                $container->setDefinition($extensionClass, $definition);

                $alias = new Alias($extensionClass, true);
                $container->setAlias("{$type}.instance.{$name}", $alias);
            }
        }
    }
}
