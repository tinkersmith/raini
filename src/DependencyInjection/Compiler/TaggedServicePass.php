<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Find service collectors, and apply the relevant tagged services to them.
 */
class TaggedServicePass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container): void
    {
        $taggedServices = ['service_collector' => []];

        foreach ($container->getDefinitions() as $id => $def) {
            foreach ($def->getTags() as $name => $tag) {
                $taggedServices[$name][$id] = $tag[0];
            }
        }

        foreach ($taggedServices['service_collector'] as $id => $tag) {
            $tagName = $tag['tag'];

            if (empty($taggedServices[$tagName])) {
                continue;
            }

            try {
                $collector = $container->getDefinition($id);
                $method = $tag['call'] ?? 'addHandler';
                $params = $this->getAddHandlerParams($collector, $method);

                foreach ($taggedServices[$tagName] as $serviceId => $values) {
                    $args = [new Reference($serviceId)];

                    foreach ($params as $paramName => $default) {
                        $args[] = $values[$paramName] ?? $default;
                    }
                    $collector->addMethodCall($tag['call'], $args);
                }
            } catch (\ReflectionException $e) {
                // Just skip this service collector. The add handler method
                // is unknown, so we won't be able to collect services.
            }
        }
    }

    /**
     * Gets the add handler parameters (excluding the first parameter).
     *
     * The returned array has the parameter names as the array keys, and the
     * parameter default values as the array values. These parameter names
     * should in theory match the service tag option to the services being collected.
     *
     * [
     *   'priority' => 0,
     *   'category' => 'default',
     * ];
     *
     * Will match up with tag options with the same property names.
     *
     * @param Definition $definition The service definition of the collector service.
     * @param string     $method     The collector service's add handler method name.
     *
     * @return array<string, mixed> An array with the names of the add handler method parameters and the parameter's
     *                              default value as the array keys and values respectively. The first parameter is
     *                              skipped because this will always be a reference to the tagged service.
     */
    protected function getAddHandlerParams(Definition $definition, string $method): array
    {
        $class = new \ReflectionClass($definition->getClass());
        $addHandler = $class->getMethod($method);
        $params = $addHandler->getParameters();

        // The first parameter for the service add handler, should always be
        // the service being added.
        array_shift($params);

        // Match the rest of the params to the options in the tag definition.
        foreach ($params as $param) {
            $args[$param->getName()] = $param->isDefaultValueAvailable() ? $param->getDefaultValue() : null;
        }

        return $args ?? [];
    }
}
