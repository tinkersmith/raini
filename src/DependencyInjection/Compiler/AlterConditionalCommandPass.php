<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\DependencyInjection\Compiler;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LazyCommand;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Alter LazyCommand definitions if they are conditionally enabled.
 *
 * The container compiler pass "AddConsoleCommandPass" adds LazyCommand
 * definitions for commands that have default definitions and can have deferred
 * instantiation, however, it doesn't create definifitions that call
 * Command::isEnabled(), and assumes all commands with default descriptions are
 * always enabled.
 *
 * This compiler pass checks if the command class overrides Command::isEnabled()
 * and updates the LazyCommand container definition arguments are "NULL" for
 * "isEnable" so the underlying command method is called.
 *
 * @see \Symfony\Component\Console\Command\LazyCommand::isEnabled()
 */
class AlterConditionalCommandPass implements CompilerPassInterface
{

    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container): void
    {
        try {
            $locatorId = $container->getDefinition('console.command_loader')->getArgument(0);
            $locatorDef = $container->getDefinition($locatorId);

            // CommandLoad is initialized with a ServiceLocator that maps the
            // LazyCommand definitions to the original Command services.
            if (is_a($locatorDef->getClass(), ServiceLocator::class, true)) {
                foreach ($locatorDef->getArgument(0) as $name => $closure) {
                    $cmdDef = $container->getDefinition($name);
                    $mappedId = (string) $closure->getValues()[0];
                    $method = new \ReflectionMethod($cmdDef->getClass(), 'isEnabled');

                    // If the command has overridden the Command::isEnabled()
                    // method, then we alter the the lazy command definition to
                    // ensure the method is called.
                    if ($mappedId !== $name && Command::class !== $method->getDeclaringClass()) {
                        $mappedDef = $container->getDefinition($mappedId);
                        $this->alterLazyCommand($mappedDef);
                    }
                }
            }
        } catch (ServiceNotFoundException) {
            // Unable to alter update commands because either none have been
            // setup or the definitions aren't available.
        }
    }

    /**
     * Alters a LazyCommand service definition to ensure it calls isEnabled().
     *
     * @param Definition $definition A container service LazyCommand definition to alter.
     */
    protected function alterLazyCommand(Definition $definition): void
    {
        $class = $definition->getClass();

        if (!is_a($class, LazyCommand::class, true)) {
            return;
        }

        $args = $definition->getArguments();
        $constructor = (new \ReflectionClass($class))->getConstructor();

        foreach ($constructor->getParameters() as $param) {
            if ('isEnabled' === $param->getName()) {
                $args[$param->getPosition()] = null;
                $definition->setArguments($args);
                break;
            }
        }
    }
}
