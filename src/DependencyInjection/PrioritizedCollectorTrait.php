<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\DependencyInjection;

/**
 * Trait for tagged service collectors which are ordered by priority values.
 *
 * Tagged service collectors using this path should define an $interface
 * property, with the interface that handlers need to implement. This allows
 * the service to check for valid handler.
 */
trait PrioritizedCollectorTrait
{

    /**
     * Info about handlers which have been registered to the this collection.
     *
     * @var mixed[]
     */
    protected $handlers = [];

    /**
     * A list of handlers which are sorted by priority (ascending order).
     *
     * @var array<object>|null
     */
    protected $sorted;

    /**
     * Compare the priority between $a and $b.
     *
     * The method is meant to be used with PHP sort functions (i.e. "uasort()").
     *
     * @param mixed[] $a An array with the service handler and its priority to compare with $b.
     * @param mixed[] $b An array with a second service handler to compare priority against $a.
     *
     * @return int Return a number greater than 0 if $b has a higher priority than $a, a negative number if $a has a
     *             higher priority than $b, and 0 if the priority is equal.
     */
    public static function compareHandlerPriority(array $a, array $b): int
    {
        return $a['priority'] <=> $b['priority'];
    }

    /**
     * Add a service to the list of handlers.
     *
     * @param mixed $handler  The service handler being added. Handler should implement the interface of type described
     *                        by the $interface property.
     * @param int   $priority A sorting priority for the service being added. Lower numbers will be executed earlier.
     */
    public function addHandler($handler, int $priority = 0): void
    {
        if (isset($this->interface) && !$handler instanceof ($this->interface)) {
            $error = sprintf('Tagged service must be of type %s to be added to service collector %s', $this->interface, static::class);
            throw new \InvalidArgumentException($error);
        }

        $data = [
            'handler' => $handler,
            'priority' => $priority,
        ];

        if ($handler instanceof ServiceInspectionInterface) {
            $this->handlers[$handler->getServiceId()] = $data;
        } else {
            $this->handlers[] = $data;
        }

        // Reset the sorted handlers so they can be regenerated.
        $this->sorted = null;
    }

    /**
     * @return array<object> An array of service handlers sorted by their priority.
     */
    protected function getSortedHandlers(): array
    {
        if (!isset($this->sorted)) {
            uasort($this->handlers, static::class.'::compareHandlerPriority');
            $this->sorted = array_column($this->handlers, 'handler');
        }

        return $this->sorted;
    }
}
