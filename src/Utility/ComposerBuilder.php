<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Utility;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Tinkersmith\Console\Cli;

/**
 * Helper class to assist with building, and altering a composer.json file.
 *
 * @implements \ArrayAccess<string, mixed>
 */
final class ComposerBuilder implements \ArrayAccess
{

    const JSON_OUTPUT_FLAGS = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_INVALID_UTF8_IGNORE|JSON_UNESCAPED_SLASHES;

    /**
     * @var bool Indicates that $data has been altered and the composer.json file needs to be updated.
     */
    protected bool $isDirty = true;

    /**
     * @var OutputInterface|null Output to write messages to user if available.
     */
    protected ?OutputInterface $output;

    /**
     * @param string               $filepath The path to the composer.json file linked to this object (read/write file).
     * @param mixed[]              $data     The JSON data for the composer.json source file.
     * @param OutputInterface|null $output
     */
    public function __construct(protected string $filepath, protected array $data, ?OutputInterface $output = null)
    {
        if (!preg_match('#(/|\\\\)composer.json$#', $filepath)) {
            throw new \InvalidArgumentException('ComposerBuilder can only work with files named "composer.json".');
        }

        $this->setOutput($output);
    }

    /**
     * Creates a new ComposerBuilder with data loaded from composer.json file.
     *
     * @param string               $filepath Path to the composer.json file.
     * @param OutputInterface|null $output   Output to write status and user messages to.
     *
     * @return self
     */
    public static function createFromFile(string $filepath, ?OutputInterface $output = null): self
    {
        $content = static::getFileData($filepath);

        $instance = new static($filepath, $content, $output);
        $instance->isDirty = false;

        return $instance;
    }

    /**
     * @return string The path to the composer.json file relative to the project directory.
     */
    public function getComposerFilepath(): string
    {
        return $this->filepath;
    }

    /**
     * @param OutputInterface|null $output
     *
     * @return self
     */
    public function setOutput(?OutputInterface $output): self
    {
        $this->output = $output;

        return $this;
    }

    /**
     * Adds repositories to Composer, if they aren't already included.
     *
     * The repository types and formats are describe in the Composer
     * documentation https://getcomposer.org/doc/05-repositories.md. Each
     * repository should be uniquely named (array key) and at a minimum, have
     * values for "type" and "url" keys.
     *
     * Repository types can be one of "composer", "path", "vcs" or "package".
     *
     * @param mixed[] $repositories Additional Composer supported repositories to add.
     *
     * @return self
     *
     * @see https://getcomposer.org/doc/05-repositories.md
     */
    public function addRepositories(array $repositories): self
    {
        $installed = array_fill_keys(['composer', 'package', 'path', 'vcs'], []);
        foreach ($this->data['repositories'] ?? [] as $repo) {
            $installed[$repo['type']][] = ('package' === $repo['type'])
                ? $repo['package']['name'] : $repo['url'];
        }

        // Add package if they haven't already been added.
        foreach ($repositories as $name => $repo) {
            $type = $repo['type'];
            $id = ('package' === $type) ? $repo['package']['name'] : $repo['url'];

            if (empty($this->data['repositories'][$name]) && !in_array($id, $installed[$type])) {
                $this->data['repositories'][$name] = $repo;
                $installed[$type][] = $id;
                $this->isDirty = true;
            }
        }

        return $this;
    }

    /**
     * Add packages to the Composer project.
     *
     * Since packages need to be found in the available repository, the value
     * suggested version of the package needs to be discovered, this method
     * actually calls the Composer on a CLI process. The command tries to do the
     * minimal to add the composer.json "require" information and updates the
     * lock file.
     *
     * This method can be expensive if intermixed with other types of composer
     * configuration changes, so it's recommended to group calls to this method
     * if possible.
     *
     * @param string[] $packages List of Composer packages as they would be passed on the commandline. This means that
     *                           each value of the array can just be the package name, or the package name and a version
     *                           constraint (i.e. "drupal/core" or "drupal/core:^9.4").
     * @param bool     $isDev    Should the packages being added be added to "require-dev".
     *
     * @return self
     */
    public function addPackages(array $packages, bool $isDev = false): self
    {
        $command = ['require', '-W', '--no-install', '--no-scripts', '--no-plugins'];
        if ($isDev) {
            $installed = $this->data['require-dev'] ?? [];
            $command[] = '--dev';
        } else {
            $installed = $this->data['require'] ?? [];
        }

        $require = [];
        foreach ($packages as $package) {
            [$packageName, $constraint] = explode(':', $package, 2) + [1 => '*'];

            // @todo Compare package version or constraints for conflicts if
            // multiple extensions are requesting different versions of the
            // same packages.
            if (!isset($installed[$packageName])) {
                $require[] = $package;

                // Ensures same package is not added multiple times.
                $installed[$packageName] = $constraint;
            }
        }

        if ($require) {
            if ($this->isDirty) {
                $this->writeFile();
            }

            // @todo determine if there is a dependency injection way to do this.
            $process = new Cli('composer', dirname($this->filepath));
            $process
                ->setTty(true)
                ->setTimeout(null)
                ->execute([...$command, ...$require], $this->output);

            $this->data = static::getFileData($this->filepath);
        }

        return $this;
    }

    /**
     * Add Composer extra configurations to the composer.json settings.
     *
     * @param mixed[] $extras    Extra configurations, keyed by the extra configuration name.
     * @param bool    $overwrite Should extra values that already exist be overwritten?
     *
     * @return self
     */
    public function addExtras(array $extras, bool $overwrite = false): self
    {
        if (!$overwrite) {
            $extras = array_diff_key($extras, $this->data['extra'] ?? []);
        }

        if ($extras) {
            $this->data['extra'] = $extras + ($this->data['extra'] ?? []);
            $this->isDirty = true;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($this->data[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->data[$offset];
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        if ('require' === $offset || 'require-dev' === $offset) {
            throw new \LogicException('ComposerBuilder cannot directly alter the packages (require or require-dev). Use the ComposerBuilder::addPackages() method instead.');
        }

        // Only mark as "dirty" and alter the values if the have changed.
        // This prevents unnecessary composer.json file writes if nothing was changed.
        if (!isset($this->data[$offset]) || $value !== $this->data[$offset]) {
            $this->data[$offset] = $value;
            $this->isDirty = true;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset(mixed $offset): void
    {
        if ('require' === $offset || 'require-dev' === $offset) {
            throw new \LogicException('ComposerBuilder cannot directly alter the packages (require or require-dev). Use the ComposerBuilder::addPackages() method instead.');
        }

        // Only mark as "dirty" if there was an actual value to unset.
        if (isset($this->data[$offset])) {
            unset($this->data[$offset]);
            $this->isDirty = true;
        }
    }

    /**
     * Update composer.json to match the contents of the $data property.
     *
     * Only writes the content if the $isDirty property is true.
     *
     * @return self
     */
    public function writeFile(): self
    {
        if ($this->isDirty) {
            $fs = new Filesystem();
            $fs->dumpFile($this->filepath, json_encode($this->data, self::JSON_OUTPUT_FLAGS));

            // Current file state has been written, which means the data and file are in sync.
            $this->isDirty = false;
        }

        return $this;
    }

    /**
     * Write messages to an output stream if an output instance has been set.
     *
     * @param string $buffer The text to write to the output if an output stream is set.
     *
     * @return self
     */
    protected function writeOutput(string $buffer): self
    {
        if ($this->output) {
            $this->output->write($buffer);
        }

        return $this;
    }

    /**
     * Extract the JSON values from the file contents at the specified filepath.
     *
     * @param string $filepath
     *
     * @return mixed[] The file JSON contents as a PHP associative array.
     */
    protected static function getFileData(string $filepath): array
    {
        if ($content = file_get_contents($filepath)) {
            return json_decode($content, true, 64, JSON_INVALID_UTF8_IGNORE) ?? [];
        }

        return [];
    }
}
