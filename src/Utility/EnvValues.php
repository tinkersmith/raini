<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Utility;

use Symfony\Component\Filesystem\Exception\IOException;

/**
 * Utility class for loading or writing to environment files.
 *
 * @implements \ArrayAccess<string, mixed>
 * @implements \Iterator<string, mixed>
 */
final class EnvValues implements \ArrayAccess, \Iterator
{

    /**
     * Accepted escape sequences that are recognized in the env variables.
     */
    const ESC_SEQUENCES = [
        'e' => "\e",
        'f' => "\f",
        'n' => "\n",
        'r' => "\r",
        't' => "\t",
        'v' => "\v",
        '\\' => '\\',
    ];

    /**
     * The values read from or for storing into an env file.
     *
     * @var array<string, mixed>
     */
    protected $values = [];

    /**
     * Create a new EnvValues instance with values loaded from an env file.
     *
     * @param string $path The path to the env file to parse and read values from.
     *
     * @return static The EnvValues instance with the environment variables loaded from the env file.
     */
    public static function createFromFile(string $path): static
    {
        $instance = new static();
        if ($stream = fopen($path, 'r')) {
            try {
                $fetchLines = function () use ($stream): \Generator {
                    while (($line = fgets($stream)) !== false) {
                        yield $line;
                    }

                    return null;
                };

                $instance->buildFromContent($fetchLines(), $path);
            } finally {
                fclose($stream);
            }
        }

        return $instance;
    }

    /**
     * Extract and load an EnvValues instance with values from env text content.
     *
     * Each environment variable should be on it's own line as it would normally
     * be defined by .env files or returned from the "env" Unix command.
     *
     * @param string $content The string content of the environment variables to parse.
     *
     * @return static The EnvValues instance with the environment variables loaded from the text.
     */
    public static function createFromText(string $content): static
    {
        $instance = new static();
        $iterator = new \ArrayIterator(preg_split("/\r\n|\n|\r/", $content));
        $instance->buildFromContent($iterator, 'buffer');

        return $instance;
    }

    /**
     * Parse an environment variable string from text content split into lines.
     *
     * It's possible that a string spans multiple lines if a matching quote is
     * not found by the end of the line, otherwise the string generally ends at
     * the end of the line.
     *
     * @param string    $value  The start of the string value (first line).
     * @param \Iterator $iter   An iterator instance that will provide string content one line at a time. This is used
     *                          in case the string needs to span multiple lines and needs to advance the position.
     * @param string    $path   The path of the file being parsed (for error reporting).
     * @param int       $lineCt Reference to the current line count. Used for error reporting, and needs to be
     *                          maintained, if more lines are read from the filestream.
     *
     * @return string The string value of the environment variable read.
     *
     * @throws IOException Throws an IO exception for file read errors or string doesn't terminate.
     */
    public static function parseEnvString(string $value, \Iterator $iter, string $path, int &$lineCt): string
    {
        $str = '';
        $capture = 0;
        $quote = '["\'#]';

        do {
            $offset = 0;
            while (preg_match("/(.*?)(\\\\([^\"'])|\\\\?({$quote}))/", $value, $matches, 0, $offset)) {
                $str .= $matches[1];

                if (!empty($matches[4])) {
                    if ($matches[2] === $matches[4]) {
                        // Found a comment when not in quoted text capture mode so skip the rest of this line.
                        if ($matches[4] === '#') {
                            break(2);
                        }

                        // If capturing, exit capture mode. Otherwise set capture and look for the matching quote.
                        $quote = ($capture ? '["\'#]' : $matches[4]);
                        $capture = !$capture;
                    } else {
                        // Escaped quote, add to string content.
                        $str .= $matches[4];
                    }
                } elseif (isset(self::ESC_SEQUENCES[$matches[3]])) {
                    $str .= self::ESC_SEQUENCES[$matches[3]];
                } else {
                    throw new IOException(sprintf('Unknown escape sequece "\\%s" on line: %u in file: %s', $matches[3], $lineCt, $path));
                }

                $offset += strlen($matches[0]);
            }

            // Add the rest of the string to the value.
            $str .= substr($value, $offset);

            // If capturing quoted text read the next line of the ENV file.
            if ($capture) {
                $iter->next();

                if (null === ($value = $iter->current())) {
                    throw new IOException(sprintf('Unable to find matching %s closing quote, while parsing file: %s', $quote, $path));
                }
                ++$lineCt;
            }
        } while ($capture);

        return rtrim($str);
    }

    /**
     * Format a string to be writeable as an environment variable value.
     *
     * @param string $str String value to escape and prepare to be written to env file.
     *
     * @return string The string value formatted and ready to be written as the value of an environment variable.
     */
    public static function formatEnvString(string $str): string
    {
        $escSeq = [
          '\\' => '\\\\',
          '"' => '\\"',
        ];

      // Add to the list of characters which need to be escaped, aside from
      // the quote and backslash.
        foreach (self::ESC_SEQUENCES as $esc => $value) {
            $escSeq[$value] = '\\'.$esc;
        }
        unset($escSeq["\n"]);

        return '"'.strtr($str, $escSeq).'"';
    }

    /**
     * Write env file with current values to the specified file path.
     *
     * @param string $path   The path to the file to write with the variables.
     * @param string $header A file header to include if provided.
     */
    public function write(string $path, string $header = ''): void
    {
        if (!($filestream = fopen($path, 'w+'))) {
            throw new IOException("Unable to open file {$path} for writing.");
        }

        try {
            if (!empty($header)) {
                fputs($filestream, preg_replace("/(^|\n)(?!#)/", "$1# ", $header));
                fputs($filestream, "\n#------------------------------------------------------------------------------#\n\n");
            }

            foreach ($this->values as $key => $value) {
                $line = $key.'=';

                if (is_string($value)) {
                    $line .= static::formatEnvString($value);
                } elseif (is_bool($value)) {
                    $line .= $value ? 'true' : 'false';
                } elseif (is_array($value)) {
                    $formatted = array_map(static::class.'::formatEnvString', $value);
                    $line .= '('.implode(' ', $formatted).')';
                } else {
                    $line .= $value;
                }

                $line .= "\n";
                if (fputs($filestream, $line) === false) {
                    throw new IOException("Unable to write variable {$key} to file {$path}.");
                }
            }
        } finally {
            fclose($filestream);
        }
    }

    //---------------------------//
    // Implement \Iterator
    //---------------------------//

    /**
     * {@inheritdoc}
     */
    public function key(): mixed
    {
        return key($this->values);
    }

    /**
     * {@inheritdoc}
     */
    public function current(): mixed
    {
        return current($this->values);
    }

    /**
     * {@inheritdoc}
     */
    public function valid(): bool
    {
        /** @var mixed $key */
        $key = $this->key();

        return null === $key;
    }

    /**
     * {@inheritdoc}
     */
    public function next(): void
    {
        next($this->values);
    }

    /**
     * {@inheritdoc}
     */
    public function rewind(): void
    {
        reset($this->values);
    }

    //---------------------------//
    // Implements \ArrayAccess
    //---------------------------//

    /**
     * {@inheritdoc}
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($this->values[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->values[$offset];
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->values[$offset] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->values[$offset]);
    }

    /**
     * Parse the environment variables from the text definitions.
     *
     * @param \Iterator $lines  An iterator that steps through the lines of the "env" content.
     * @param string    $source Label for the source of this data. Used to report errors.
     */
    protected function buildFromContent(\Iterator $lines, string $source): void
    {
        $lineCt = 0;
        while (null !== ($line = $lines->current())) {
            ++$lineCt;

            if (preg_match('#^\s*([a-z_][a-z0-9_]+)=(.+)$#is', (string) $line, $matches)) {
                [, $key, $value] = $matches;

                // Test if the value is a number.
                if (preg_match('/^(-|\+)?(\d*(\.\d+)?|\d+\.?)(e-?\d+)?(?:\s*#|$)/i', $value, $parts)) {
                    $value = empty($parts[3]) && empty($parts[4]) ? intval($parts[0]) : doubleval($parts[0]);
                } elseif (preg_match('/^(true|false)(\s*#|$)/i', $value, $parts)) {
                    // Value is a boolean, either TRUE or FALSE.
                    $value = strtolower($parts[1]) === 'true' ? true : false;
                } else {
                    // Otherwise the value is a string - potentially multiple lines.
                    $value = static::parseEnvString($value, $lines, $source, $lineCt);
                }

                $this[$key] = $value;
            }
            $lines->next();
        }
    }
}
