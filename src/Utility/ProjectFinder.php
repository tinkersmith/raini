<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Utility;

use Raini\Core\Environment;
use Raini\Core\Exception\EnvFileNotFoundException;

/**
 * Class which locates the base project path.
 *
 * The base project directory is marked by the ENV file (.raini.env). This
 * utility will search for this file in the current directory and all its
 * parent directories.
 */
final class ProjectFinder
{

    /**
     * Finds the path to the base project directory.
     *
     * @param string|null $path The current path, or path to start the search from.
     *
     * @return string The path of the base project directory.
     *
     * @throws EnvFileNotFoundException If not able to locate the base project directory.
     */
    public static function find(?string $path = null): string
    {
        // Normally set by the Raini launcher shell script.
        if ($envPath = getenv('RAINI_PROJECT_HOME')) {
            $path = $envPath;
        } elseif (empty($path)) {
            $path = getcwd();
        }

        // Search for the Raini ENV file to indicate the root of the project.
        while (!file_exists(Environment::getEnvFilePath($path))) {
            if (\DIRECTORY_SEPARATOR === $path || empty($path)) {
                throw new EnvFileNotFoundException(getcwd(), Environment::ENV_FILE_PATH);
            }

            $path = dirname($path);
        }

        return $path;
    }
}
