<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Devel;

use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * Interface code standards that need to customize the execution of the linter.
 *
 * This allows a code standard implementation to take over the execution of the
 * CodeSniffer / code linting process. This ::execute method gets called instead
 * of the defult CodeStandardManagerInterface::execute() method.
 *
 * @see CodeStandardManagerInterface::execute()
 */
interface ExecutableStandardInterface extends CodeStandardInterface
{

    /**
     * Execute the PHP CodeSniffer command.
     *
     * Used instead of the normal CodeStandardManagerInterface::execute()
     * invocation. Should be reserved for when the execution of this standard
     * requires customization.
     *
     * @param string                                              $standard The coding standard use with the CodeSniffer execution.
     * @param PathInfo[]|PathInfo                                 $paths    A path or an array of paths to run the code linter on.
     * @param Tenant                                              $tenant   The project tenant the CodeSniffer is run on.
     * @param string[]                                            $options  The CodeSniffer options to run the command with.
     * @param EnvironmentInterface|ExecutionContextInterface|null $context  The environment execution context to run the command with.
     *
     * @return int The CLI execution return code (0 means no errors, and any other value is an exit error code).
     */
    public function execute(string $standard, array|PathInfo $paths, Tenant $tenant, array $options = [], EnvironmentInterface|ExecutionContextInterface|null $context = null): int;
}
