<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Devel;

use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;

/**
 * Service collector to manage Tester implementations (to run tests).
 */
interface AnalyzerManagerInterface
{

    /**
     * Determine what tester should be used to execute testing base on a path.
     *
     * @param PathInfo|PathInfo[] $paths  Information about the path (filepath, path type), to target for testing.
     * @param Tenant              $tenant The tenant to run the static analysis on.
     *
     * @return AnalyzerInterface|null The tester to use on the provided info if a matching tester can be determined.
     */
    public function getAnalyzerByPath(PathInfo|array $paths, Tenant $tenant): ?AnalyzerInterface;
}
