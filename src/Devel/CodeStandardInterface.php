<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Devel;

use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;

/**
 * Interface for classes that provide coding standards for PHP CodeSniffer.
 */
interface CodeStandardInterface
{

    /**
     * Gets coding standards made available by this CodeStandard class.
     *
     * Informationg about standards is returned as an array with the keys as the
     * identifier of the standard, and the values as arrays with the following
     * format:
     *     [
     *         "label" => <display name>,
     *         "value" => <CodeSniffer Standard Argument>,
     *     ]
     *
     * The "label" can be displayed in the help text and the "value" should be
     * the argument used with the CodeSniffer "--standard" argument. This can be
     * either a path to the sniffs to use, or the identifier for the code
     * standard.
     *
     * @return array<string, string[]> Information about coding standards to include.
     */
    public function getInfo(): array;

    /**
     * Guess a coding standard to use base on information about the path.
     *
     * @param PathInfo $pathInfo Information about the path to be evaluated, including a path type.
     * @param Tenant   $tenant   The tenant which manages the path to run the code sniffer on.
     *
     * @return string|null The coding standard identifier if this CodeStandard object was able to pick a standard
     *                     that applies to the target path. NULL is returned if no standard can be determined.
     */
    public function getStandardByPath(PathInfo $pathInfo, Tenant $tenant): ?string;

    /**
     * Gets the CLI command to execute for the
     *
     * @param Tenant   $tenant  The tenant the code linter command is being run for.
     * @param string[] $options The CodeSniffer options to execute with.
     *
     * @return string|string[] The CLI command to execute that is compatible with CliInterface.
     */
    public function getCommand(Tenant $tenant, array $options = []): string|array;

    /**
     * Get the value to pass to CodeSniffer as the standard.
     *
     * The coding standard can be a string name of the standard or a path to a
     * standards definition XML file.
     *
     * @param string $standard The internal standard identifier for the standard to get the value for.
     * @param Tenant $tenant   The tenant that the CLI command is being run on.
     *
     * @return string The coding standards ID to pass to the CodeSniffer CLI command.
     */
    public function getStandardValue(string $standard, Tenant $tenant): string;
}
