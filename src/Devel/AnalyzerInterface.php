<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Devel;

use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * The code analyzer handler interface.
 *
 * Handlers run static code analysis commands to a target path.
 */
interface AnalyzerInterface
{

    /**
     * @return string The human friendly analyzer name or identifier.
     */
    public function getName(): string;

    /**
     * @return string The analyzer description.
     */
    public function getDescription(): string;

    /**
     * @param PathInfo $path   The target path to run tests on.
     * @param Tenant   $tenant
     *
     * @return bool Is the path specified applicable to this tester?
     */
    public function isPathApplicable(PathInfo $path, Tenant $tenant): bool;

    /**
     * Execute the test for the target path.
     *
     * @param PathInfo[]|PathInfo                            $path    Target path to execute the tests on.
     * @param Tenant                                         $tenant  The tenant to execute the analysis on.
     * @param EnvironmentInterface|ExecutionContextInterface $context The execution context or environment to run this command on.
     * @param mixed[]                                        $options The command options for the analyze execution.
     */
    public function execute(array|PathInfo $path, Tenant $tenant, EnvironmentInterface|ExecutionContextInterface $context, array $options): int;
}
