<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Devel;

use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * Interface for services that manages code standard handlers.
 *
 * Code standard handlers provide information and selection of PHP coding
 * standards.
 */
interface CodeStandardManagerInterface
{

    /**
     * @return array<string, string[]> An array of all the PHP coding standards available.
     *
     * @see \Raini\Standard\CodeStandardInterface::getInfo()
     */
    public function getAvailableStandards(): array;

    /**
     * Get a code standard handler to use with the provided standard.
     *
     * @param string $standard The code standard identifier to find the name for.
     *
     * @return CodeStandardInterface|null The code standard instance that should be used with the request code standard.
     */
    public function getHandlerByStandard(string $standard): ?CodeStandardInterface;

    /**
     * From the path, guess what the correct coding standard should be used.
     *
     * Generally this is used when the user is not explicit about which coding
     * standard to use, and is leaving it up to Raini to select one.
     *
     * @param PathInfo[]|PathInfo $pathInfo The resolved path info to use to determine the coding standard should be used.
     * @param Tenant              $tenant   The tenant the CS command will be run on.
     *
     * @return array<mixed>|null The identifier for the coding standard and the matching standard handler to use for
     *                           this path. NULL is returned if no match is available.
     *
     * @see \Raini\File\PathResolver::resolve()
     */
    public function getStandardByPath(array|PathInfo $pathInfo, Tenant $tenant): ?array;

    /**
     * Execute the PHP CodeSniffer command.
     *
     * @param PathInfo[]|PathInfo                                 $paths    A path or an array of paths to run the code linter on.
     * @param Tenant                                              $tenant   The project tenant the CodeSniffer is run on.
     * @param CodeStandardInterface|null                          $handler  The code standard handler to execute with.
     * @param string|null                                         $standard The coding standard use with the CodeSniffer execution.
     * @param string[]                                            $options  The CodeSniffer options to run the command with.
     * @param EnvironmentInterface|ExecutionContextInterface|null $context  The environment execution context to run the command with.
     *
     * @return int The CLI execution return code (0 means no errors, and any other value is an exit error code).
     */
    public function execute(array|PathInfo $paths, Tenant $tenant, ?CodeStandardInterface $handler = null, ?string $standard = null, array $options = [], EnvironmentInterface|ExecutionContextInterface|null $context = null): int;
}
