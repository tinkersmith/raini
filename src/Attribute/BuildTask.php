<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Attribute;

/**
 * Attribute for marking classes as implementing a build task.
 *
 * The attribute will in the future be used to mark build tasks and provider
 * the necessary info for build task discovery. This will make it easier for
 * developers to tag their classes instead of having to update the
 * "raini.services.yml" services file.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class BuildTask
{

    /**
     * @param string $name        The name of the build task (identifier).
     * @param string $description A description of the build task and what is it for.
     * @param int    $priority    A sorting priorty to help determine the order build tasks are run. Higher values
     *                            runs first.
     */
    public function __construct(public readonly string $name, public readonly string $description, public readonly int $priority = 0)
    {
    }
}
