<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File;

/**
 * Represents information about a path.
 *
 * Path information includes a base path, a resource path, and a path type.
 * PathInfo objects are normally generated and managed by using path resolvers.
 *
 * The PathInfo represents either an absolute path (not recommended) or a path
 * relative to the project directory.
 *
 * The path type information helps to identify information about the path that
 * has already been discovered by a path resolver, and utilized by other Raini
 * functionality like the \Raini\Devel\CodeStandardarManager using
 * this to determine which coding standard should be applied to this path.
 *
 * @see \Raini\File\PathResolver::resolve()
 */
#[\AllowDynamicProperties]
class PathInfo implements \Stringable
{

    const ABSOLUTE_PATH = 'absolute';
    const PROJECT_PATH = 'project';

    protected string $base;
    protected string $path;

    /**
     * @param string $type    The path type.
     * @param string $path    The path relative to a base path.
     * @param string $base    The base path, which should be consistent for the path type. The path type rules and
     *                        path formats are normally determined by a path resolver which handles that path type.
     * @param bool   $makeAbs IFF true any project relative path is made absolute in a target execution environment.
     *                        This allows the path to be defined relative to the project, but made absolute when used
     *                        inside of a Docker container. This is critical for using a path which need to be absolute
     *                        but maybe used in multiple environments.
     */
    public function __construct(protected string $type, string $path, string $base = '', protected bool $makeAbs = false)
    {
        $this->__set('base', $base);
        $this->__set('path', $path);
    }

    /**
     * Implements magic method to get a instance property.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->{$name};
    }

    /**
     * Sets the value of a instance property.
     *
     * @param string $name
     * @param mixed  $value
     */
    public function __set(string $name, $value): void
    {
        switch ($name) {
            case 'base':
                if (!empty($value) && !str_ends_with($value, '/')) {
                    $value .= '/';
                }
                // Intentional fall-thru.
            case 'type':
            case 'makeAbs':
                $this->{$name} = $value;
                break;

            case 'path':
                if (str_starts_with($value, '/')) {
                    // Absolute path. Remove the leading and trailing slash and
                    // update the base path.
                    $value = trim($value, '/');
                    $this->base = '/';
                } else {
                    // Only remove the trailing slash.
                    $value = rtrim($value, '/');
                }

                $this->{$name} = $value;
                break;

            default:
                $err = sprintf('Cannot set property %s for class of type %s.', $name, static::class);
                trigger_error($err, E_USER_ERROR);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return "{$this->base}{$this->path}";
    }

    /**
     * Is this path info empty?
     *
     * Path info is considered empty when the path and the base are both empty.
     *
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->path) && empty($this->base);
    }

    /**
     * Returns a bool to indicate if a path should be made absolute.
     *
     * @return bool Should this path be made absolute in environment contexts?
     */
    public function isAbs(): bool
    {
        return $this->makeAbs;
    }

    /**
     * @return string The path type.
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string The string representation of the filesystem path.
     *
     * @see self::__toString()
     */
    public function getFullpath(): string
    {
        return (string) $this;
    }
}
