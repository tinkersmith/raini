<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File;

use Raini\Core\Project\Tenant;

/**
 * The default path resolver handles the "vendor" path type or a plain filepath.
 */
class DefaultPathResolver implements PathResolverInterface
{

    /**
     * {@inheritdoc}
     */
    public function resolve(string $path, string $type, Tenant $tenant): ?PathInfo
    {
        switch ($type) {
            case PathInfo::PROJECT_PATH:
                $path = ltrim($path, '/');
                // Intentional fall-thru.
            case PathInfo::ABSOLUTE_PATH:
                return new PathInfo($type, $path);
        }

        return null;
    }
}
