<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File;

use Raini\Core\Extension\Exception\ExtensionNotFoundException;
use Raini\Core\Extension\ExtensionList;
use Raini\Core\Project\Tenant;

/**
 * Path resolver that provides paths to extensions and droplets.
 *
 * Adds the path types for "extension" and "droplet" to allow paths to be
 * relative to a Raini extension directory. Currently this class needs to be
 * statically initialized with extension list service.
 */
class ExtensionPathResolver implements PathResolverInterface
{

    /**
     * @param ExtensionList $extensions
     */
    public function __construct(protected ExtensionList $extensions)
    {
    }

    /**
     * The general extension path builder.
     *
     * @param string $extId The extension ID to base the path from.
     * @param string $path  The path relative to the extension, with the empty to mean the root of the extension.
     * @param string $type  Either "droplet" or "extension" to specify the extension type. "extension" checks for both.
     *
     * @return PathInfo A path instance going to the extension path.
     *
     * @throws ExtensionNotFoundException If the requested path is not available because the extension is missing.
     */
    public function extensionPath(string $extId, string $path = '', string $type = 'extension'): PathInfo
    {
        $extDef = $this->extensions->getDroplet($extId);
        if ('extension' === $type && !$extDef) {
            $extDef = in_array($extId, ['core', 'raini'])
                ? $this->extensions->getCore()
                : $this->extensions->getExtensionDefinition($extId);
        }
        if (!$extDef) {
            throw new ExtensionNotFoundException($extId);
        }

        return new PathInfo(PathInfo::PROJECT_PATH, $path, $extDef->getPath());
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(string $path, string $type, Tenant $tenant): ?PathInfo
    {
        switch ($type) {
            case 'droplet':
            case 'extension':
                [$extId, $path] = explode('/', ltrim($path, '/'), 2) + [
                    'core',
                    '',
                ];

                return $this->extensionPath($extId, $path, $type);
        }

        return null;
    }
}
