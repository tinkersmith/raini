<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File;

use Raini\Core\Project\Tenant;

/**
 * Base interface for path resolvers.
 *
 * Path resolvers are a collection of tagged services which add resolution of
 * path from command arguments. This allows path prefixes as shorthand to common
 * file locations such as "vendor:tinkersmith/raini" as an argument to a command
 * which can resolve to the Raini Composer package in the vendor directory.
 *
 * The path resolvers are tagged services with the tag "path_resolver" and
 * implement this interface. This allows extension packages to define resolvers
 * for their specific use cases like "module:toolshed" for the "raini-drupal"
 * package to target the Drupal Toolshed module.
 */
interface PathResolverInterface
{

    /**
     * Resolve the a path (relative to the project directory).
     *
     * Resolving the path will convert a string path to a PathInfo object. This
     * classifies the path type (vendor, bin, etc...) and separates the base
     * path and filepath.
     *
     * @param string $path   The path should be processed by PathHelper::reducePath().
     * @param string $type   The path type of the path provided.
     * @param Tenant $tenant The project tenant path resolution is for.
     *
     * @return PathInfo|null The path information if the resolver can resolve this path; otherwise returns NULL.
     *
     * @see \Raini\File\PathHelper::reducePath()
     * @see \Raini\File\PathResolver
     */
    public function resolve(string $path, string $type, Tenant $tenant): ?PathInfo;
}
