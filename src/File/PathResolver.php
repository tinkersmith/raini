<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File;

use Raini\Core\DependencyInjection\PrioritizedCollectorTrait;
use Raini\Core\Project\Tenant;

/**
 * The path resolver service collector.
 *
 * Collects and manages the path resolver tagged services.
 */
class PathResolver
{

    use PrioritizedCollectorTrait;

    /**
     * The namespaced interface for all services the path resolver can collect.
     *
     * @var string
     */
    protected string $interface = PathResolverInterface::class;

    /**
     * @param PathHelper $helper The PathHelper instance to help make paths more consistent and workable.
     */
    public function __construct(protected PathHelper $helper)
    {
    }

    /**
     * @return bool Is the current path the project base path?
     */
    public function isBasePath(): bool
    {
        return $this->helper->isBasePath();
    }

    /**
     * Generates a PathInfo object from a path string.
     *
     * The path string can be an normal filesystem path, or a path in the
     * "<path-type>:<relative path>" format.
     *
     * If the $path argument is just a normal filesystem path, registered file
     * resolvers will attempt to determine the path type (for instance, if in
     * the vendor directory, then the path info is tagged with a type of
     * "vendor" -- see VendorPathResolver::resolve()).
     *
     * If in the latter format, then the type is known, and the appropriate
     * registered resolver should recognize and build the appropriate PathInfo
     * object. If no PathResolver is available for this type, then NULL.
     *
     * @param string $path    The path to determine the type and path information for.
     * @param Tenant $tenant  The tenant to resolve the paths from.
     * @param bool   $fromCwd If true, relative paths are relative to the CWD of the application invocation, otherwise
     *                        paths are treated as relative to the base project directory.
     *
     * @return PathInfo|null If the path can be resolved, then a PathInfo object with the information about
     *                       this path, including its type. NULL is returned if the path cannot be resolved.
     */
    public function resolve(string $path, Tenant $tenant, bool $fromCwd = true): ?PathInfo
    {
        // Resolve the real filesystem path of the path argument provided.
        $data = explode(':', $path, 2);

        // All direct paths should be resolved and compared relative to the
        // project directory. This allows the path information to be used from
        // the host or from within a container.
        if (empty($data[1])) {
            $type = PathInfo::PROJECT_PATH;
            $path = $this->helper->getRelToProject($data[0], $fromCwd);
        } else {
            $type = $data[0];
            $path = $this->helper::reducePath($data[1]);
        }

        foreach ($this->getSortedHandlers() as $resolver) {
            if ($info = $resolver->resolve($path, $type, $tenant)) {
                return $info;
            }
        }

        return null;
    }
}
