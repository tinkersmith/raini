<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File\Exception;

/**
 * Base exception class for file based errors.
 */
class FileException extends \Exception
{

    const ERR_MESSAGE = 'The path <info>%s</info> is invalid.';

    /**
     * @param string          $path Path to the file that was expected to be a directory.
     * @param int             $code An error code value.
     * @param \Throwable|null $prev A previous exception or throwable in order to chain errors.
     */
    public function __construct(string $path, int $code = 0, ?\Throwable $prev = null)
    {
        $message = sprintf(static::ERR_MESSAGE, $path);

        parent::__construct($message, $code, $prev);
    }
}
