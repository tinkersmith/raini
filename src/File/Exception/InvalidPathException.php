<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File\Exception;

/**
 * Base exception for when a path doesn't exist or can't be resolved.
 */
class InvalidPathException extends \Exception
{

    const ERR_MESSAGE = 'Unable to find or resolve the path: <info>%s</info>.';

    /**
     * @param string          $path Path to the file that was expected to be a directory.
     * @param int             $code An error code value.
     * @param \Throwable|null $prev A previous exception or throwable in order to chain errors.
     */
    public function __construct(string $path, int $code = 0, ?\Throwable $prev = null)
    {
        $message = sprintf(static::ERR_MESSAGE, $path);

        parent::__construct($message, $code, $prev);
    }
}
