<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File\Exception;

use Raini\Core\File\PathInfo;
use Symfony\Component\Console\Command\Command;

/**
 * Except thrown for path info which is not compatible with the Symfony command.
 */
class InvalidCommandPathException extends \Exception
{

    /**
     * @param Command         $command The command which triggered the error.
     * @param string|PathInfo $path    The path which is not compatible with the command.
     * @param string|null     $text    A string template to build the exception message with.
     * @param int             $code    The error code.
     * @param \Throwable|null $prev    A throwable error for exception chaining.
     */
    public function __construct(Command $command, string|PathInfo $path, ?string $text = null, int $code = 0, ?\Throwable $prev = null)
    {
        if (!$text) {
            $text = 'The path <info>"%s"</info> is invalid for the <info>"%s"</info> command';
        }
        $message = sprintf($text, $path, $command->getName());

        parent::__construct($message, $code, $prev);
    }
}
