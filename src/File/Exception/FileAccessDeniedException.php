<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File\Exception;

/**
 * Exception if unable to perform file operations due to file permissions.
 */
class FileAccessDeniedException extends FileException
{

    const ERR_MESSAGE = 'The missing required permissions for path <info>%s</info>.';
}
