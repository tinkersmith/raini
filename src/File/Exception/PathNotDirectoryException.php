<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File\Exception;

/**
 * Exception for when a path exists but is not a directory.
 *
 * Raini projects may need certain paths to be directories, but if a file
 * already exists this will prevent a directory form being created.
 */
class PathNotDirectoryException extends FileException
{

    const ERR_MESSAGE = 'Path <info>%s</info> is not a directory, however, the project expects it to be.';
}
