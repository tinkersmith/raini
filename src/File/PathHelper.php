<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File;

/**
 * Helper class for assisting with common directory tasks.
 *
 * The helper assumes that all paths are using "/" as the directory separator.
 * Most Raini paths should be created and referenced this way, so paths should
 * using other directory separators should transformed to use the "/" as the
 * directory separator before being used with the PathHelper and other Raini
 * path related tooling.
 */
final class PathHelper
{

    /**
     * @param string $cwd        The CLI current working directory.
     * @param string $projectDir Raini base project directory.
     */
    public function __construct(protected string $cwd, protected string $projectDir)
    {
    }

    /**
     * Simplify a path as much as possible.
     *
     * @param string $path The path to attempt to reduce.
     *
     * @return string The simplified path. Relative paths should still stay relative.
     */
    public static function reducePath(string $path): string
    {
        // Clean up the './' at the start or '/./' and '//' in the middle.
        $path = rtrim(preg_replace('#^\./|(/\.|/+)(?=/)#', '', $path), '/');

        $count = 0;
        do {
            $path = preg_replace('#(^|/)[^/.]+/\.\./#', '$1', $path, -1, $count);
        } while ($count);

        return $path;
    }

    /**
     * Is the current execution path the base project path?
     *
     * @return bool TRUE if the cwd and the project directory match, FALSE otherwise.
     */
    public function isBasePath(): bool
    {
        return $this->cwd === $this->projectDir;
    }

    /**
     * Transforms a path to a real absolute path without resolving symlinks.
     *
     * @param string $path    The path to normalize.
     * @param bool   $fromCwd If true, relative paths are relative to the CWD of the application invocation,
     *                        otherwise paths are treated as relative to the base project directory.
     *
     * @return string The real absolute path of the original $path parameter.
     */
    public function normalizePath(string $path, bool $fromCwd = true): string
    {
        if (!str_starts_with($path, '/')) {
            $base = $fromCwd ? $this->cwd : $this->projectDir;
            $path = $base.'/'.$path;
        }

        return self::reducePath($path);
    }

    /**
     * Gets the equivalent path relative to the project directory.
     *
     * Converts the path parameter value to the equivalent path, which is
     * relative to the project path. If path is not in the project directory,
     * then method will return NULL.
     *
     * @param string $path    Path to convert to be relative to project path.
     * @param bool   $fromCwd If true, relative paths are relative to the CWD of the application invocation,
     *                        otherwise paths are treated as relative to the base project directory.
     *
     * @return string The path relative to the project path if the original path parameter was in the project
     *                directory.
     */
    public function getRelToProject(string $path, bool $fromCwd = true): string
    {
        if (!str_starts_with($path, '/')) {
            if (!$fromCwd) {
                return self::reducePath($path);
            }

            $path = $this->cwd.'/'.$path;
        }

        $path = self::reducePath($path);

        if (str_starts_with($path, $this->projectDir)) {
            return substr($path, strlen($this->projectDir) + 1);
        }

        return self::doMakeRelative($path, $this->projectDir);
    }

    /**
     * Compute the relative path from a destination path, from a reference path.
     *
     * Relative paths need to be relative to the CWD or the project base
     * directory. If both paths are relative, they need to be relative to same
     * base path (CWD or Project Directory).
     *
     * A mix of absolute path and relative paths are can be used.
     *
     * @param string $dstPath The path to make relative to the reference path.
     * @param string $refPath The reference path to base the relative path from.
     * @param bool   $fromCwd Are these paths relative to the CWD? If FALSE, relative paths are relative to the
     *                        project base path.
     *
     * @return string The path for $dstPath relative to $refPath.
     */
    public function makeRelative(string $dstPath, string $refPath, bool $fromCwd = true): string
    {
        $refPath = self::reducePath($refPath);
        $dstPath = self::reducePath($dstPath);
        $isRefAbs = str_starts_with($refPath, '/');
        $isDstAbs = str_starts_with($dstPath, '/');

        // If paths are not both relative or both absolute, we'll have to first
        // ensure the paths are comparable, so we can work with them properly.
        if ($isRefAbs !== $isDstAbs) {
            $base = $fromCwd ? $this->cwd : $this->projectDir;

            if (!$isRefAbs) {
                $refPath = $base.'/'.$refPath;
            } else {
                $dstPath = $base.'/'.$dstPath;
            }
        }

        if ($refPath === $dstPath) {
            return './';
        }

        return self::doMakeRelative($dstPath, $refPath);
    }

    /**
     * Internal method to generate relative paths, for paths already normalized.
     *
     * Used mainly as a common code for other methods to utilize after they have
     * normalized and reduced that paths.
     *
     * @param string $path    The path to make relative to the reference path (value assumed normalized).
     * @param string $refPath The reference path, to build the resulting path relative to (value assumed normalized).
     *
     * @return string The path relative from $refPath to $path.
     */
    protected static function doMakeRelative(string $path, string $refPath): string
    {
        $i = 0;
        $match = 0;

        // Should still work with UTF8 since we're matching bytes and only care
        // if we encounter a '/' directory separator.
        //
        // @todo Do directory strings ever use any multi-char encoding other
        // than UTF-8 or plain 8-bit chars? Seems like
        // \Symfony\Componet\Filesystem does not currently appear to expect
        // multi-byte characters for path names (so assume this is ok for now).
        while (isset($refPath[$i]) && isset($path[$i]) && $refPath[$i] === $path[$i]) {
            if ($refPath[$i] === '/') {
                $match = $i + 1;
            }
            ++$i;
        }

        if (isset($refPath[$i])) {
            $depth = substr_count($refPath, '/', $match) + 1;
            $prefix = str_repeat('../', $depth);
        } else {
            $prefix = '';
        }

        return $prefix.substr($path, $match);
    }
}
