<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\File;

use Raini\Core\Environment;
use Raini\Core\Project\Tenant;

/**
 * The default path resolver handles the "vendor" path type or a plain filepath.
 */
class VendorPathResolver implements PathResolverInterface
{

    /**
     * @param Environment $env
     */
    public function __construct(protected Environment $env)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(string $path, string $type, Tenant $tenant): ?PathInfo
    {
        $vendorDir = $tenant->getVendorDir();

        switch ($type) {
            case PathInfo::PROJECT_PATH:
                if (!str_starts_with($path, $vendorDir)) {
                    return null;
                }
                $path = mb_substr($path, mb_strlen($vendorDir));

                // Intentionally fall through.
            case 'vendor':
                return new PathInfo('vendor', $path, $vendorDir);
        }

        return null;
    }
}
