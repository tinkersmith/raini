<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

/**
 * Project settings object that allows mutation of settings, and extensions.
 *
 * The normal settings are protected from mutations since with the exception of
 * a couple configuration commands, settings should never be changed. This makes
 * the mutability of the settings more intentional.
 */
class MutableSettings extends Settings
{

    /**
     * @param string  $rainiDir
     * @param mixed[] $data
     */
    public function __construct(string $rainiDir, array $data)
    {
        $this->rainiDir = $rainiDir;
        $this->data = $data;
    }

    /**
     * Set values for the primary droplet settings.
     *
     * @param mixed[] $settings The settings to set for the extension.
     *
     * @return self
     */
    public function setDropletSettings(array $settings): self
    {
        $this->data['settings'] = $settings;

        return $this;
    }

    /**
     * Alter the settings for a Raini extension.
     *
     * @param string  $name     The name of the extension to set the settings for.
     * @param mixed[] $settings The settings to set for the extension.
     *
     * @return self
     */
    public function setExtensionSettings(string $name, array $settings): self
    {
        $this->data['extensions'][$name] = $settings;

        return $this;
    }

    /**
     * Dump the project settings to the a "raini.project.yml" file.
     */
    public function dumpFile(): void
    {
        $fs = new Filesystem();
        $content = Yaml::dump($this->data, 6, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
        $fs->dumpFile($this->getProjectFilepath(), $content);
    }
}
