<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Database;

use Raini\Core\Configurator\ExtensionSchemaConfigurator;
use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Environment\EnvironmentInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tinkersmith\Configurator\Attribute\Configurator;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\Console\Output\BufferedOutput;

/**
 * Database driver for MySQL databases.
 *
 * Driver is targeted for MySQL 5.7 or 8.0 and newer versions of the database.
 */
 #[Configurator(ExtensionSchemaConfigurator::class, 'core', 'config/db/mysql.schema.yml')]
class MySQL implements DatabaseInterface
{

    /**
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return 'MySQL';
    }

    /**
     * {@inheritdoc}
     */
    public function command(): string
    {
        return 'mysql';
    }

    /**
     * {@inheritdoc}
     */
    public function dumpCommand(): string
    {
        return 'mysqldump';
    }

    /**
     * {@inheritdoc}
     */
    public function dumpFlags(): array
    {
        return [
            '--set-gtid-purged=off',
            '--no-tablespaces',
            '--column-statistics=0',
        ];
    }

    /**
     * Create connection arguments that can be used for database commands.
     *
     * @param string[] $dbInfo Information about the database to create the database connection arguments.
     *
     * @return string[] The CLI process arguments for providing the database connection information.
     */
    public function getConnectionArguments(array $dbInfo): array
    {
        $args = [];
        if (!empty($dbInfo['host'])) {
            $args[] = '--host='.$dbInfo['host'];
        }
        if (!empty($dbInfo['port'])) {
            $args[] = '--port='.$dbInfo['port'];
        }
        if (!empty($dbInfo['user'])) {
            $args[] = '--user='.$dbInfo['user'];
        }

        // Allow the local container to provide the database via an environment
        // variable. This helps to reduce the display mysql command warning.
        if (!empty($dbInfo['pass']) && getenv('MYSQL_PWD') !== $dbInfo['pass']) {
            $args[] = '--password='.$dbInfo['pass'];
        }

        // Append the target database.
        $args[] = $dbInfo['database'];

        return $args;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(array $dbInfo, mixed $input, ?OutputInterface $output = null, ExecutionContextInterface|EnvironmentInterface|null $context = null): int
    {
        $cli = $this->cliFactory->create($this->command(), $context);
        $args = $this->getConnectionArguments($dbInfo);
        $args[] = '--binary-mode=1';

        return $cli->execute($args, $output, $input);
    }

    /**
     * {@inheritdoc}
     */
    public function getTables(array $dbInfo, string|EnvironmentInterface|ExecutionContextInterface|null $context = null): array
    {
        $args = [
            ...$this->getConnectionArguments($dbInfo),
            '--skip-column-names',
            '-B',
        ];

        $buffer = new BufferedOutput();
        $this->cliFactory
            ->create($this->command(), $context)
            ->execute($args, $buffer, 'show tables;');

        return array_filter(explode("\n", $buffer->fetch()));
    }

    /**
     * {@inheritdoc}
     */
    public function dropTables(array $dbInfo, EnvironmentInterface|ExecutionContextInterface|null $context = null): void
    {
        $dropSql = [];
        foreach ($this->getTables($dbInfo, $context) as $table) {
            $dropSql[] = "DROP TABLE IF EXISTS `{$table}`;";
        }

        // If any drop SQL statements, then execute them to clear the database.
        if ($dropSql) {
            $connArgs = $this->getConnectionArguments($dbInfo);
            $this->cliFactory
                ->create($this->command(), $context)
                ->execute($connArgs, null, new \ArrayObject($dropSql));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function dump(array $dbInfo, OutputInterface $output, array $structTables = [], ExecutionContextInterface|EnvironmentInterface|null $context = null): int
    {
        $dumpCmd = $this->cliFactory->create($this->dumpCommand(), $context);
        $args = array_merge($this->getConnectionArguments($dbInfo), $this->dumpFlags());

        if ($structTables) {
            $structArgs = [...$args, '--no-data', '--tables'];

            // Output the table data, skipping the structure only tables.
            $dbName = $dbInfo['database'];
            foreach ($structTables as $tableName) {
                $args[] = "--ignore-table={$dbName}.{$tableName}";
                $structArgs[] = $tableName;
            }

            // Dump the structure only tables.
            $dumpCmd->execute($structArgs, $output);
        }

        return $dumpCmd->execute($args, $output);
    }
}
