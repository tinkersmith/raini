<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Database;

use Raini\Core\Database\Exception\DriverNotFoundException;

/**
 * The database driver manager.
 *
 * Collects database driver services and makes them available to fetch by ID.
 */
interface DatabaseManagerInterface
{

    /**
     * Get the database display name labels, keyed by driver ID.
     *
     * @return array<array{label: string, class: class-string}> Array of supported database drivers.
     */
    public function getDriverList(): array;

    /**
     * Get a database driver instance that matches the requested ID.
     *
     * @param string $id The identifier for the driver to fetch.
     *
     * @return DatabaseInterface The matching database driver if one matching the ID is available.
     *
     * @throws DriverNotFoundException
     */
    public function getDriver(string $id): DatabaseInterface;

    /**
     * Add a database driver to the manager.
     *
     * @param DatabaseInterface $driver The database driver to add.
     * @param string            $id     The ID for database driver.
     */
    public function addDriver(DatabaseInterface $driver, string $id): void;
}
