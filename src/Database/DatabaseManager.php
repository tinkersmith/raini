<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Database;

use Raini\Core\Database\Exception\DriverNotFoundException;

/**
 * The database driver manager.
 *
 * Collects database driver services and makes them available to fetch by ID.
 */
class DatabaseManager implements DatabaseManagerInterface
{

    /**
     * List of database drivers available for executing database commands.
     *
     * @var DatabaseInterface[]
     */
    protected array $drivers = [];

    /**
     * {@inheritdoc}
     */
    public function getDriverList(): array
    {
        return array_map(function ($driver) {
            return [
                'label' => $driver->label(),
                'class' => get_class($driver),
            ];
        }, $this->drivers);
    }

    /**
     * {@inheritdoc}
     */
    public function getDriver(string $id): DatabaseInterface
    {
        if (empty($this->drivers[$id])) {
            throw new DriverNotFoundException();
        }

        return $this->drivers[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function addDriver(DatabaseInterface $driver, string $id): void
    {
        $this->drivers[$id] = $driver;
    }
}
