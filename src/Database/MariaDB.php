<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Database;

use Raini\Core\Configurator\ExtensionSchemaConfigurator;
use Tinkersmith\Configurator\Attribute\Configurator;

/**
 * Database driver for MariaDB databases.
 *
 * For the most part MariaDB and MySQL are compatible but as starting to diverge
 * more and more as time goes by. This driver handles differences for MariaDB
 * databases when there need to be differences in cli command arguments and
 * settings differences.
 */
 #[Configurator(ExtensionSchemaConfigurator::class, 'core', 'config/db/mysql.schema.yml')]
class MariaDB extends MySQL implements DatabaseInterface
{

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return 'MariaDB';
    }

    /**
     * {@inheritdoc}
     */
    public function command(): string
    {
        return 'mariadb';
    }

    /**
     * {@inheritdoc}
     */
    public function dumpCommand(): string
    {
        return 'mariadb-dump';
    }

    /**
     * {@inheritdoc}
     */
    public function dumpFlags(): array
    {
        return ['--no-tablespaces'];
    }
}
