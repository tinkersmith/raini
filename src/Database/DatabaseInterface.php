<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Database;

use Raini\Core\Environment\EnvironmentInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * The interface for implementing a database driver.
 *
 * Database drivers provide Raini with the ability to perform common database
 * functionality for commands and services.
 */
interface DatabaseInterface
{

    /**
     * Get the database friendly label.
     *
     * @return string The database friendly display label.
     */
    public function label(): string;

    /**
     * The CLI command to use to execute databse queries.
     *
     * @return string The CLI command string.
     */
    public function command(): string;

    /**
     * The CLI command to use to execute the database dump.
     *
     * @return string The CLI command for dumping the database.
     */
    public function dumpCommand(): string;

    /**
     * The CLI arguments to use with the database dump commands.
     *
     * @return string[] The CLI options to use with the database dump command.
     */
    public function dumpFlags(): array;

    /**
     * Get a list of all database table names from the target database.
     *
     * @param string[]                                            $dbInfo  The database information for the db target.
     * @param EnvironmentInterface|ExecutionContextInterface|null $context The target environment for the database CLI process to use.
     *
     * @return string[] List of database tables from the target database.
     *
     * @throws \LogicException
     * @throws ProcessFailedException
     * @throws RuntimeException
     */
    public function getTables(array $dbInfo, EnvironmentInterface|ExecutionContextInterface|null $context = null): array;

    /**
     * Execute queries on a target database.
     *
     * @param mixed[]                                             $dbInfo  The database info for the target db.
     * @param InputInterface|iterable<mixed>|resource|string|null $input   The query or script to execute.
     * @param OutputInterface|null                                $output  Optionally the output stream.
     * @param ExecutionContextInterface|EnvironmentInterface|null $context The execution context to run the queries in.
     *
     * @return int The process return code. 0 means no errors.
     */
    public function execute(array $dbInfo, mixed $input, ?OutputInterface $output = null, ExecutionContextInterface|EnvironmentInterface|null $context = null): int;

    /**
     * Create a database dump for backup and storage of the database data.
     *
     * @param string[]                                            $dbInfo       The database information for the db target.
     * @param OutputInterface                                     $output       The stream, or resource to output the dump to.
     * @param string[]                                            $structTables The list of database tables that should be output as data only format.
     * @param EnvironmentInterface|ExecutionContextInterface|null $context      The target environment for the database CLI process to use.
     *
     * @return int The process return code. 0 means no errors.
     */
    public function dump(array $dbInfo, OutputInterface $output, array $structTables = [], ExecutionContextInterface|EnvironmentInterface|null $context = null): int;

    /**
     * Drop all tables in the target database.
     *
     * @param string[]                                            $dbInfo  The database information for the db target.
     * @param EnvironmentInterface|ExecutionContextInterface|null $context The target environment for the database CLI process to use.
     *
     * @throws \LogicException
     * @throws ProcessFailedException
     * @throws RuntimeException
     */
    public function dropTables(array $dbInfo, EnvironmentInterface|ExecutionContextInterface|null $context = null): void;
}
