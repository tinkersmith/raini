<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension;

/**
 * The path, name and handler class information about a Raini extension.
 */
class ExtensionDefinition implements ExtensionDefinitionInterface
{

    /**
     * @var string[]
     */
    protected $def;

    /**
     * @param string[]           $info Path, name, and handler class information for the extension.
     * @param ExtensionType|null $type The type of this extension.
     */
    public function __construct(array $info, protected ?ExtensionType $type = null)
    {
        $this->def = $info + [
            'class' => null,
        ];

        if (!$type && $this->def['class']) {
            $this->type = is_a($this->def['class'], DropletInterface::class, true)
                ? ExtensionType::Droplet
                : ExtensionType::Extension;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): ExtensionType
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): string
    {
        return $this->def['id'];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->def['name'];
    }

    /**
     * {@inheritdoc}
     */
    public function getPath(): string
    {
        return $this->def['path'];
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): ?string
    {
        return $this->def['class'];
    }
}
