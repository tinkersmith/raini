<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension;

use Raini\Core\Project\Tenant;

/**
 * Base class for Droplet extension handlers.
 */
abstract class AbstractDroplet extends AbstractExtension implements DropletInterface
{

    /**
     * {@inheritdoc}
     */
    public function buildTenant(string $name, array $definition): Tenant
    {
        $tenantClass = static::TENANT_CLASS;

        return new $tenantClass($this, $name, $definition);
    }
}
