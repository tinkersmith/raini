<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension\Exception;

/**
 * Exception for when a Raini extension package is requested but missing.
 */
class ExtensionNotFoundException extends \Exception
{

    /**
     * @param string          $extensionName The name of the extension that is missing.
     * @param int             $code          An error code value, defaults to 0.
     * @param \Throwable|null $prev          A previous exception to chain exceptions.
     */
    public function __construct(string $extensionName, int $code = 0, ?\Throwable $prev = null)
    {
        $message = sprintf('Requested extension: %s is not available and cannot be loaded.', $extensionName);

        parent::__construct($message, $code, $prev);
    }
}
