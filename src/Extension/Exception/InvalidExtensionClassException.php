<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension\Exception;

/**
 * Exception for when a Raini extension package is requested but missing.
 */
class InvalidExtensionClassException extends \Exception
{

    /**
     * @param string          $extensionName The name of the extension that is missing.
     * @param string          $interface     The interface name that was supposed to be implemented by extension class.
     * @param int             $code          An error code value, defaults to 0.
     * @param \Throwable|null $prev          A previous exception to chain exceptions.
     */
    public function __construct(string $extensionName, string $interface = 'ExtensionInterface', int $code = 0, ?\Throwable $prev = null)
    {
        $message = sprintf('Invalid class extension class for "%s". Class must implement "%s" interface', $extensionName, $interface);

        parent::__construct($message, $code, $prev);
    }
}
