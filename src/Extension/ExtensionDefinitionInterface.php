<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension;

/**
 * Interface for the base extension definition objects.
 *
 * Extension definitions are loaded and managed by the ExtensionList service.
 * These definitions provide the information needed for the ExtensionManager
 * to create instances of the handlers, and locate the extension files.
 */
interface ExtensionDefinitionInterface
{

    /**
     * @return ExtensionType The type of extension described by this definition.
     *
     * @see ExtensionType
     */
    public function getType(): ExtensionType;

    /**
     * @return string ID of the extension.
     */
    public function getId(): string;

    /**
     * @return string Name of the extension.
     */
    public function getName(): string;

    /**
     * @return string Path to the extension, relative to the project directory.
     */
    public function getPath(): string;

    /**
     * @return class-string<ExtensionInterface> The fully namespace class name of the extension handler.
     */
    public function getClass(): ?string;
}
