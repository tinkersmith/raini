<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension;

use Symfony\Component\Filesystem\Filesystem;

/**
 * Extension information about the Raini package itself.
 *
 * Since this is describing the core Raini package, we can take shortcuts to
 * find this information, and use the information from the source files, which
 * is more reliable than using the "extensions.yml" file to store the package
 * information.
 */
class CoreExtensionDefinition implements ExtensionDefinitionInterface
{

    /**
     * Path to the core extension, relative to the project directory.
     *
     * @var string
     */
    protected $path;

    /**
     * @param string $projectDir The project directory.
     */
    public function __construct(string $projectDir)
    {
        $fs = new Filesystem();
        $this->path = $fs->makePathRelative(dirname(__DIR__, 2), $projectDir);
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): string
    {
        return 'core';
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'raini/core';
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): ExtensionType
    {
        return ExtensionType::Core;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): ?string
    {
        return null;
    }
}
