<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base interface for Raini extensions.
 *
 * Extensions add functionality and behaviors to a Raini project. Information
 * about the install location, and extension features available.
 */
interface ExtensionInterface
{

    /**
     * @param ExtensionDefinitionInterface $definition The information about this extension from the ExtensionList.
     * @param mixed[]                      $settings   The settings for this extension.
     */
    public function __construct(ExtensionDefinitionInterface $definition, array $settings);

    /**
     * Creates a new instance of the extension handler.
     *
     * @param ContainerInterface           $container  The dependency injection container.
     * @param ExtensionDefinitionInterface $definition The information about this extension from the ExtensionList.
     * @param mixed[]                      $settings   The settings for this extension.
     *
     * @return static A new instance of the extension handler of the type of the calling class.
     */
    public static function create(ContainerInterface $container, ExtensionDefinitionInterface $definition, array $settings = []): static;

    /**
     * The extension internal identifier.
     *
     * The internal extension identifier for module name and info.
     *
     * @return string The ID of the extension.
     */
    public function getId(): string;

    /**
     * The extension name.
     *
     * The extension name is the same as the Composer package name, and should
     * uniquely identify this package.
     *
     * @return string The name of the extension.
     */
    public function getName(): string;

    /**
     * The directory where this extension files and resource can be found.
     *
     * @return string The filepath where the extension is installed.
     */
    public function getPath(): string;

    /**
     * Get the default settings for this extension.
     *
     * @return mixed[] The default settings defined for this extension.
     */
    public function defaultSettings(): array;

    /**
     * Gets the current extension settings.
     *
     * @return mixed[] The current droplet or extension settings.
     */
    public function getSettings(): array;

    /**
     * Sets the settings for this extension.
     *
     * This method is generally responsible for ensuring defaults are applied
     * and internal structures which rely on settings information are updated
     * properly (e.g. $sites for Droplets).
     *
     * @param mixed[] $settings The extension's settings to be applied.
     */
    public function setSettings(array $settings): void;

    /**
     * Describe the Docker containers expected and used by this Droplet.
     *
     * These are used by the containerizer services to generate the appropriate
     * Docker files. Definitions are for network, images and volumes to be used
     * by a Docker based system. The containerizer services can use these
     * definitions and templates to create files for "docker-compose", "Lando",
     * "Docksal" or "DDev" based on available containerizer services
     * (can be switched out).
     *
     * @return array<string, mixed> Docker definitions for container services, volumes and networks.
     *
     * @see \Raini\Containerize\ContainerizerInterface
     */
    public function containerDefinitions(): array;
}
