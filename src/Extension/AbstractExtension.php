<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for extension handlers.
 */
abstract class AbstractExtension implements ExtensionInterface
{

    /**
     * @var mixed[]
     */
    protected array $settings;

    /**
     * {@inheritdoc}
     */
    public function __construct(protected ExtensionDefinitionInterface $definition, array $settings)
    {
        $settings += $this->defaultSettings();
        $this->setSettings($settings);
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, ExtensionDefinitionInterface $definition, array $settings = []): static
    {
        return new static($definition, $settings);
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): string
    {
        return $this->definition->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->definition->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function getPath(): string
    {
        return $this->definition->getPath();
    }

    /**
     * {@inheritdoc}
     */
    public function defaultSettings(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * {@inheritdoc}
     */
    public function setSettings(array $settings): void
    {
        $this->settings = $settings;
    }

    /**
     * {@inheritdoc}
     */
    public function containerDefinitions(): array
    {
        return [];
    }
}
