<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension;

use Raini\Core\Project\Exception\TenantNotFoundException;
use Raini\Core\Project\Tenant;
use Raini\Core\Utility\ComposerBuilder;

/**
 * Base interface for Droplet extensions.
 *
 * Droplets are Raini extensions which determine the project type and behavior.
 * Only one Droplet can be active for a project.
 */
interface DropletInterface extends ExtensionInterface
{
    /**
     * @var class-string<Tenant>
     */
    const TENANT_CLASS = Tenant::class;

    /**
     * Creates a new ComposerBuilder object to construct a composer.json file.
     *
     * @param Tenant $tenant The tenant to create this composer.json file for.
     *
     * @return ComposerBuilder The composer builder object to set values and construct a composer.json data.
     *
     * @throws TenantNotFoundException The site is not configured or is not managed by this Droplet.
     */
    public function createComposerFile(Tenant $tenant): ComposerBuilder;

    /**
     * Creates the Droplet tenants from provided definitions.
     *
     * @param string  $name       The name and identifier for the tenant being built.
     * @param mixed[] $definition An array of tenant definitions which match the Droplet extension.
     *
     * @return Tenant The initialized tenant instance.
     */
    public function buildTenant(string $name, array $definition): Tenant;
}
