<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension;

use Raini\Core\DependencyInjection\ClassResolver;
use Raini\Core\Extension\Exception\ExtensionNoHandlerException;
use Raini\Core\Extension\Exception\ExtensionNotFoundException;
use Raini\Core\Extension\Exception\InvalidExtensionClassException;
use Raini\Core\Project\Exception\ProjectNotInitializedException;
use Raini\Core\Settings;

/**
 * Extension manager creates and manages the handlers for Raini extensions.
 *
 * Extensions like Raini Droplets and Raini Extensions can have an extension
 * class which provides the behaviors for the projects that are setup to use
 * them. The extension manager is only useful after projects have been created
 * and are setup with a Droplet.
 */
class ExtensionManager
{

    /**
     * @var DropletInterface[]
     */
    protected $droplets = [];

    /**
     * @var ExtensionInterface[]|null
     */
    protected $extensions;

    /**
     * @param ExtensionList $extensionList Information about installed Raini extensions.
     * @param Settings      $settings      The Raini project settings.
     * @param ClassResolver $classResolver The dependency injection class resolver.
     */
    public function __construct(protected ExtensionList $extensionList, protected Settings $settings, protected ClassResolver $classResolver)
    {
    }

    /**
     * Gets the project Droplet extension if project has been created.
     *
     * A project must have a single Droplet extension set once it is created.
     * Projects are created by running the "raini project:create" command.
     * During that time users are given the option to select between available
     * Droplets to create the project with.
     *
     * @param string|null $name The name of the Droplet to fetch, if empty the default active Droplet is retrieved.
     *
     * @return DropletInterface Gets the fully loaded Droplet extension object if project has been created.
     *                          Otherwise NULL is returned.
     *
     * @throws ProjectNotInitializedException If project has not been configured with a droplet, or it is not installed.
     */
    public function getDroplet(?string $name = null): DropletInterface
    {
        $name = $name ?? $this->settings->getType();

        if (!isset($this->droplets[$name])) {
            if ($definition = $this->extensionList->getDroplet($name)) {
                $args = ['definition' => $definition];
                $args['settings'] = $name === $this->settings->getType()
                    ? $this->settings->getDropletSettings()
                    : $this->settings->getExtensionSettings($name);

                $instance = $this->classResolver->createInstance($definition->getClass(), $args);
                if ($instance instanceof DropletInterface) {
                    $this->droplets[$name] = $instance;
                } else {
                    throw new InvalidExtensionClassException($name, 'DropetInterface');
                }
            } else {
                throw new ProjectNotInitializedException();
            }
        }

        return $this->droplets[$name];
    }

    /**
     * @param string $name The name of the extension handler to fetch.
     *
     * @return ExtensionInterface The extension handler of the requested extension.
     *
     * @throws ExtensionNoHandlerException If the extension definition exists but does not have a handler class.
     * @throws ExtensionNotFoundException  If the requested extension doesn't exist or is not installed.
     */
    public function getExtension(string $name): ExtensionInterface
    {
        $extensions = $this->getExtensions();
        if (isset($extensions[$name])) {
            return $extensions[$name];
        }

        if ($this->extensionList->getExtensionDefinition($name)) {
            throw new ExtensionNoHandlerException($name);
        }
        throw new ExtensionNotFoundException($name);
    }

    /**
     * @return ExtensionInterface[] All loaded particle extensions which have a handler class defined.
     */
    public function getExtensions(): array
    {
        if (!isset($this->extensions)) {
            $this->extensions = [];

            foreach ($this->extensionList->getExtensionDefinitions() as $name => $def) {
                if (!$def->getClass()) {
                    continue;
                }

                $this->extensions[$name] = $this->classResolver->createInstance($def->getClass(), [
                    'definition' => $def,
                    'settings' => $this->settings->getExtensionSettings($name),
                ]);
            }
        }

        return $this->extensions;
    }
}
