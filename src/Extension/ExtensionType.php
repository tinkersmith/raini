<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension;

/**
 * Describes the extension type.
 *
 * Droplets dictate the type and behavior of project tenants, and only one
 * can be active per tenant, while extensions modify Raini behavior in general
 * and multiple can be active at a time.
 */
enum ExtensionType
{
    case Core;
    case Droplet;
    case Extension;
}
