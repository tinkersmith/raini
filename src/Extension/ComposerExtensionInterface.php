<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Extension;

/**
 * Interface for extensions that add Composer configurations and resources.
 */
interface ComposerExtensionInterface extends ExtensionInterface
{

    /**
     * Get Composer repositories that this extension relies on.
     *
     * The array is keyed by the repository name and the array values are
     * respository configurations. Repository configurations vary based on the
     * repository type, but at a minimum should have a "type" and "url" value.
     *
     * Repository types can be one of "composer", "path", "vcs" or "package".
     *
     * @return array<string, mixed[]> Configurations for repositories required by this Droplet, keyed by a repository name.
     *
     * @see https://getcomposer.org/doc/05-repositories.md
     *
     * @return mixed[]
     */
    public function getRepositories(): array;

    /**
     * Composer packages that this extension wants installed per tenant.
     *
     * @return string[] List of Composer packages as they would be passed on the commandline. This means that each
     *                  value of the array can just be the package name, or the package name and a version constraint
     *                  (i.e. "drupal/core" or "drupal/core:^9.4").
     */
    public function getPackages(): array;

    /**
     * Composer dev packages that this extension wants installed per tenant.
     *
     * @return string[] List of Composer packages as they would be passed on the commandline. This means that each
     *                  value of the array can just be the package name, or the package name and a version constraint
     *                  (i.e. "drupal/core" or "drupal/core:^9.4").
     */
    public function getDevPackages(): array;
}
