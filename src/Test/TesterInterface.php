<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Test;

use Raini\Core\DependencyInjection\ServiceInspectionInterface;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * Base interface for creating Testers.
 *
 * Testers execute automated tests on a target. The tests are normally Unit,
 * functional or kernel tests defined by the project.
 */
interface TesterInterface extends ServiceInspectionInterface
{

    /**
     * @return string The human friendly name for this tester.
     */
    public function getName(): string;

    /**
     * @return string The tester description.
     */
    public function getDescription(): string;

    /**
     * @param PathInfo $path   The target path to run tests on.
     * @param Tenant   $tenant The tenant the tests will be run for.
     *
     * @return bool Is the path specified applicable to this tester?
     */
    public function isPathApplicable(PathInfo $path, Tenant $tenant): bool;

    /**
     * Execute the test for the target path.
     *
     * @param PathInfo[]|PathInfo                            $paths   Target path to execute the tests on.
     * @param Tenant                                         $tenant  The Raini tenant to run the tests against.
     * @param EnvironmentInterface|ExecutionContextInterface $context The environment to run the tests in.
     *
     * @return int The return code of the executed test. This is meant to be compatible with CLI return codes were the
     *             return code being zero means there were no errors, and any other number represents and error code.
     */
    public function execute(array|PathInfo $paths, Tenant $tenant, EnvironmentInterface|ExecutionContextInterface $context): int;
}
