<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Test;

use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;

/**
 * Service collector to manage Tester implementations (to run tests).
 */
interface TestManagerInterface
{

    /**
     * Determine what tester should be used to execute testing base on a path.
     *
     * @param PathInfo|PathInfo[] $paths  Information about the path (filepath, path type), to target for testing.
     * @param Tenant              $tenant The tenant the test is being run for.
     *
     * @return TesterInterface|null The tester to use on the provided info if a matching tester can be determined.
     */
    public function getTesterByPath(PathInfo|array $paths, Tenant $tenant): ?TesterInterface;
}
