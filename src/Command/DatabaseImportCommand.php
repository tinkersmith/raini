<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command;

use Raini\Core\Event\Database\DatabaseEvents;
use Raini\Core\Event\Database\DbImportEvent;
use Raini\Core\Project\DatabaseProviderInterface;
use Raini\Core\Project\Exception\SiteNotFoundException;
use Raini\Core\Project\Exception\TenantNotFoundException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\DockerContext;
use Tinkersmith\Console\Exception\CliRuntimeException;

/**
 * Import database from exported file source.
 */
#[AsCommand(
    'db:import',
    'Import a database dump into a tenant or site database.',
)]
class DatabaseImportCommand extends AbstractDatabaseCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $helpText = <<<EOT
            Generate project uses.
            EOT;

        $this->setHelp($helpText)
            ->addOption('gzip', 'z', InputOption::VALUE_NONE, 'Indicates that the source file is compressed with GZIP, not needed if file extension ends in ".gz".')
            ->addOption('scripts', null, InputOption::VALUE_NEGATABLE, 'Run pre-process and post-process scripts before and after the database import.')
            ->addArgument('file', InputArgument::OPTIONAL, 'The exported database dump file to import.');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $errorLevel = error_reporting();
        try {
            @['environment' => $environment, 'site' => $site, 'tenant' => $tenant] = $this->options->getValues($input);

            $context = $environment->createContext();
            if ($context instanceof DockerContext) {
                $context->setContainer('database');
            }

            $dbProvider = $site ?: $tenant;
            if (!$dbProvider instanceof DatabaseProviderInterface) {
                $msg = sprintf('The resource named "%s" does not provide a database connection to export.', $dbProvider->getName());
                throw new \InvalidArgumentException($msg);
            }

            $useScripts = $input->getOption('scripts') ?? false;
            $targetDb = $input->getOption('db');
            $dbInfo = $this->getDbInfo($targetDb, $dbProvider, $context);
            $driver = $this->dbManager->getDriver($dbInfo['type']);

            // Prevent notice an warnings produced by the process execution,
            // these create extra noise.
            error_reporting(E_ALL & ~E_NOTICE);

            $importOpts = $this->getDbOperationOptions($tenant, $input);
            $importEvent = new DbImportEvent($dbProvider, $targetDb, $useScripts, $importOpts, $environment, $output);
            $this->eventDispatcher->dispatch($importEvent, DatabaseEvents::PRE_IMPORT);

            // Drop all current tables first.
            // Create the mydump command.
            $output->writeln('<info>Clearing existing tables...</>');
            $driver->dropTables($dbInfo, $context);

            // Get the source file.
            $filesource = $input->getArgument('file') ?? $this->getDefaultFilename($dbProvider, $targetDb);
            $stream = $this->getInputStream($filesource, $input->getOption('gzip'));

            $output->writeln('<info>Importing database from source</>');
            $retval = $driver->execute($dbInfo, $stream, $output, $context);
            $this->eventDispatcher->dispatch($importEvent, DatabaseEvents::POST_IMPORT);

            return $retval;
        } catch (RuntimeException|CliRuntimeException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        } catch (TenantNotFoundException|SiteNotFoundException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        } catch (\InvalidArgumentException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        } finally {
            // Restore the error reporting level.
            error_reporting($errorLevel);

            // Close the stream if it is still open due to an error.
            if (!empty($stream)) {
                fclose($stream);
            }
        }

        return 1;
    }

    /**
     * Get or create the file resource stream to use as the SQL input.
     *
     * @param string $filepath   The target filepath for the input stream.
     * @param bool   $compressed The file should be assumed to be GZIP compressed regardless of file extension.
     *
     * @return resource The filestream resource to use as the SQL command input stream.
     *
     * @throws \InvalidArgumentException
     */
    protected function getInputStream(string $filepath, bool $compressed = false): mixed
    {
        if (is_readable($filepath)) {
            try {
                if ($compressed || preg_match('#\.gz$#i', $filepath)) {
                    // PHP Compress stream doesn't actually error until you try to read
                    // data from the stream. Read a small bit of data to see if a
                    // RuntimeException is thrown.
                    $stream = fopen("compress.zlib://{$filepath}", 'r');
                    fread($stream, 10);
                    rewind($stream);
                } else {
                    $stream = fopen("{$filepath}", 'r');
                }
            } catch (\RuntimeException) {
                // Rethrow as an invalid argument because the file was not
                // a GZip (or corrupted) file.
                // Don't get it confused with a process runtime error.
                throw new \InvalidArgumentException("Unable to uncompress or read file {$filepath} for importing");
            }

            // Only return the stream if it was successfully opened as a
            // resource handle and is ready to be passed as an input stream.
            if (is_resource($stream)) {
                return $stream;
            }
        }

        throw new \InvalidArgumentException("Unable to find or read the import source file: {$filepath}");
    }
}
