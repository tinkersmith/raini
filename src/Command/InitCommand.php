<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command;

use Raini\Core\Command\Option\CommandOptionInterface;
use Raini\Core\Command\Option\EnvironmentOption;
use Raini\Core\Command\Option\OptionFactoryInterface;
use Raini\Core\Project\GenerateOptions;
use Raini\Core\Project\ProjectInitializer;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\Exception\CliRuntimeException;

/**
 * Initialize the project instance with local environment overrides.
 */
#[AsCommand(
    name: 'project:init',
    description: 'Setup the project instance with local environment overrides.',
    aliases: ['init'],
)]
class InitCommand extends Command
{

    /**
     * The command option handler for providing environment options.
     *
     * @var CommandOptionInterface
     */
    public CommandOptionInterface $environmentOption;

    /**
     * @param ProjectInitializer     $initializer   The project intializer to create local project files and overrides.
     * @param OptionFactoryInterface $optionFactory The command options factory.
     */
    public function __construct(protected ProjectInitializer $initializer, OptionFactoryInterface $optionFactory)
    {
        $this->environmentOption = $optionFactory->createOption(EnvironmentOption::class);

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this->setHelp(<<<EOF
            Initializes the project for to run in the target environment. These
            are normally environment specific overrides and which are not
            normally include in the repository.

            This means this file is normally run to setup and create those file
            and overrides locally.
            EOF
        );

        // Add the project initializer command options and arguments info.
        $this->addOption('mode', null, InputOption::VALUE_REQUIRED, 'The write mode determines how existing files are handled (replacing, skipping, or merging).');
        $this->environmentOption->apply($this);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $options = new GenerateOptions();
            $options->mode = $input->getOption('mode') ?? GenerateOptions::MODE_MERGE;
            $options->environment = $this->environmentOption->getValues($input)['environment'];

            $styler = new SymfonyStyle($input, $output);
            $this->initializer->run($options, $styler);

            return 0;
        } catch (RuntimeException|CliRuntimeException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        } catch (\InvalidArgumentException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        }

        return 1;
    }
}
