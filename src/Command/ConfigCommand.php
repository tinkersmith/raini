<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command;

use Raini\Core\Configurator\Exception\ExtensionNoConfigException;
use Raini\Core\Extension\Exception\ExtensionNoHandlerException;
use Raini\Core\Extension\ExtensionManager;
use Raini\Core\Project\GenerateOptions;
use Raini\Core\Project\ProjectGenerator;
use Raini\Core\Project\ProjectInitializer;
use Raini\Core\Prompt\Prompter;
use Raini\Core\Settings;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Tinkersmith\Configurator\Attribute\Configurator;

/**
 * Command to configure a Raini extension.
 */
#[AsCommand(
    name: 'config:extension',
    description: 'Command to configure a Raini extension.',
    aliases: ['config', 'config:ext']
)]
class ConfigCommand extends Command
{

    /**
     * @param Settings           $settings
     * @param ExtensionManager   $extensions
     * @param ProjectGenerator   $generator
     * @param ProjectInitializer $initializer
     * @param Prompter           $prompter
     */
    public function __construct(protected Settings $settings, protected ExtensionManager $extensions, protected ProjectGenerator $generator, protected ProjectInitializer $initializer, protected Prompter $prompter)
    {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $helpText = <<<EOT
            Configure an installed Raini extension. Not all extensions have configurations
            but many will provide options that get written to the "raini.project.yml" file.

            Extensions that provide configurations are likely to describe their
            configuration settings in more detail in their "README.md" files.
            EOT;

        $this
            ->setHelp($helpText)
            ->addOption('generate', null, InputOption::VALUE_NEGATABLE, 'Regenerate and initialize the project after updating configuration.')
            ->addOption('reset', '', InputOption::VALUE_NONE, '')
            ->addArgument('extension', InputArgument::REQUIRED, 'The ID of the extension to configure.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $extName = $input->getArgument('extension');

        try {
            $extension = $this->extensions->getExtension($extName);

            if ($input->getOption('reset')) {
                $settings = $extension->defaultSettings();
            } else {
                $configurator = Configurator::fromClass(get_class($extension));
                if (!$configurator) {
                    // No configurations for this extension, we can skip.
                    throw new ExtensionNoConfigException($extName);
                }

                $settings = $this->prompter->prompt($input, $output, $configurator, $extension->getSettings());
            }

            $this->settings
                ->getMutable()
                ->setExtensionSettings($extension->getId(), $settings)
                ->dumpFile();

            if ($input->getOption('generate') ?? true) {
                $extension->setSettings($settings);

                $options = new GenerateOptions();
                $options->mode = GenerateOptions::MODE_MERGE;

                $styler = new SymfonyStyle($input, $output);
                $this->generator->run($options, $styler);
                $this->initializer->run($options, $styler);
            }

            return 0;
        } catch (ExtensionNoHandlerException|ExtensionNoConfigException $e) {
            $msg = sprintf('<info>The extension "%s" does not have any configurations available.</>', $extName);
            $output->writeln($msg);
        } catch (\Exception $e) {
            $this->getApplication()->renderThrowable($e, $output);
        }

        return 1;
    }
}
