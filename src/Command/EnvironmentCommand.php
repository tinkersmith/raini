<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command;

use Raini\Core\Configurator\EnvironmentConfigurator;
use Raini\Core\Environment;
use Raini\Core\Environment\EnvironmentManagerInterface;
use Raini\Core\Prompt\Prompter;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Creates a new project, or reconfigures an existing project.
 *
 * This command also generates the project files after all configurations have
 * been completed.
 */
#[AsCommand(
    name: 'project:env',
    description: 'Creates or edits environment settings in the environments.yml.',
    aliases: ['environment', 'env'],
)]
class EnvironmentCommand extends Command
{

    /**
     * @param Environment                 $env
     * @param EnvironmentManagerInterface $environmentManager
     * @param Prompter                    $prompter
     */
    public function __construct(protected Environment $env, protected EnvironmentManagerInterface $environmentManager, protected Prompter $prompter)
    {
        parent::__construct();
    }

    /**
     * Validates that user input validates as an environment name.
     *
     * @param string|null $name The user input to validate for an environment name.
     *
     * @return string A valid environment name if the name passes validation.
     *
     * @throws \InvalidArgumentException
     */
    public function validateEnvironmentName(?string $name): string
    {
        if (empty($name)) {
            throw new \InvalidArgumentException('A valid environment name is required.');
        }
        if (preg_match('/^[a-zA-Z][a-zA-Z0-9_]*$/', $name)) {
            return $name;
        }

        throw new \InvalidArgumentException('Environment name should start with a letter, and only have alphanumeric characters and underscores.');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $helpText = <<<EOT
            Command to create or edit environment entries to the <info>environments.yml</> or
            the <info>envirnments.local.yml</> (with the --local option) files.

            The optional name argument can specify the environment ID to edit, or if the ID does
            not exist a new environment with that name will be created with the interactive
            environment builder.

            If no environment name is provided as an argument, the interactive builder will
            allow you to either create a new environment or edit an existing one.
            EOT;

        $this
            ->setHelp($helpText)
            ->addOption('local', 'l', InputOption::VALUE_NONE, 'Configure the local environment settings when options is set')
            ->addOption('del', 'd', InputOption::VALUE_NONE, 'Delete the specified environment')
            ->addArgument('name', InputArgument::OPTIONAL, 'Environment name to add or existing environment to edit');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            /** @var QuestionHelper $qHelper */
            $qHelper = $this->getHelper('question');
            $filename = 'environments'.($input->getOption('local') ? '.local' : '').'.yml';

            try {
                $envs = Yaml::parseFile($this->env->getRainiPath().'/'.$filename);
            } catch (ParseException $e) {
                $envs = [];
            }

            $name = $input->getArgument('name');
            if ($input->getOption('del')) {
                if (!$name && $options = array_keys($envs)) {
                    $name = $qHelper->ask($input, $output, new ChoiceQuestion('<question>Which environment would you like to delete?</> ', $options));
                } elseif (empty($envs[$name])) {
                    throw new \InvalidArgumentException('Unknown environment name, cannot remove environment.');
                }

                // Remove the environment settings and write out the updates.
                unset($envs[$name]);
            } else {
                if (!$name) {
                    if ($options = array_keys($envs)) {
                        $options[] = '*new environment';
                        $name = $qHelper->ask($input, $output, new ChoiceQuestion('<question>Which environment would you like to edit?</> ', $options));
                    }

                    if (empty($options) || '*new environment' === $name) {
                        $question = new Question('<question>Provide a name for the new environment:</> ');
                        $question->setValidator($this->validateEnvironmentName(...));
                        $name = $qHelper->ask($input, $output, $question);

                        $confirm = new ConfirmationQuestion(sprintf('<question>Environment %s exists, do you wish to overwrite it? [y/N]</> ', $name), false);
                        if (isset($envs[$name]) && !$qHelper->ask($input, $output, $confirm)) {
                            return 1;
                        }
                    }
                } elseif (empty($envs[$name])) {
                    $this->validateEnvironmentName($name);
                    $text = sprintf('<question>No environment with ID "%s", would you like to create it? [Y/n]</> ', $name);
                    if (!$qHelper->ask($input, $output, new ConfirmationQuestion($text, true))) {
                        return 1;
                    }
                }

                // Capture tenant overrides if present. The prompts and schemas
                // don't support this yet, and if manually entered, we need to
                // retain these settings and re-apply them.
                $stashed = array_intersect_key($envs[$name] ?? [], ['tenants' => true]);
                $configurator = new EnvironmentConfigurator($this->environmentManager);
                $envs[$name] = $this->prompter->prompt($input, $output, $configurator, $envs[$name] ?? []);
                $envs[$name] += $stashed;
            }

            $fs = new Filesystem();
            $content = Yaml::dump($envs, 3, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
            $fs->dumpFile($this->env->getRainiPath().'/'.$filename, $content);

            return 0;
        } catch (\Exception $e) {
            $this->getApplication()->renderThrowable($e, $output);

            return 1;
        }
    }
}
