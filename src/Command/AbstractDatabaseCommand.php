<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command;

use Raini\Core\Command\Option\EnvironmentTenantOption;
use Raini\Core\Command\Option\OptionCollection;
use Raini\Core\Command\Option\OptionFactoryInterface;
use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Database\DatabaseManagerInterface;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\Event\Database\AlterDbCommandEvent;
use Raini\Core\Event\Database\DatabaseEvents;
use Raini\Core\Project\DatabaseProviderInterface;
use Raini\Core\Project\SiteInterface;
use Raini\Core\Project\SiteTenantInterface;
use Raini\Core\Project\Tenant;
use Raini\Core\Project\TenantDatabaseInterface;
use Raini\Core\Project\TenantManagerInterface;
use Raini\Core\Value\SettingsValueResolverInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Tinkersmith\Console\DockerContext;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * Base class for console commands which manipulate tenant or site databases.
 *
 * Provides base utility for fetching the tables, creating connection strings
 * and determining the target database and servers from the command options.
 *
 * @todo refactor this for supporting other databases beyond just
 * supporting MySQL (MariaDB, AuroraDB, etc...). This is likely going to require
 * abstracting the databases into different driver classes. Would it also be
 * possible to support NoSQL, and document databases?
 */
abstract class AbstractDatabaseCommand extends Command
{

    /**
     * A collection of command option providers.
     *
     * The option providers add the command line option information and
     * resolution into their respective instances. E.G. Rather than just getting
     * the string value for environment from the command input object, the
     * EnvironmentOption provider will validate (including throwing errors)
     * and provide the EnvironmentInterface object that matches the value.
     *
     * @var OptionCollection
     */
    protected OptionCollection $options;

    /**
     * @param TenantManagerInterface         $tenantManager
     * @param DatabaseManagerInterface       $dbManager
     * @param CliFactoryInterface            $cliFactory
     * @param SettingsValueResolverInterface $settingsResolver
     * @param EventDispatcherInterface       $eventDispatcher
     * @param OptionFactoryInterface         $optionFactory
     */
    public function __construct(protected TenantManagerInterface $tenantManager, protected DatabaseManagerInterface $dbManager, protected CliFactoryInterface $cliFactory, protected SettingsValueResolverInterface $settingsResolver, protected EventDispatcherInterface $eventDispatcher, OptionFactoryInterface $optionFactory)
    {
        $this->options = $optionFactory->createCollection([
            'tenant' => EnvironmentTenantOption::class,
        ]);

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled(): bool
    {
        foreach ($this->tenantManager->getTenantClasses() as $tenantClass) {
            // Database commands are only available for projects that utilize
            // and define databases for their tenants.
            if (is_a($tenantClass, DatabaseProviderInterface::class, true)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Create a default or expected filename based on the database provider.
     *
     * @param DatabaseProviderInterface $dbProvider The tenant or site which own the database being targeted.
     * @param string                    $dbName     The name of the database being targeted by the database command.
     *
     * @return string A default filename for the database.
     */
    public function getDefaultFilename(DatabaseProviderInterface $dbProvider, string $dbName = ''): string
    {
        $parts = [];
        if ($dbProvider instanceof SiteInterface) {
            $tenant = $dbProvider->getTenant();
            $parts[] = $tenant->getName();

            // If multi-site setup, the include the target site in the name of
            // the filename, since there are multiple sites under this tenant
            // and we need to distinguish between them.
            if ($tenant instanceof SiteTenantInterface && $tenant->isMultiSite()) {
                $parts[] = $dbProvider->getName();
            }
        } else {
            $parts[] = $dbProvider->getName();
        }

        // Apply the database name if not default.
        if ($dbName && 'default' !== $dbName) {
            $parts[] = $dbName;
        }

        return implode('-', $parts).'.sql.gz';
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this->addOption('db', null, InputOption::VALUE_REQUIRED, 'The target database', 'default');
        $this->options->apply($this);

        $event = new AlterDbCommandEvent($this->getName(), $this);
        $this->eventDispatcher->dispatch($event, DatabaseEvents::ALTER_COMMAND);
    }

    /**
     * Get the target database information.
     *
     * The database information contains the database name, host, user, and
     * authentication. This information is needed to create connection strings
     * and also dictate options for further database CLI processes.
     *
     * @param string                                              $target   The name or ID of the database to get the information for.
     * @param DatabaseProviderInterface                           $provider The site or tenant to get the database information from.
     * @param EnvironmentInterface|ExecutionContextInterface|null $context  The environment to resolve the database settings for.
     *
     * @return string[] The database information.
     *
     * @throws \InvalidArgumentException If the requested database resource cannot be found.
     */
    protected function getDbInfo(string $target, DatabaseProviderInterface $provider, EnvironmentInterface|ExecutionContextInterface|null $context = null): array
    {
        $dbInfo = $provider->getDatabase($target);
        // If the host matches the Docker container name, the host parameter
        // isn't needed, and actually causes confusion for the command.
        if ($context instanceof DockerContext && $context->getContainer() === ($dbInfo['host'] ?? null)) {
            unset($dbInfo['host']);
        }

        return $this->settingsResolver->resolveSettings($dbInfo, $context);
    }

    /**
     * Apply options for the tenant to the database operation.
     *
     * @param Tenant         $tenant The tenant which the database operation is being performed on.
     * @param InputInterface $input  The command input from the commandline calls.
     *
     * @return mixed[] The database operation options based on input and application contexts.
     */
    protected function getDbOperationOptions(Tenant $tenant, InputInterface $input): array
    {
        $options = [];
        if ($tenant instanceof TenantDatabaseInterface) {
            $tenant->applyDbOptions($this->getName(), $input->getOptions(), $options);
        }

        return $options;
    }
}
