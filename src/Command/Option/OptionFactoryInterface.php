<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command\Option;

/**
 * Interface for factories that generate command option handler instances.
 */
interface OptionFactoryInterface
{

    /**
     * Generates a command option collection from an array of option definitions.
     *
     * @param mixed[] $options The array values each define a single command option handler. The value can be just a
     *                         the class name, or an array with the class name and any additional constructor arguments.
     *
     * @return OptionCollection A collection of command options defined by the $options parameter.
     *
     * @throws \InvalidArgumentException
     * @throws \ReflectionException
     */
    public function createCollection(array $options): OptionCollection;

    /**
     * Creates a single command option handler instance.
     *
     * @param string  $className The fully namespaced class name to use for this command option handler.
     * @param mixed[] $args      Additional arguments to pass to the command option constructor.
     *
     * @return CommandOptionInterface A new instance of the command option defined by the classname and arguments.
     *
     * @throws \InvalidArgumentException
     * @throws \ReflectionException
     */
    public function createOption(string $className, array $args = []): CommandOptionInterface;
}
