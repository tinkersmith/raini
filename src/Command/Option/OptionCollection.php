<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command\Option;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;

/**
 * A class to mantain a group of command options.
 */
class OptionCollection
{

    /**
     * List of command argument handlers.
     *
     * @var CommandOptionInterface[]
     */
    protected array $handlers = [];

    /**
     * Add a new console command option handler under the name identifier.
     *
     * @param string                 $name    The command option handler identifier.
     * @param CommandOptionInterface $handler The command option instance to add to this collection.
     */
    public function add(string $name, CommandOptionInterface $handler): void
    {
        $this->handlers[$name] = $handler;
    }

    /**
     * Get a console option handler by its name.
     *
     * @param string $name The option handler name, to find a matching option handler for.
     *
     * @return CommandOptionInterface|null The command option handler if one matching the requested name is available.
     */
    public function getHandler(string $name): ?CommandOptionInterface
    {
        return $this->handlers[$name] ?? null;
    }

    /**
     * Add all the console options from the option collection.
     *
     * @param Command $command The command to apply the console options to.
     */
    public function apply(Command $command): void
    {
        foreach ($this->handlers as $handler) {
            $handler->apply($command);
        }
    }

    /**
     * Get all the input option values from the collected option handlers.
     *
     * @param InputInterface $input The console input object with the raw command options.
     *
     * @return mixed[] All the command option values.
     */
    public function getValues(InputInterface $input)
    {
        $values = [];
        foreach ($this->handlers as $handler) {
            $values += $handler->getValues($input);
        }

        // With all option values discovered, allow the option handlers to
        // update their values based on other option values. An example where
        // this is important is the "environment" selection as many settings
        // can be overridden based on the environment selection.
        foreach ($this->handlers as $handler) {
            $handler->alterValues($values);
        }

        return $values;
    }
}
