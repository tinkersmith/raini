<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command\Option;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Interface for classes that provide console command options.
 *
 * The CommandOptionInterface is the interface for objects that are responsible
 * for defining, validating and resolving commandline options.
 */
interface CommandOptionInterface
{

    /**
     * Apply the console command options to a command object.
     *
     * @param Command $command The command to apply the options to.
     */
    public function apply(Command $command): void;

    /**
     * Get the parsed and validated option values for the provide options.
     *
     * @param InputInterface $input The command input object.
     *
     * @return mixed[]
     */
    public function getValues(InputInterface $input): array;

    /**
     * Allows the command option to alter values based on other option values.
     *
     * Some options have rely on the values of other options, and this allows
     * the handler to adjust values based on other values after they have all
     * been initially resolve. The "environment" option is notably a value
     * that many other options might rely on, and may need to load overridden
     * values from after it has been selected.
     *
     * @param mixed[] $values Reference to the current set of resolved option values.
     */
    public function alterValues(array &$values): void;
}
