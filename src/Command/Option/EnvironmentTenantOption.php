<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command\Option;

use Raini\Core\Environment\EnvironmentManagerInterface;
use Raini\Core\Project\SiteTenantInterface;
use Raini\Core\Project\TenantManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Command option for supporting "environment", "tenant" and "site" selection.
 *
 * Apply this to commands which target a tenant and site of the project.
 */
class EnvironmentTenantOption extends TenantOption implements CommandOptionInterface
{

    /**
     * The ID of the environment to use by default.
     *
     * This allows commands to set a command specific default, rather than the
     * normal default, which the environment manager would pick.
     *
     * @var string|null
     */
    public ?string $defaultEnvironment = null;

    /**
     * Constructs a new instance of the TenantOption command option handler.
     *
     * @param TenantManagerInterface      $tenantManager Tenant manager.
     * @param EnvironmentManagerInterface $envManager    The environment manager.
     * @param bool                        $includeSite   Should this command option include an option for site selection.
     */
    public function __construct(protected TenantManagerInterface $tenantManager, protected EnvironmentManagerInterface $envManager, protected bool $includeSite = true)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Command $command): void
    {
        $command->addOption('environment', 'e', InputOption::VALUE_REQUIRED, 'Name of the environment to use for the command. The default environment will be used if no value is provided.', null);

        // Only display the tenant option if there are multiple to choose from.
        if ($this->tenantManager->isMultiTenant()) {
            $command->addOption('tenant', 't', InputOption::VALUE_REQUIRED, 'Set the project tenant to target with this command. This option can also be used to specify and environment with the format "{environment}.{tentant}', null);

            if ($this->includeSite && $this->hasSites()) {
                $command->addOption('site', 's', InputOption::VALUE_REQUIRED, 'The target site for the command. If this is a multi-tenant project, specify a tenant with the --tenant (-t) option, or use the format "{tenant}.{site}"', null);
            }
        } elseif ($this->includeSite && $this->hasSites()) {
            $command->addOption('site', 's', InputOption::VALUE_REQUIRED, 'The target site for the command. You can specify the target environment by assigning this value with format "{environment}.{site}" (ex: "qa.my-site").', null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getValues(InputInterface $input): array
    {
        $envId = $input->getOption('environment') ?: $this->defaultEnvironment;
        if ($this->includeSite && $this->hasSites()) {
            $siteArg = $input->getOption('site');

            if ($this->tenantManager->isMultiTenant()) {
                if ($siteArg) {
                    // Either '<site>', '<tenant>.<site>', or '<env>.<tenant>.<site>'
                    $parts = explode('.', $siteArg, 3);

                    if (count($parts) < 2) {
                        // Apply the values from the tenant option, with environment fallback.
                        $tenantId = $input->getOption('tenant');
                        array_unshift($parts, ...$this->parseTenantOption($tenantId, $envId));
                    } elseif (count($parts) < 3) {
                        // Just apply the environment.
                        array_unshift($parts, $envId);
                    }
                } else {
                    $parts = $this->parseTenantOption($input->getOption('tenant'), $envId);
                    $parts[] = null;
                }
            } else {
                // Site may appear as '<site>' or '<env>.<site>'
                $parts = $siteArg ? explode('.', $siteArg, 2) : [$envId, null];

                if (count($parts) < 2) {
                    array_unshift($parts, $envId);
                }
                array_splice($parts, 1, 0, [null]);
            }
            [$envId, $tenantId, $siteId] = $parts;

            // Only a "site" specified, find first match by site ID only.
            if (empty($tenantId) && !empty($siteId)) {
                $retVal = $this->findSite($siteId);
            } else {
                $tenant = $this->tenantManager->getTenant($tenantId);
                $retVal = $tenant instanceof SiteTenantInterface
                    ? ['tenant' => $tenant, 'site' => ($siteId || $this->useDefaultSite) ? $tenant->getSite($siteId) : null]
                    : ['tenant' => $tenant];
            }
            $retVal['environment'] = $this->envManager->getEnvironment($envId);

            return $retVal;
        }

        // Multiple tenants, but no sites.
        [$envId, $tenantId] = $this->tenantManager->isMultiTenant()
            ? $this->parseTenantOption($input->getOption('tenant'), $envId)
            : [$envId, null];

        return [
            'environment' => $this->envManager->getEnvironment($envId),
            'tenant' => $this->tenantManager->getTenant($tenantId),
        ];
    }

    /**
     * Parse the command input option for tenant.
     *
     * The option can appear as "<tenant>" or "<env>.<tenant>" and this method
     * handles various formats correctly. The environment default is applied if
     * provided.
     *
     * @param string|null $tenantId The tenant option from the command input.
     * @param string|null $envId    The environment option from the command input.
     *
     * @return array<string|null> The environment ID and the tenant ID names in that order.
     */
    protected function parseTenantOption(?string $tenantId, ?string $envId): array
    {
        // Tenant might be listed as '<tenant>' or '<env>.<tenant>'
        $parts = $tenantId ? explode('.', $tenantId, 2) : [$envId, null];

        if (count($parts) < 2) {
            array_unshift($parts, $envId);
        }

        return $parts;
    }
}
