<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command\Option;

use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\Project\Exception\SiteNotFoundException;
use Raini\Core\Project\SiteInterface;
use Raini\Core\Project\SiteTenantInterface;
use Raini\Core\Project\Tenant;
use Raini\Core\Project\TenantManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Command option for supporting "tenant" and "site" selection.
 *
 * Apply this to commands which target a tenant and site of the project.
 */
class TenantOption implements CommandOptionInterface
{

    /**
     * When site value is excluded, should the default site value be used.
     *
     * If "FALSE" then the value for site can be false. This is useful when you
     * want to know that the user did not explicitly target a site value.
     *
     * @var bool
     */
    public bool $useDefaultSite = true;

    /**
     * Constructs a new instance of the TenantOption command option handler.
     *
     * @param TenantManagerInterface $tenantManager The tenant manager instance.
     * @param bool                   $includeSite   Should this command option include an option for site selection.
     */
    public function __construct(protected TenantManagerInterface $tenantManager, protected bool $includeSite = true)
    {
    }

    /**
     * Does this tenant type support having site definitions.
     *
     * Used to determine if the tenants of this Droplet supports having sites
     * and implements the SiteTenantInterface. This means that tenants can
     * be multi-site and have per site settings.
     *
     * @return bool Does this tenant type suppport sites? TRUE if it does.
     */
    public function hasSites(): bool
    {
        foreach ($this->tenantManager->getTenantClasses() as $className) {
            if (is_a($className, SiteTenantInterface::class, true)) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Command $command): void
    {
        $command->addOption('tenant', 't', InputOption::VALUE_REQUIRED, 'Set the tenant to use for this command.', null);

        if ($this->includeSite && $this->hasSites()) {
            $command->addOption('site', 's', InputOption::VALUE_REQUIRED, 'The target site for the command. If this is a multi-tenant project, specify a tenant with the --tenant (-t) option, or use the format "{tenant}.{site}"', null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getValues(InputInterface $input): array
    {
        if ($this->includeSite && $this->hasSites()) {
            // If site was specified in the format '<tenant>.<site>' then
            // the tenant option is ignored.
            $siteName = $input->getOption('site');
            $parts = $siteName
                ? explode('.', $siteName, 2)
                : [$input->getOption('tenant'), null];

            // Shuffle items as needed to ensure both values in the array.
            if (count($parts) < 2) {
                array_unshift($parts, $input->getOption('tenant'));
            }
            [$tenantId, $siteId] = $parts;

            // Only a "site" specified, find first match by site ID only.
            if (empty($tenantId) && !empty($siteId)) {
                return $this->findSite($siteId);
            }

            /** @var Tenant&SiteTenantInterface $tenant */
            $tenant = $this->tenantManager->getTenant($tenantId);

            return [
                'tenant' => $tenant,
                'site' => ($siteId || $this->useDefaultSite) ? $tenant->getSite($siteId) : null,
            ];
        } else {
            $tenantId = $input->getOption('tenant');

            return ['tenant' => $this->tenantManager->getTenant($tenantId)];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function alterValues(array &$values): void
    {
        $environment = $values['environment'] ?? null;

        // If the options collection values include an environment selection,
        // apply the tenant and site overrides from the active environment.
        if ($environment instanceof EnvironmentInterface) {
            /** @var Tenant&SiteTenantInterface $tenant */
            $tenant = $environment->createTenantOverrides($values['tenant']);

            // If site info was previously retrieved we'll want to update it
            // after the environment overrides have been applied.
            if ($tenant instanceof SiteTenantInterface && $this->includeSite && isset($values['site'])) {
                /** @var SiteInterface $site */
                $site = $values['site'];
                $values['site'] = $tenant->getSite($site->getName());
            }

            $values['tenant'] = $tenant;
        }
    }

    /**
     * Find a site that matches the $siteId.
     *
     * Used when a tenant has not been specified and have been only provided
     * the site identifier to locate. This method finds the first match and
     * returns the tenant and site objects for the matching site.
     *
     * @param string $siteId The site identifier to search for.
     *
     * @return mixed[] The resolved tenant and site values if the site can be found.
     *
     * @throws SiteNotFoundException when unable to find a site that matches the requested $siteId
     */
    protected function findSite(string $siteId): array
    {
        /** @var Tenant&SiteTenantInterface $tenant */
        foreach ($this->tenantManager->getTenants() as $tenant) {
            if (!$tenant instanceof SiteTenantInterface) {
                continue;
            }

            // If tenant has this site, return it. Otherwise keep
            // searching for a site that matches the name.
            if ($site = $tenant->getSite($siteId, false)) {
                return [
                    'tenant' => $tenant,
                    'site' => $site,
                ];
            }
        }

        // Couldn't find a match, report the error.
        throw new SiteNotFoundException("No site found that matches the name: {$siteId}.");
    }
}
