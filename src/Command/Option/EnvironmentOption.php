<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command\Option;

use Raini\Core\Environment\EnvironmentManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Provided consistent environment selection option for commands.
 */
class EnvironmentOption implements CommandOptionInterface
{

    /**
     * The ID of the environment to use by default.
     *
     * This allows commands to set a command specific default, rather than the
     * normal default, which the environment manager would pick.
     *
     * @var string|null
     */
    public ?string $defaultEnvironment = null;

    /**
     * Construct a new instance of the EnvironmentOption command option handler.
     *
     * @param EnvironmentManagerInterface $manager The environment manager service.
     */
    public function __construct(protected EnvironmentManagerInterface $manager)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Command $command): void
    {
        $command->addOption('environment', 'e', InputOption::VALUE_REQUIRED, 'Name of the environment to use for the command. The default environment will be used if no value is provided.');
    }

    /**
     * {@inheritdoc}
     */
    public function getValues(InputInterface $input): array
    {
        $environmentId = $input->getOption('environment') ?: $this->defaultEnvironment;

        return [
            'environment' => $this->manager->getEnvironment($environmentId),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function alterValues(array &$values): void
    {
    }
}
