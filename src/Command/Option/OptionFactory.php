<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command\Option;

use Raini\Core\DependencyInjection\ClassResolver;

/**
 * Factory for creating command option handlers.
 *
 * The factory assists with creating collections of options in addition to just
 * single option handler instances.
 */
class OptionFactory implements OptionFactoryInterface
{

    /**
     * Constructs a new instance of the command option factory.
     *
     * @param ClassResolver $classResolver The dependency injection aware class resolver.
     */
    public function __construct(protected ClassResolver $classResolver)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function createCollection(array $options): OptionCollection
    {
        $collection = new OptionCollection();

        // Generate the command options defined by the array.
        foreach ($options as $key => $handler) {
            if (!$handler instanceof CommandOptionInterface) {
                if (!is_array($handler)) {
                    $handler = [$handler, []];
                }
                $handler = $this->createOption($handler[0], $handler[1]);
            }

            // Add the instantiated command option to the collection.
            $collection->add($key, $handler);
        }

        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    public function createOption(string $className, array $args = []): CommandOptionInterface
    {
        return $this->classResolver->createInstance($className, $args);
    }
}
