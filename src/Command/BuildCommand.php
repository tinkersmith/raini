<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command;

use Raini\Core\Command\Option\EnvironmentTenantOption;
use Raini\Core\Command\Option\OptionCollection;
use Raini\Core\Command\Option\OptionFactoryInterface;
use Raini\Core\Extension\ExtensionManager;
use Raini\Core\Project\BuildOptions;
use Raini\Core\Project\ProjectBuilder;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\Exception\CliRuntimeException;

/**
 * Command to rebuild project assets, and deployment artifacts.
 */
#[AsCommand(
    'project:build',
    'Builds project assets, run updates and run build processes.',
)]
class BuildCommand extends Command
{
    /**
     * A collection of command option providers.
     *
     * @var OptionCollection
     */
    protected OptionCollection $options;

    /**
     * @param ExtensionManager       $extensionManager The extension manager.
     * @param ProjectBuilder         $builder          The project's build task service collector.
     * @param OptionFactoryInterface $optionFactory    The command option factory.
     */
    public function __construct(protected ExtensionManager $extensionManager, protected ProjectBuilder $builder, OptionFactoryInterface $optionFactory)
    {
        $this->options = $optionFactory->createCollection([
            'tenant' => EnvironmentTenantOption::class,
        ]);

        /** @var EnvironmentTenantOption $tenantOption */
        $tenantOption = $this->options->getHandler('tenant');
        $tenantOption->useDefaultSite = false;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $helpText = <<<EOT
            Build tasks are defined by the Raini core and extensions. Build tasks
            are defined and registered through the raini.servicesyml using the
            "build_task" container service tag.

            The default list of build task is defined set by the container parameter
            "project.build.tasks".

            The available list of build tasks are:

            EOT;

        foreach ($this->builder->getTaskLabels() as $id => $label) {
            $helpText .= " - $label ($id)\n";
        }

        $this
            ->setHelp($helpText)
            ->addOption('prod', null, InputOption::VALUE_NONE, 'Build for a production release.')
            ->addOption('silent', null, InputOption::VALUE_NONE, 'Silent build mode, only display errors and critical build information.')
            ->addOption('skip', null, InputOption::VALUE_IS_ARRAY|InputOption::VALUE_REQUIRED, 'Build tasks to skip. These tasks will NOT run.')
            ->addArgument('task', InputArgument::IS_ARRAY|InputArgument::OPTIONAL, 'List of build tasks to run. Leave blank to run default build tasks.');

        $this->options->apply($this);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $options = new BuildOptions();
            $options->isProduction = $input->getOption('prod') ?: false;
            $options->isSilent = $input->getOption('silent') ?: false;
            $options->skip = $input->getOption('skip');
            $options->tasks = $input->getArgument('task');

            // Set up environment and site targeting.
            $optValues = $this->options->getValues($input);
            $tenant = $optValues['tenant'];
            $options->targetSite = $optValues['site'] ?? null;
            $options->environment = $optValues['environment'] ?? null;

            $styler = new SymfonyStyle($input, $output);
            $this->builder->run($tenant, $options, $styler);

            return 0;
        } catch (RuntimeException|CliRuntimeException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        } catch (\InvalidArgumentException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        }

        return 1;
    }
}
