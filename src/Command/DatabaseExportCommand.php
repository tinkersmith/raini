<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command;

use Raini\Core\Event\Database\DatabaseEvents;
use Raini\Core\Event\Database\StructureTableEvent;
use Raini\Core\Project\DatabaseProviderInterface;
use Raini\Core\Project\Exception\SiteNotFoundException;
use Raini\Core\Project\Exception\TenantNotFoundException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\DockerContext;
use Tinkersmith\Console\Exception\CliRuntimeException;
use Tinkersmith\Console\Output\GZStreamOutput;

/**
 * Command to create database exports.
 */
#[AsCommand(
    'db:export',
    'Create a database dump.',
)]
class DatabaseExportCommand extends AbstractDatabaseCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $helpText = <<<EOT
            Export a database from a target tenant or site that define a database connection.
            EOT;

        $this
            ->setHelp($helpText)
            ->addOption('backup', null, InputOption::VALUE_NONE, 'Database export is meant to be used as a backup, and should not sanitize data.')
            ->addOption('no-compression', null, InputOption::VALUE_NONE, 'By default database exports are created with GZIP compression, this indicates that the resulting file should not be compressed.')
            ->addArgument('file', InputArgument::OPTIONAL, 'The database dump file to import.');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $errorLevel = error_reporting();
        try {
            @['environment' => $environment, 'site' => $site, 'tenant' => $tenant] = $this->options->getValues($input);

            // Create the mydump command.
            $context = $environment->createContext();
            if ($context instanceof DockerContext) {
                $context->setContainer('database');
            }

            $targetDb = $input->getOption('db');
            $dbProvider = $site ?: $tenant;
            if (!$dbProvider instanceof DatabaseProviderInterface) {
                $msg = sprintf('The resource named "%s" does not provide a database connection to export.', $dbProvider->getName());
                throw new \InvalidArgumentException($msg);
            }

            $dbInfo = $this->getDbInfo($targetDb, $dbProvider, $context);
            $driver = $this->dbManager->getDriver($dbInfo['type']);

            // Prevent notice an warnings produced by the process execution,
            // these create extra noise.
            error_reporting(E_ALL & ~E_NOTICE);

            // Create the output stream to store the database dump.
            $file = $input->getArgument('file') ?? $this->getDefaultFilename($dbProvider, $targetDb);
            $stream = gzopen($file, 'wb');
            $errOutput = $output instanceof ConsoleOutput ? $output->getErrorOutput() : $output;
            $gzOutput = new GZStreamOutput($stream, null, $errOutput);

            // Determine which tables are designated as structure only tables.
            $tables = $driver->getTables($dbInfo, $context);
            $structOnly = $this->getStructureTables($tables, $dbProvider, $targetDb, $input->getOption('backup'));

            return $driver->dump($dbInfo, $gzOutput, $structOnly, $context);
        } catch (RuntimeException|CliRuntimeException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        } catch (TenantNotFoundException|SiteNotFoundException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        } catch (\InvalidArgumentException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        } finally {
            // Restore the error reporting level.
            error_reporting($errorLevel);

            if (!empty($stream)) {
                gzclose($stream);
            }
        }

        return 1;
    }

    /**
     * Get the list of database tables that would be exported without data.
     *
     * Export resulting database tables only with their structure and without
     * the data. These tables might contain private data, or data that should
     * be regenerated on the target environments.
     *
     * @param string[]                  $tables   List of table names to filter from the database.
     * @param DatabaseProviderInterface $provider The database provider object.
     * @param string                    $targetDb The name of the target database.
     * @param bool                      $isBackup Is the database export meant to be a backup?
     *
     * @return string[] List of database table names that should be structure only (no data) in database export.
     *
     * @throws \LogicException
     * @throws ProcessFailedException
     * @throws RuntimeException
     */
    protected function getStructureTables(array $tables, DatabaseProviderInterface $provider, string $targetDb, bool $isBackup = false): array
    {
        // Allow extensions to alter the list of structure only tables.
        $event = new StructureTableEvent($provider, $targetDb, $isBackup);
        $this->eventDispatcher->dispatch($event, DatabaseEvents::STRUCTURE_TABLE_REGEX);

        if ($tableRegex = $event->getTablesRegex()) {
            $pattern = '/^('.implode('|', $tableRegex).')$/';

            // Find all existing tables that match
            return array_filter($tables, function ($table) use ($pattern) {
                return preg_match($pattern, $table);
            });
        }

        return [];
    }
}
