<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command;

use Raini\Core\Configurator\ProjectConfigurator;
use Raini\Core\Environment;
use Raini\Core\Environment\EnvironmentManagerInterface;
use Raini\Core\Extension\Exception\ExtensionNoHandlerException;
use Raini\Core\Extension\ExtensionList;
use Raini\Core\Extension\ExtensionManager;
use Raini\Core\MutableSettings;
use Raini\Core\Project\GenerateOptions;
use Raini\Core\Project\ProjectGenerator;
use Raini\Core\Project\ProjectInitializer;
use Raini\Core\Project\TenantManagerInterface;
use Raini\Core\Prompt\Prompter;
use Raini\Core\Settings;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Tinkersmith\Configurator\Attribute\Configurator;

/**
 * Creates a new project, or reconfigures an existing project.
 *
 * This command also generates the project files after all configurations have
 * been completed.
 */
#[AsCommand(
    name: 'project:create',
    description: 'Creates the Raini project settings with interactive prompts.',
    aliases: ['create'],
)]
class CreateCommand extends Command
{

    /**
     * @param Environment                 $env
     * @param Settings                    $settings
     * @param ExtensionList               $extList
     * @param ExtensionManager            $extensions
     * @param EnvironmentManagerInterface $environmentManager
     * @param TenantManagerInterface      $tenantManager
     * @param ProjectGenerator            $generator
     * @param ProjectInitializer          $initializer
     * @param Prompter                    $prompter
     */
    public function __construct(protected Environment $env, protected Settings $settings, protected ExtensionList $extList, protected ExtensionManager $extensions, protected EnvironmentManagerInterface $environmentManager, protected TenantManagerInterface $tenantManager, protected ProjectGenerator $generator, protected ProjectInitializer $initializer, protected Prompter $prompter)
    {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $helpText = <<<EOT
            Interactive prompts to generate a new Raini project.

            Ensure that you have at least a single "raini-droplet" package installed, as
            these are required to dictate the behavior of the project tenants and
            functionality.

            This command will guide you through building a Raini project configuration
            of the supported types and configure installed extensions and generate project
            files after the settings have been all prompted.

            Users can skip either the configuring of extensions or generating project file
            by using the available command options for "--no-extensions" or "--no-generate"
            respectively.
            EOT;

        $this
            ->setHelp($helpText)
            ->addOption('generate', null, InputOption::VALUE_NEGATABLE, 'Regenerate and initialize the project after configuring.')
            ->addOption('extensions', null, InputOption::VALUE_NEGATABLE, 'Configure extensions now, if not provided user will be prompted.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $settingsPath = $this->settings->getProjectFilepath();
            if (file_exists($settingsPath)) {
                // Throw error, suggest using the "setting" command instead.
                $existsErr = sprintf('<error>Project files already exist, create is for starting new project files. To update settings, use the "project:settings" command or alter the %s file directly.</>', $settingsPath);
                $output->writeln($existsErr);

                return 1;
            }

            $styler = new SymfonyStyle($input, $output);
            $configurator = new ProjectConfigurator($this->settings, $this->extList, $this->environmentManager);
            $defaults = $this->getDefaultValues();
            $schema = $configurator->getSchema();
            unset($schema['settings'], $schema['tenant']);

            $settings = [];
            foreach ($schema as $name => $prop) {
                $settings[$name] = $this->prompter->promptProperty($input, $output, $prop, $configurator, $defaults[$name] ?? null);
            }

            // Configure the Droplet settings, so we can provide the defaults
            // after we are able to determine the project type.
            $droplet = $this->extensions->getDroplet($settings['type']);
            $dropletLabel = sprintf('%s project settings', $droplet->getName());
            $dropletConfig = Configurator::fromClass(get_class($droplet));
            $settings['settings'] = $this->prompter->prompt($input, $output, $dropletConfig, $droplet->defaultSettings(), $dropletLabel);
            $droplet->setSettings($settings['settings']);

            // Configure a single tenant of the main project type.
            $styler->title(sprintf('Creating %s tenant:', $droplet->getName()));
            $tenantConfig = Configurator::fromClass($droplet::TENANT_CLASS);
            $settings['tenant'] = $this->prompter->prompt($input, $output, $tenantConfig);

            // Create mutable settings that can be altered and saved.
            $settings = new MutableSettings($this->env->getRainiPath(), $settings);

            // Optionally prompt users to configure extensions, can be skipped
            // if the input option "extensions" is negated.
            if ($input->getOption('extensions') ?? true) {
                foreach ($this->extensions->getExtensions() as $extension) {
                    try {
                        // Only prompt for extensions that have configurations.
                        if ($extConfig = Configurator::fromClass(get_class($extension))) {
                            $styler->title(sprintf('%s extension settings:', $extension->getName()));
                            $values = $this->prompter->prompt($input, $output, $extConfig, $extension->defaultSettings());
                            $settings->setExtensionSettings($extension->getId(), $values);
                            $extension->setSettings($values);
                        }
                    } catch (ExtensionNoHandlerException $e) {
                        // safely skip, no handler also means no configurations.
                    }
                }
            }
            $settings->dumpFile();

            // Generate the project after configuration file has been created.
            if ($input->getOption('generate') ?? true) {
                // Ensure that the settings are regenerated with updates.
                $this->settings->rebuild();
                $this->tenantManager->rebuild($this->settings);

                // Regenerate the project files from the configuration values.
                $options = new GenerateOptions();
                $options->mode = GenerateOptions::MODE_MERGE;
                $this->generator->run($options, $styler);
                $this->initializer->run($options, $styler);
            }

            return 0;
        } catch (\Exception $e) {
            $this->getApplication()->renderThrowable($e, $output);

            return 1;
        }
    }

    /**
     * Find default values from composer.json file to prepopulate project info.
     *
     * @return array
     *   An array of default values found from current project assets.
     */
    protected function getDefaultValues(): array
    {
        $defaults = [];
        try {
            $composer = Yaml::parseFile($this->env->getProjectDir().'/composer.json');
            if (!empty($composer['name'])) {
                $nameParts = explode('/', $composer['name'], 2);
                $defaults['id'] = array_pop($nameParts);
                $defaults['name'] = ucwords(strtr($defaults['id'], [
                    '_' => ' ',
                    '-' => ' ',
                ]));
            }
            $defaults['description'] = $composer['description'] ?? null;
        } catch (ParseException $e) {
        }

        return $defaults;
    }
}
