<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Command;

use Raini\Core\Command\Option\CommandOptionInterface;
use Raini\Core\Command\Option\EnvironmentOption;
use Raini\Core\Command\Option\OptionFactoryInterface;
use Raini\Core\Project\GenerateOptions;
use Raini\Core\Project\ProjectGenerator;
use Raini\Core\Project\ProjectInitializer;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\Exception\CliRuntimeException;

/**
 * Command which generates container and project files based on settings.
 *
 * Project files are created based on the configurations stored in the
 * "raini.project.yml" file, which can be manually edited or by using the
 * "project:config" command. The project files created here are meant to be
 * stored as part of the project repository.
 *
 * Overrides for local development environments are created by using the
 * "project:init" command. The generate command is meant to be run once
 * per project (or to update project resources), while the "project:init"
 * command is intended per user's local development environment.
 */
#[AsCommand(
    name: 'project:generate',
    description: 'Generate project assets based on "raini.project.yml" file.',
    aliases: ['generate'],
)]
class GenerateCommand extends Command
{

    /**
     * The command option handler for providing environment options.
     *
     * @var CommandOptionInterface
     */
    public CommandOptionInterface $environmentOption;

    /**
     * @param ProjectGenerator       $generator     The Raini project generator.
     * @param ProjectInitializer     $initializer   The project intializer to create local project files and overrides.
     * @param OptionFactoryInterface $optionFactory The command options factory.
     */
    public function __construct(protected ProjectGenerator $generator, protected ProjectInitializer $initializer, OptionFactoryInterface $optionFactory)
    {
        $this->environmentOption = $optionFactory->createOption(EnvironmentOption::class);

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this->setHelp(<<<EOT
            Generate project to build out the composer files and setup the base
            project. Content generated here are files that normally get added
            to your code repository.

            Use "raini init" to generate local file overrides and development
            file.
            EOT);

        $this->addOption('mode', null, InputOption::VALUE_REQUIRED, 'The write mode sets determines how existing files are handled (replacing, skipping, or merging).');
        $this->addOption('no-init', null, InputOption::VALUE_NONE, 'Generate files only and do not initialize the project fully.');
        $this->environmentOption->apply($this);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $options = new GenerateOptions();
            $options->mode = $input->getOption('mode') ?? GenerateOptions::MODE_MERGE;
            $options->environment = $this->environmentOption->getValues($input)['environment'];

            $styler = new SymfonyStyle($input, $output);
            $this->generator->run($options, $styler);
            if (!$input->getOption('no-init')) {
                $this->initializer->run($options, $styler);
            }

            return 0;
        } catch (RuntimeException|CliRuntimeException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        } catch (\InvalidArgumentException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        }

        return 1;
    }
}
