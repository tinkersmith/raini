<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Console;

use Tinkersmith\Console\CliInterface;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\Environment\EnvironmentInterface;

/**
 * Interface for CLI factories services.
 */
interface CliFactoryInterface
{

    /**
     * Gets the default execution context.
     *
     * Gets the execution context that should be used when no environment or
     * execution context is provided.
     *
     * @return ExecutionContextInterface The default execution context.
     */
    public function getDefaultContext(): ExecutionContextInterface;

    /**
     * Creates a new CLI command object.
     *
     * @param string|string[]                                            $command     The CLI shell command to run.
     * @param string|EnvironmentInterface|ExecutionContextInterface|null $environment The environment to send the command and arguments through
     */
    public function create(string|array $command, string|EnvironmentInterface|ExecutionContextInterface|null $environment = null): CliInterface;
}
