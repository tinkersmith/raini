<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Console;

use Raini\Core\Environment\EnvironmentManagerInterface;
use Tinkersmith\Console\Cli;
use Tinkersmith\Console\CliInterface;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\Environment\EnvironmentInterface;

/**
 * Default Raini implementation of the CLI factory.
 *
 * Has special handling for applying environment variables when running Composer
 * commands. This allows us to setup Composer, like memory usage, for any CLI
 * calls by adding options to the container parameters "composer_cli_options" in
 * a service file or container compiling functionality.
 */
class CliFactory implements CliFactoryInterface
{

    /**
     * @param mixed[]                     $cliOptions
     * @param EnvironmentManagerInterface $environmentManager The environment manager to run commands in the context of.
     */
    public function __construct(protected array $cliOptions, protected EnvironmentManagerInterface $environmentManager)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultContext(): ExecutionContextInterface
    {
        return $this->environmentManager
            ->getEnvironment()
            ->createContext();
    }

    /**
     * {@inheritdoc}
     */
    public function create(string|array $command, string|EnvironmentInterface|ExecutionContextInterface|null $context = null): CliInterface
    {

        $strCmd = (is_array($command) ? reset($command) : preg_split('/\s/', $command, 1, PREG_SPLIT_NO_EMPTY)[0]);
        $strCmd = basename($strCmd);
        $options = $this->cliOptions[$strCmd] ?? [];

        /** @var class-string<Cli> $classname */
        $classname = $options['class'] ?? Cli::class;
        $cli = new $classname($command);

        if (!empty($options)) {
            $cli->setOptions($options);
        }

        if (!$context instanceof ExecutionContextInterface) {
            if (!$context instanceof EnvironmentInterface) {
                $context = $this->environmentManager->getEnvironment($context);
            }
            $context = $context->createContext();
        }
        $cli->setContext($context);

        return $cli;
    }
}
