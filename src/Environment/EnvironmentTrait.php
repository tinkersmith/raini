<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment;

use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;

/**
 * Raini tenant based support for environment class definitions.
 */
trait EnvironmentTrait
{

    /**
     * Tenant override definitions for this environment.
     *
     * These allow settings to be overridden for this environment. For instance
     * this would allow us to target different hosts or passwords for different
     * remote environments or targets.
     *
     * @var mixed[]
     */
    protected array $tenants;

    /**
     * {@inheritdoc}
     */
    public function applyPathContext(array &$command, \Stringable|string $basePath): void
    {
        foreach ($command as $arg) {
            // Transform any paths relative to the project path with the
            // ABS flag to be absolute relative to the Docker container.
            if ($arg instanceof PathInfo && $arg->isAbs()) {
                $arg->__set('base', (string) $basePath);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createTenantOverrides(Tenant $tenant): Tenant
    {
        $values = $this->tenants[$tenant->getName()] ?? [];

        return $tenant->createOverridden($this->id(), $values);
    }

    /**
     * Translate the various YAML tenant formats into a the canonical format.
     *
     * To allow flexibility with tenant definitions there are a couple different
     * supported formats for defining tenants and tenant overrides. Regardless
     * for the configuration format (if supported), the result of this method
     * is a consistent tenant definition format.
     *
     * The dot notation shorthand:
     * Allow keys to be "tenant.property" where nested elements of a tenant
     * are separated by a '.' and is useful for defining overrides of a single
     * nested property.
     *
     * @param mixed[] $definitions The tenant definition overrides to convert into the canonical tenant format.
     *
     * @return mixed[] The tenant definitions in the canonical format.
     */
    protected static function fromTenantsDefinitions(array $definitions): array
    {
        /** @var mixed[] $tenants */
        $tenants = [];
        foreach ($definitions as $key => $values) {
            $parts = explode('.', $key);

            $ref = &$tenants;
            foreach ($parts as $i) {
                if (!isset($ref[$i])) {
                    $ref[$i] = [];
                }

                $ref = &$ref[$i];
            }
            $ref = $values;
            unset($ref);
        }

        return $tenants;
    }
}
