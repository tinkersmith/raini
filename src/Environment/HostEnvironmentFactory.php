<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment;

use Raini\Core\Environment;

/**
 * Default Raini environment factory.
 *
 * For most Raini provided environment types, this factory is sufficient for
 * creating new environment instances. For more complicate initialization needs
 * implement other factories which can make use of container services.
 */
class HostEnvironmentFactory implements EnvironmentFactoryInterface
{
    /**
     * @var \ReflectionClass<EnvironmentInterface>
     */
    protected \ReflectionClass $reflected;

    /**
     * @param class-string<EnvironmentInterface> $handlerClass The fully namespaced class to use when instantiating a new environment instance.
     * @param Environment                        $environment  The project ENVVAR and system information.
     */
    public function __construct(protected string $handlerClass, protected Environment $environment)
    {
        $this->reflected = new \ReflectionClass($handlerClass);
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return $this->handlerClass;
    }

    /**
     * {@inheritdoc}
     */
    public function create(string $id, array $definition): EnvironmentInterface
    {
        return $this->reflected->newInstance($id, $definition, $this->environment);
    }
}
