<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment;

use Raini\Core\Project\Tenant;
use Tinkersmith\Environment\EnvironmentInterface as ConsoleEnvironmentInterface;

/**
 * Interface for services which build the containerization project settings.
 *
 * Containerizers generate the Docker container settings files, based on the
 * container definitions provided by the project extensions. It is up to the
 * containerizer service to format and generate the correct set of files
 * for the Docker container system or tool it supports.
 *
 * For instance the default DockerContainerizer generates the
 * "docker-compose.yml", "docker-compose.overrides.yml" and the dockerfiles for
 * containers, volumes and networks the project defines. A different
 * containerizer service might generate the 'docksal.yml' files or Docksal, or
 * for Lando or for DDev.
 */
interface EnvironmentInterface extends ConsoleEnvironmentInterface
{

    /**
     * Create a new tenant with the environment specific tenant overrides.
     *
     * This method creates a new tenant with the overrides applied to it. This
     * keeps the original tenant unaltered, and can be used other environments.
     *
     * @param Tenant $tenant The tenant to generate environment overrides for.
     *
     * @return Tenant A new tenant instance with overrides applied.
     */
    public function createTenantOverrides(Tenant $tenant): Tenant;
}
