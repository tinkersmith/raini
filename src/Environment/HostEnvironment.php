<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment;

use Raini\Core\Environment;
use Tinkersmith\Console\CliInterface;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\Environment\Exception\UnsupportedOptionException;
use Tinkersmith\Environment\HostEnvironment as ConsoleHostEnvironment;

/**
 * A NULL containerizer implementation.
 *
 * This is a NULL pattern implementation for the environment interface. It runs
 * the project CLI commands directly on the  host system and allows projects to
 * utilize Raini without needing to use containers or remote environments.
 */
class HostEnvironment extends ConsoleHostEnvironment implements EnvironmentInterface
{

    use EnvironmentTrait;

    /**
     * Creates a new instance of an Environment class.
     *
     * @param string      $id         The environment identifier.
     * @param mixed[]     $definition Environment definitions from the project settings.
     * @param Environment $env        The running environment info object.
     */
    public function __construct(protected string $id, protected array $definition, protected Environment $env)
    {
        $this->tenants = self::fromTenantsDefinitions($definition['tenants'] ?? []);
    }

    /**
     * {@inheritdoc}
     */
    public function alterCommand(string|array $command, CliInterface $cli, ExecutionContextInterface $context): array
    {
        if ($context->getRunAs()) {
            // @todo - add the `sudo` execution option.
            throw new UnsupportedOptionException();
        }

        $command = is_array($command) ? $command : [$command];
        $this->applyPathContext($command, $context->getCWD() ?: $this->env->getProjectDir());

        return $command;
    }
}
