<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment;

/**
 * Interface for implementing environment factories.
 *
 * Environment factories create the environment instances from the project
 * environment configurations ("raini.project.yml" file), and will typically
 * get instantiated by the environment manager.
 *
 * @see EnvironmentManagerInterface::getEnvironment()
 */
interface EnvironmentFactoryInterface
{

    /**
     * Get the fully namespaced class name of the environment handler.
     *
     * @return class-string<EnvironmentInterface> The environment class type.
     */
    public function getClass(): string;

    /**
     * Create a new environment instance from an environment definition array.
     *
     * @param string  $id         The environment handler type identifier.
     * @param mixed[] $definition Environment definitions from the project settings.
     *
     * @return EnvironmentInterface An environment generated based on the provided configuration settings.
     */
    public function create(string $id, array $definition): EnvironmentInterface;
}
