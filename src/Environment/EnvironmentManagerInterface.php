<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment;

use Raini\Core\Environment\Exception\HandlerNotFoundException;

/**
 * Manager for containerization handlers.
 *
 * The manager collects and retrieves the available containerization handlers
 * and makes them available for the rest of the application. This allows Raini
 * to build different configurations or route commands through Docker containers
 * for different containerization methods (DDev, Lando, Docksal, etc...).
 *
 * @see HandlerInterface
 */
interface EnvironmentManagerInterface
{

    /**
     * Get a list of available environment identifiers.
     *
     * Environments are defined in the "<raini-dir>/environments.yml" file and
     * the "environment" property from the "raini.project.yml" file.
     *
     * @return string[] The environment identifiers for currently registered environments.
     */
    public function getEnvironmentOptions(): array;

    /**
     * @param string|null $id The ID for the containerization service handler to retrieve, the default if null.
     *
     * @return EnvironmentInterface A containerizer service handler that matches the requested ID.
     *
     * @throws HandlerNotFoundException No environment handler found matching the requested ID.
     */
    public function getEnvironment(?string $id = null): EnvironmentInterface;

    /**
     * @param string $id The environment handler type identifier.
     *
     * @return EnvironmentFactoryInterface Factory for creating environment handlers matching the environment handler
     *                                     type maching the requested handler ID.
     *
     * @throws HandlerNotFoundException No environment handler found matching the requested ID.
     */
    public function getFactory(string $id): EnvironmentFactoryInterface;

    /**
     * Get the list of registered environment factories.
     *
     * @return EnvironmentFactoryInterface[] The list of environment factories.
     */
    public function getFactories(): array;

    /**
     * @param EnvironmentFactoryInterface $factory The containerization service handler to register.
     * @param string                      $id      The identifier for this containerization handler.
     */
    public function addFactory(EnvironmentFactoryInterface $factory, string $id): void;
}
