<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment\Builder;

use Raini\Core\Settings;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

/**
 * Generate the settings files and definitions for a Docker environment.
 */
class DockerEnvironmentBuilder implements EnvironmentBuilderInterface
{

    const DOCKERFILE_CUSTOM_MARKER = <<<EOT
        # ========================================
        # Add image customizations below this line
        # ========================================
        EOT;


    /**
     * Creates a new instance of the DockerEnvironmentBuilder class.
     *
     * @param Settings $settings The project settings object.
     */
    public function __construct(protected Settings $settings)
    {
    }

    /**
     * Generate project files for the docker compose and container dockerfiles.
     *
     * @param mixed[] $containers Definitions of Docker services and images to build.
     * @param mixed[] $volumes    Definitions of Docker volumes use by the project.
     * @param mixed[] $networks   Definitions of Docker networks use by the project.
     */
    public function createFiles(array $containers, array $volumes = [], array $networks = []): void
    {
        $projectId = $this->settings->getId();

        foreach ($containers as $name => $definition) {
            if (!empty($definition['template'])) {
                $containers[$name] = $this->buildImageFromTemplate($name, $definition['template']) + $definition;
                unset($containers[$name]['template']);
            }

            // Ensure a consistent docker container name, prefixed with the
            // project ID.
            $containers[$name] = [
                'container_name' => $projectId.'-'.$name,
            ] + $containers[$name];
        }

        $dockerDefs = [
            'version' => '3.3',
            'services' => $containers,
        ];

        if ($volumes) {
            $dockerDefs['volumes'] = $volumes;
        }
        if ($networks) {
            $dockerDefs['networks'] = $networks;
        }

        $this->dumpDockerCompose('docker-compose.yml', $dockerDefs);
    }

    /**
     * Creates an overrides file and resources for local environments.
     */
    public function createOverrides(): void
    {
        $overrideDefs = [
            'version' => '3.3',
            'services' => [],
        ];

        $this->dumpDockerCompose('docker-compose.override.yml', $overrideDefs);
    }

    /**
     * Create a Docker image file from a template definition.
     *
     * The template definitions, should specify the template file to use, and
     * the replacement variables to use for the template values. The "variables"
     * should match the variable in the "{{ variable }}" placeholders. Any
     * placeholders without values will get removed.
     *
     * @param string      $name       The container name.
     * @param mixed[]     $definition The container template definition.
     * @param string|null $filepath   The path to place the Dockerfiles of the container image file.
     *
     * @return mixed[] The container definition that should replace the template definition.
     */
    protected function buildImageFromTemplate(string $name, array $definition, ?string $filepath = null): array
    {
        $content = file_get_contents($definition['file']);

        if (is_readable($filepath)) {
            $existing = file_get_contents($filepath);

            $pos = strpos($existing, static::DOCKERFILE_CUSTOM_MARKER);
            $customizations = $pos !== false ? substr($existing, $pos) : static::DOCKERFILE_CUSTOM_MARKER;

            if ($pos = strpos($content, static::DOCKERFILE_CUSTOM_MARKER)) {
                $content = substr($content, 0, $pos);
            }
        }

        $content = preg_replace_callback('/\{\{ ([\w_]+) \}\}/', function ($matches) use ($definition): string {
            return $definition['variables'][$matches[1]] ?? '';
        }, $content);

        if (isset($customizations)) {
            $content .= $customizations;
        }

        // Write the image definition file contents to the image dockerfile.
        $fs = new Filesystem();
        $fs->dumpFile($filepath, $content);

        // Write to the dockerfile and build context to the docker-compose file.
        return [
            'image' => $this->settings->getId()."/{$name}:latest",
            'build' => [
                'context' => \dirname($filepath),
                'args' => $definition['args'],
            ],
        ];
    }

    /**
     * Generate the Docker compose file from the container definitions.
     *
     * The Docker definitions consist of container services, volumes and network
     * definitions as described by the Docker compose documentation.
     *
     * @param string  $filepath    The docker-compose output file path.
     * @param mixed[] $definitions The docker service container, volumes and network definitions to generate output for.
     */
    protected function dumpDockerCompose(string $filepath, array $definitions): void
    {
        $yaml = new Yaml();
        $fs = new Filesystem();
        $content = $yaml->dump($definitions, 5, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
        $fs->dumpFile($filepath, $content);
    }
}
