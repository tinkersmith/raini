<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment;

use Raini\Core\Configurator\ExtensionSchemaConfigurator;
use Symfony\Component\Process\Process;
use Tinkersmith\Configurator\Attribute\Configurator;
use Tinkersmith\Console\Cli;
use Tinkersmith\Console\CliInterface;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\Environment\AbstractEnvironment;

/**
 * Utilize CLI commands through an SSH connection.
 *
 * Allows running through a remote environment through a SSH interface. The
 * environment configurations manage the SSH connection parameters and .
 */
#[Configurator(ExtensionSchemaConfigurator::class, 'core', 'config/env/ssh.schema.yml')]
class SshEnvironment extends AbstractEnvironment implements EnvironmentInterface
{

    use EnvironmentTrait;

    /**
     * Creates a new instance of an Environment class.
     *
     * @param string  $id         The environment identifier.
     * @param mixed[] $definition Environment definitions from the project settings.
     */
    public function __construct(protected string $id, protected array $definition)
    {
        $this->tenants = self::fromTenantsDefinitions($definition['tenants'] ?? []);
    }

    /**
     * {@inheritdoc}
     */
    public function alterCommand(string|array $command, CliInterface $cli, ExecutionContextInterface $context): array
    {
        $ssh = ['ssh'];
        $def = $this->getDefinition() + [
            'user' => null,
            'projectDir' => null,
        ];

        if (!empty($def['tty'])) {
            $ssh[] = '-t';
        }
        if (!empty($def['keyfile'])) {
            $ssh[] = '-i';
            $ssh[] = $def['keyfile'];
        }
        if (!empty($def['port'])) {
            $ssh[] = '-p';
            $ssh[] = $def['port'];
        }
        if (!empty($def['options'])) {
            $options = $def['options'];
            if (!is_array($options)) {
                $options = [$options];
            }

            foreach ($options as $opt) {
                $ssh[] = '-o';
                $ssh[] = $opt;
            }
        }

        if ($env = $cli->getEnv()) {
            foreach (array_keys($env) as $name) {
                $alter[] = '-o';
                $alter[] = "SendEnv={$name}";
            }
        }

        $connectStr = $def['host'];
        $user = $context->getRunAs() ?: $def['user'];
        if ($user) {
            // Prefer to use SSH keys over using passwords but still want to
            // support this feature for times when it is needed.
            if (!empty($def['pass'])) {
                $user .= ':'.$def['pass'];
            }

            $connectStr = $user.'@'.$connectStr;
        }
        $ssh[] = $connectStr;

        // Ensure that command is an array.
        if (!is_array($command)) {
            $command = [$command];
        }

        // Check if there is a configured working directory that commands should
        // be run relative to. This is usually needed for SSH remote
        // environments because default loading into the user's home directory
        // while the project is usually in an application directory.
        //
        // We prefer the working directory set in an execution context, and
        // fallback to an environment "projectDir" settings.
        $workDir = $context->getCWD() ?? $def['projectDir'];
        if ($workDir) {
            $this->applyPathContext($command, $workDir);

            // Sanitize the user command and add it to SSH command output.
            $cmdStr = implode(' ', array_map(Cli::escapeArgument(...), $command));
            $ssh[] = "cd {$workDir} && $cmdStr";
        } else {
            $ssh = array_merge($ssh, $command);
        }

        return $ssh;
    }

    /**
     * {@inheritdoc}
     */
    public function alterCliProcess(Process $process, CliInterface $cli, ExecutionContextInterface $context): void
    {
        // Ensure that TTY is disabled for this command.
        $process->setTty(false);

        // Set envvars so `SendEnv` can set the values through SSH.
        if ($env = $cli->getEnv()) {
            $process->setEnv($env);
        }
    }
}
