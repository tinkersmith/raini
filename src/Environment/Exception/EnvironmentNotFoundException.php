<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment\Exception;

/**
 * Exception thrown when an environment is not defined or cannot be found.
 */
class EnvironmentNotFoundException extends \Exception
{

    const ERROR_MSG = 'No environment with key "%s" is not available. Check your "raini.project.yml" or "environments.yml" to ensure they have defined an environment with a key of "%s".';

    /**
     * @param string          $environmentId The requested containerizer service identifier.
     * @param int             $code          An numeric error code.
     * @param \Throwable|null $previous      A previous exception if exceptions are being chained.
     */
    public function __construct(string $environmentId, int $code = 0, ?\Throwable $previous = null)
    {
        $message = sprintf(static::ERROR_MSG, $environmentId, $environmentId);

        parent::__construct($message, $code, $previous);
    }
}
