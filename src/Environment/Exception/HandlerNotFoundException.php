<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment\Exception;

/**
 * Exception thrown when a requested environment handler is not available.
 *
 * This can happen if a project has a environment handler that isn't
 * installed because of a missing extension or an invalid handler ID
 * was provided by the "project.raini.yml" settings.
 */
class HandlerNotFoundException extends \Exception
{

    const ERROR_MSG = 'The %s environment handler is not available. Either the extension providing this is missing or the type is invalid.';

    /**
     * @param string          $environmentType The environment handler type identifier.
     * @param int             $code            An numeric error code.
     * @param \Throwable|null $previous        A previous exception if exceptions are being chained.
     */
    public function __construct(string $environmentType, int $code = 0, ?\Throwable $previous = null)
    {
        $message = sprintf(static::ERROR_MSG, $environmentType);

        parent::__construct($message, $code, $previous);
    }
}
