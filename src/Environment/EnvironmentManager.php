<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment;

use Raini\Core\Environment;
use Raini\Core\Environment\Exception\EnvironmentNotFoundException;
use Raini\Core\Environment\Exception\HandlerNotFoundException;
use Raini\Core\Settings;
use Symfony\Component\Yaml\Yaml;

/**
 * Manager for loading environment handlers.
 *
 * The manager collects and retrieves the available environment definitions from
 * the project settings (<raini-dir>/raini.project.yml) and the environments
 * file (<raini_dir>/environments.yml). Definitions in the environments file
 * take precedence over environment definitions in the project file.
 *
 * Environments allow Raini to build different configurations or route commands
 * through Docker containers for different containerization methods (DDev,
 * Lando, Docksal, etc...) and execute commands through remote connections.
 *
 * @see EnvironmentInterface
 */
class EnvironmentManager implements EnvironmentManagerInterface
{

    /**
     * Loaded environment definitions (including tenant overrides).
     *
     * @var mixed[]
     */
    protected array $definitions = [];

    /**
     * @var EnvironmentFactoryInterface[]
     */
    protected array $factories = [];

    /**
     * @var EnvironmentInterface[]
     */
    protected array $environments = [];

    /**
     * @param Environment $env      Project environment variables, system settings.
     * @param Settings    $settings Project configuration settings.
     */
    public function __construct(Environment $env, Settings $settings)
    {
        $environmentFile = $env->getRainiPath().'/environments.yml';
        if (is_readable($environmentFile)) {
            $this->definitions = Yaml::parseFile($environmentFile, YAML::PARSE_CUSTOM_TAGS) + $settings->getEnvironments();
        } else {
            $this->definitions = $settings->getEnvironments();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFactory(string $type): EnvironmentFactoryInterface
    {
        if (isset($this->factories[$type])) {
            return $this->factories[$type];
        }

        throw new HandlerNotFoundException($type);
    }

    /**
     * {@inheritdoc}
     */
    public function getFactories(): array
    {
        return $this->factories;
    }

    /**
     * {@inheritdoc}
     */
    public function getEnvironmentOptions(): array
    {
        return array_keys($this->definitions);
    }

    /**
     * @param string|null $id The environment handler to retrieve. If NULL, fetch the default environment.
     *
     * @return EnvironmentInterface An environment handler that matches the requested environment ID.
     *
     * @throws HandlerNotFoundException No environment handler found matching the requested environment type.
     */
    public function getEnvironment(?string $id = null): EnvironmentInterface
    {
        // Use the default environment. Either "default" or "local" if an
        // environment definition exists are the possible defaults.
        if (!$id) {
            if (isset($this->definitions['default']['type'])) {
                $id = 'default';
            } elseif (isset($this->definitions['local']['type'])) {
                $id = 'local';
            } else {
                throw new EnvironmentNotFoundException('NULL');
            }
        }

        // If environment is already built and the environment type is valid.
        if (empty($this->environments[$id])) {
            $definition = $this->definitions[$id] ?? null;

            if ($definition) {
                $factory = $this->getFactory($definition['type']);
                $this->environments[$id] = $factory->create($id, $definition);
            } else {
                throw new EnvironmentNotFoundException($id);
            }
        }

        return $this->environments[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function addFactory(EnvironmentFactoryInterface $factory, string $id): void
    {
        $this->factories[$id] = $factory;
    }
}
