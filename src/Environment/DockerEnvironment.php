<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Environment;

use Raini\Core\Configurator\ExtensionSchemaConfigurator;
use Tinkersmith\Configurator\Attribute\Configurator;
use Tinkersmith\Environment\DockerEnvironment as ConsoleDockerEnvironment;
use Tinkersmith\Environment\DockerEnvironmentInterface;

/**
 * The vanilla Docker containerizer implementation with Docker Compose.
 *
 * This containerizer generates "docker-composer.yml" and the needed dockerfiles
 * for the project and will generated "docker-composer.overrides.yml" file for
 * the local development file overrides.
 */
 #[Configurator(ExtensionSchemaConfigurator::class, 'core', 'config/env/docker.schema.yml')]
class DockerEnvironment extends ConsoleDockerEnvironment implements EnvironmentInterface, DockerEnvironmentInterface
{

    use EnvironmentTrait;

    /**
     * Creates a new instance of an Environment class.
     *
     * @param string  $id         The environment identifier.
     * @param mixed[] $definition Environment definitions from the project settings.
     */
    public function __construct(protected string $id, protected array $definition)
    {
        $this->tenants = self::fromTenantsDefinitions($definition['tenants'] ?? []);
    }
}
