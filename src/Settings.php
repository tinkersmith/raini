<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core;

use Symfony\Component\Yaml\Yaml;

/**
 * Project settings class, which loads and manages the project settings.
 *
 * @property mixed[] $nodeBuild
 */
#[\AllowDynamicProperties]
class Settings
{

    /**
     * The project settings data.
     *
     * @var mixed[]
     */
    protected array $data;

    /**
     * Loaded environment definitions (including tenant overrides).
     *
     * @see static::getEnvironments()
     *
     * @var mixed[]
     */
    protected ?array $environments;

    /**
     * @param string $rainiDir The path to the base project directory.
     */
    public function __construct(protected string $rainiDir)
    {
        $this->rebuild();
    }

    /**
     * Rebuild the settings configurations from the current file settings.
     *
     * @return self
     */
    public function rebuild(): self
    {
        $path = $this->getProjectFilepath();
        $this->data = file_exists($path) ? Yaml::parseFile($path, Yaml::PARSE_CUSTOM_TAGS) : [];

        // Apply minimal default settings.
        $this->data += [
            'id' => 'raini',
            'type' => null,
            'environments' => [
                'default' => ['type' => 'docker'],
            ],
        ];

        return $this;
    }

    /**
     * Get the path to the Raini project settings file.
     *
     * @return string The path to the Raini project settings file, relative to the project root.
     */
    public function getProjectFilepath(): string
    {
        return $this->rainiDir.'/raini.project.yml';
    }

    /**
     * @return string The project identifier.
     */
    public function getId(): string
    {
        return $this->data['id'];
    }

    /**
     * @return string The project display name.
     */
    public function getName(): string
    {
        return $this->data['name'];
    }

    /**
     * Get the Raini project type or droplet extension.
     *
     * @return string|null The Raini project type (the project's Droplet extension).
     */
    public function getType(): ?string
    {
        return $this->data['type'];
    }

    /**
     * Does the settings key exist.
     *
     * @param string $name The settings data value to validate exists.
     *
     * @return bool Is the data value with key $name exist in the settings data.
     */
    public function __isset(string $name): bool
    {
        return isset($this->data[$name]);
    }

    /**
     * Get a setting property by the name in the YAML file.
     *
     * Only top level settings are available to be fetched from here.
     *
     * @param string $name The property name of the setting to retrieve.
     *
     * @return mixed|null The setting value matching the property name requested. NULL if no property matching the
     *                    requested name is available.
     */
    public function __get(string $name): mixed
    {
        return $this->data[$name] ?? null;
    }

    /**
     * Gets a mutable version of the settings.
     *
     * Most uses of the settings should never alter the settings, and in order
     * to protect from accidental modifications we make this object immutable.
     *
     * For writing settings and altering configurations, use the mutable
     * instance version of the settings class.
     *
     * @return MutableSettings A mutable version of the settings for configuration changes.
     */
    public function getMutable(): MutableSettings
    {
        return new MutableSettings($this->rainiDir, $this->data);
    }

    /**
     * Gets the list of environment names included in the project.
     *
     * Environments might be "dev", "stage" or "prod" and are used with Raini
     * commands to target different containers (ie Docker, Lando, DDEV, etc...)
     * or remote environments (like through SSH).
     *
     * Initially NULL until first requested. The following will be combine as
     * when they are available (in order or precedence):
     *  - local overrides (local.environments.yml)
     *  - environments file (environments.yml)
     *  - project settings (ranini.project.yml)
     *
     * That means that a definition in the local overrides will replace settings
     * in the environments file or the project settings.
     *
     * @return mixed[] Get a list of environments configure for this Raini project.
     */
    public function getEnvironments(): array
    {
        $envs = $this->data['environments'] ?? [];
        if (!empty($this->data['environment']['type'])) {
            $envs['default'] = $this->data['environment'];
        }

        return $envs;
    }

    /**
     * Gets the list of project tenant definitions.
     *
     * The tenant definitions are depended on the project Droplet extension
     * which is active. The tenant definitions all should implement the
     * \Raini\Core\Project\Tenant class or are a derivative of
     * this class.
     *
     * @return mixed[] Tenant definitions from the settings file.
     */
    public function getTenants(): array
    {
        $tenants = $this->data['tenants'] ?? [];
        if (!empty($this->data['tenant'])) {
            $tenants[$this->getId()] = $this->data['tenant'];
        }

        return $tenants;
    }

    /**
     * @return mixed[] Settings for the active Droplet extension.
     */
    public function getDropletSettings(): array
    {
        return $this->data['settings'] ?? [];
    }

    /**
     * @param string $name The name of the extension to fetch the settings for.
     *
     * @return mixed[] Settings for the requested extension.
     */
    public function getExtensionSettings(string $name): array
    {
        return $this->data['extensions'][$name] ?? [];
    }
}
