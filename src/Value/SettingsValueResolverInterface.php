<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Value;

use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\Value\Exception\UnknownValueTagException;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * Resolver service for resolving values from settings arrays, or single values.
 */
interface SettingsValueResolverInterface extends ValueResolverInterface
{

    /**
     * @param ValueResolverInterface $resolver A value resolver to add. The tags supported by this resolver is added to
     *                                         the tag types supported by this settings resolver. This resolver will be
     *                                         called anytime a tagged value with a supported tag is encountered.
     */
    public function addResolver(ValueResolverInterface $resolver): void;

    /**
     * @param mixed                                               $settings The settings values to resolve.
     * @param EnvironmentInterface|ExecutionContextInterface|null $context  The environment context to use to resolve the values with.
     *
     * @return mixed The original value with any tagged values resolved.
     *
     * @throws UnknownValueTagException When a value tag that doesn't match any value resolver is encountered.
     */
    public function resolveSettings(mixed $settings, EnvironmentInterface|ExecutionContextInterface|null $context = null): mixed;
}
