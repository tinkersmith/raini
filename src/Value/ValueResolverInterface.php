<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Value;

use Raini\Core\Environment\EnvironmentInterface;
use Symfony\Component\Yaml\Tag\TaggedValue;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\SettingsBuilder\Php\Expr\ExpressionInterface;

/**
 * Base interface for resolving tagged values.
 */
interface ValueResolverInterface
{

    /**
     * Get the list of tagged value tags that this resolver supports.
     *
     * @return string[] List of tags this resolver supports.
     */
    public function getTags(): array;

    /**
     * Resolve a tagged value object into a useable value.
     *
     * Resolving values can be expensive depending on the underlying encryption
     * or API calls that need to be made to retrieve a value. Some ideas for
     * optimizing this would be to only resolve values when they are needed
     * (avoid resolving all value for the settings file when only one database
     * setting is being used), and cache information as much as possible to
     * avoid loading expensive API calls over and over.
     *
     * @param TaggedValue                                         $value   The value with tag information to be resolved into a plain value.
     * @param EnvironmentInterface|ExecutionContextInterface|null $context The environment context to use when resolving values.
     *
     * @return mixed The resolved value.
     */
    public function resolve(TaggedValue $value, EnvironmentInterface|ExecutionContextInterface|null $context = null): mixed;

    /**
     *
     *
     * @param TaggedValue $value The value with tag information to be resolved into a plain value.
     *
     * @return ExpressionInterface
     */
    public function valueExpression(TaggedValue $value): ExpressionInterface;
}
