<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Value\Exception;

use Symfony\Component\Yaml\Tag\TaggedValue;

/**
 * Exception thrown when there is an unsupported tagged value to parse.
 *
 * Normally this is a result of a missing value resolver (missing extension) or
 * there might be a typo with the tag.
 */
class UnknownValueTagException extends \Exception
{

    /**
     * @param TaggedValue $value The value to resolve, which failed.
     */
    public function __construct(TaggedValue $value)
    {
        $message = sprintf('Unrecognized value tag "%s" with value of "%s". Is an extension supporting this tag missing?', $value->getTag(), $value->getValue());

        parent::__construct($message);
    }
}
