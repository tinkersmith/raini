<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Value;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\Utility\EnvValues;
use Symfony\Component\Yaml\Tag\TaggedValue;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\Console\Output\BufferedOutput;
use Tinkersmith\SettingsBuilder\Php\Expr\Expression;
use Tinkersmith\SettingsBuilder\Php\Expr\ExpressionInterface;

/**
 * Resolve tagged values that reference environment variables.
 */
class EnvValueResolver implements ValueResolverInterface
{

    /**
     * The loaded LANDO_INFO data keyed by the environment ID.
     *
     * @var mixed[]
     */
    protected array $data = [];

    /**
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getTags(): array
    {
        return ['env'];
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(TaggedValue $value, EnvironmentInterface|ExecutionContextInterface|null $context = null): mixed
    {
        if (!$context || 'host' === $context->getType()) {
            $val = getenv($value->getValue());
        } else {
            $envId = $context->id();
            if (!isset($this->data[$envId])) {
                $output = new BufferedOutput();
                $this->cliFactory
                    ->create('env', $context)
                    ->execute([], $output);

                $this->data[$envId] = EnvValues::createFromText($output->fetch());
            }

            $val = $this->data[$envId][$value->getValue()] ?? null;
        }

        if (false === $val) {
            $err = sprintf('Environment variable %s is missing or not available.', $value->getValue());
            throw new \InvalidArgumentException($err);
        }

        return $val;
    }

    /**
     * {@inheritdoc}
     */
    public function valueExpression(TaggedValue $value): ExpressionInterface
    {
        $envName = $value->getValue();

        return new Expression("getenv('$envName')");
    }
}
