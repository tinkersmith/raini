<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Value;

use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\Value\Exception\UnknownValueTagException;
use Symfony\Component\Yaml\Tag\TaggedValue;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\SettingsBuilder\Php\Expr\ExpressionInterface;

/**
 * The default settings resolver.
 */
class SettingsValueResolver implements SettingsValueResolverInterface, ValueResolverInterface
{

    /**
     * Info about handlers which have been registered to the this collection.
     *
     * @var ValueResolverInterface[]
     */
    protected $resolvers = [];

    /**
     * {@inheritdoc}
     */
    public function getTags(): array
    {
        $tags = [];
        foreach ($this->resolvers as $resolver) {
            $tags = [...$tags, ...$resolver->getTags()];
        }

        return $tags;
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(TaggedValue $value, EnvironmentInterface|ExecutionContextInterface|null $context = null): mixed
    {
        $resolver = $this->resolvers[$value->getTag()] ?? null;

        if ($resolver instanceof ValueResolverInterface) {
            return $resolver->resolve($value, $context);
        }
        throw new UnknownValueTagException($value);
    }

    /**
     * {@inheritdoc}
     */
    public function valueExpression(TaggedValue $value): ExpressionInterface
    {
        $resolver = $this->resolvers[$value->getTag()] ?? null;

        if ($resolver instanceof ValueResolverInterface) {
            return $resolver->valueExpression($value);
        }
        throw new UnknownValueTagException($value);
    }

    /**
     * {@inheritdoc}
     */
    public function addResolver(ValueResolverInterface $resolver): void
    {
        $this->resolvers += array_fill_keys($resolver->getTags(), $resolver);
    }

    /**
     * {@inheritdoc}
     */
    public function resolveSettings(mixed $settings, EnvironmentInterface|ExecutionContextInterface|null $context = null): mixed
    {
        if ($settings instanceof TaggedValue) {
            return $this->resolve($settings, $context);
        }

        if (is_array($settings)) {
            $transformed = [];
            foreach ($settings as $key => $value) {
                // Recursively traverse the values looking for tagged values
                // to resolve.
                $transformed[$key] = $this->resolveSettings($value, $context);
            }

            return $transformed;
        }

        // Return the value as is, it isn't anything we need to transform.
        return $settings;
    }
}
