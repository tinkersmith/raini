<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core;

use Symfony\Component\Console\Application as ApplicationBase;

/**
 * The Symfony Console application for Raini.
 *
 * Defines the version and the CLI commands.
 */
class Application extends ApplicationBase
{

    const VERSION = '0.9.0';

    /**
     * Creates a new instance of the Raini application.
     *
     * @param Settings $settings
     */
    public function __construct(protected Settings $settings)
    {
        parent::__construct('Raini CLI', self::VERSION);
    }
}
