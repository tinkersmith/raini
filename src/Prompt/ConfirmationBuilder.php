<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Prompt;

use Symfony\Component\Console\Question\ConfirmationQuestion;
use Tinkersmith\Configurator\PropertyInterface;

/**
 * Build a confirmation prompt. Typically for bool properties.
 */
class ConfirmationBuilder extends QuestionBuilder
{

    /**
     * {@inheritdoc}
     */
    public function formatPrompt(PropertyInterface $property, mixed $default): string
    {
        $valueText = '('.($default ? 'Y/n' : 'y/N').')';

        return sprintf('<question>%s?</> %s: ', $property->label(), "<info>{$valueText}</>");
    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function __invoke(PropertyInterface $property, mixed $default = null): mixed
    {
        $prompt = $this->formatPrompt($property, $default);
        $question = new ConfirmationQuestion($prompt, $this->processDefault($default));
        $question->setValidator($this->getValidator($property, $question));

        return $question;
    }

    /**
     * {@inheritdoc}
     */
    protected function processDefault(mixed $default): mixed
    {
        return (bool) $default;
    }
}
