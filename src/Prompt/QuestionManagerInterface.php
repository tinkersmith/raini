<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Prompt;

use Tinkersmith\Configurator\ConfiguratorInterface;
use Tinkersmith\Configurator\PropertyInterface;

/**
 * Base interface for the question builder manager.
 */
interface QuestionManagerInterface
{

    /**
     * Generates a question to ask the user to get a value for a property.
     *
     * @param PropertyInterface          $property The property to build the question for.
     * @param ConfiguratorInterface|null $config   The configurator instance this property is for.
     *
     * @return Question|mixed The generated question that the prompter can ask the user, or the property value if
     *                        a value is already known.
     *
     * @throws \InvalidArgumentException
     */
    public function getBuilder(PropertyInterface $property, ?ConfiguratorInterface $config = null): mixed;

    /**
     * Adds a new question resolver callable to map properties to a builder.
     *
     * Allows services to add new mappings from properties to question builders.
     *
     * @param \Closure(PropertyInterface, ?ConfiguratorInterface):?QuestionBuilderInterface $handler  Question builder resolver callable.
     * @param int                                                                           $priority The order to call the resolver.
     */
    public function addHandler($handler, int $priority = 0): void;
}
