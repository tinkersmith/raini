<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Prompt;

use Symfony\Component\Console\Helper\Helper;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\StreamableInputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Tinkersmith\Configurator\ConfiguratorInterface;
use Tinkersmith\Configurator\DependentPropertyInterface;
use Tinkersmith\Configurator\Exception\ValueRequiredException;
use Tinkersmith\Configurator\ListType;
use Tinkersmith\Configurator\PropertyInterface;
use Tinkersmith\Configurator\TypedConfigurator;

/**
 * Implementation of a CLI prompter to populate configurator property values.
 *
 * The prompter is a CLI implementation of a tool to populate values of
 * configurators and configurator properties based on schema and known types.
 *
 * At the time of this writing, the properties and types it supports are static
 * but there are plans to extend this to use the event dispatcher to allow
 * extensions to add question factories, and more advanced property handling.
 *
 * The schema is written in a way that can enable other types of implementations
 * to populate these configurations values. One being planned is a web interface
 * for generating project configuration files.
 */
class Prompter
{

    /**
     * @param QuestionManagerInterface $questionManager
     * @param QuestionHelper           $inquisitor
     */
    public function __construct(protected QuestionManagerInterface $questionManager, protected ?QuestionHelper $inquisitor = new QuestionHelper())
    {
    }

    /**
     * Get the QuestionHelper instance.
     *
     * @return QuestionHelper The current question helper instance.
     */
    public function getAskHelper(): QuestionHelper
    {
        return $this->inquisitor;
    }

    /**
     * Sets a new question helper instance.
     *
     * @param QuestionHelper $askHelper The question helper to for the prompt to leverage.
     *
     * @return self
     */
    public function setAskHelper(QuestionHelper $askHelper): self
    {
        $this->inquisitor = $askHelper;

        return $this;
    }

    /**
     * Prompt users for value from the console to populate configuration values.
     *
     * The prompter takes a configurator instance, and interates through the
     * properties to populate values. Prompts are generated from the property
     * type
     *
     * @param InputInterface        $input  The input source
     * @param OutputInterface       $output The output to send feedback and display results to.
     * @param ConfiguratorInterface $config The configurator definition to prompt values for.
     * @param mixed[]               $values The current or default values to use.
     * @param string|null           $label  The configuration label if available.
     *
     * @return mixed[] The extracted configurations values.
     */
    public function prompt(InputInterface $input, OutputInterface $output, ConfiguratorInterface $config, array $values = [], ?string $label = null): array
    {
        $asker = $this->getAskHelper()->ask(...);
        $results = [];

        if ($label) {
            $msg = "Configure {$label}:";
            $output->writeln([
                "<info>{$msg}</>",
                '<info>'.str_repeat('-', Helper::width($msg)).'</>',
            ]);
        }

        // If the configurator is based on type information, prompt for it.
        if ($config instanceof TypedConfigurator) {
            if (empty($values[$config->typeKey])) {
                $options = $config->getOptions();
                $typePrompt = 'Select the configuration type:';
                $values[$config->typeKey] = (1 !== count($options))
                    ? $asker($input, $output, new ChoiceQuestion($typePrompt, $options))
                    : key($options);
            }
            $results[$config->typeKey] = $values[$config->typeKey];
        }

        // Prompt values based on the schema definitions.
        foreach ($config->getSchema($values) as $name => $prop) {
            if ($prop instanceof DependentPropertyInterface) {
                $prop->setState($results + $values);
            }

            $results[$name] = $this->promptProperty($input, $output, $prop, $config, $values[$name] ?? null);
        }
        $output->writeln("<info>----------------</>\n");

        return $config->extract($results);
    }

    /**
     * Prompt for the values of a single property.
     *
     * @param InputInterface             $input    The IO input for user responses and values.
     * @param OutputInterface            $output   The IO output to display prompts and messages.
     * @param PropertyInterface          $property The property to interactively prompt.
     * @param ConfiguratorInterface|null $config   The configurator that owns the property if there is one.
     * @param mixed                      $value    The current value of the property or defaults.
     *
     * @return mixed The user populated values for the property.
     */
    public function promptProperty(InputInterface $input, OutputInterface $output, PropertyInterface $property, ?ConfiguratorInterface $config = null, mixed $value = null): mixed
    {
        $asker = $this->getAskHelper()->ask(...);
        $type = $property->getType();

        if ($type instanceof ConfiguratorInterface) {
            $addQuestion = new ConfirmationQuestion(sprintf('<info>Add another %s:</> (y/N) ', $property->label()), false);

            switch ($property->getListType()) {
                case ListType::Sequence:
                    $result = [];
                    do {
                        $result[] = $this->prompt($input, $output, $type, [], $property->label());
                    } while ($asker($input, $output, $addQuestion));

                    return $result;

                case ListType::Assoc:
                    $result = [];
                    do {
                        $key = $asker($input, $output, new Question(sprintf('<info>ID for this %s:</> ', $property->label())));
                        $result[$key] = $this->prompt($input, $output, $type, [], $property->label());
                    } while ($asker($input, $output, $addQuestion));

                    return $result;

                case ListType::None:
                    return $this->prompt($input, $output, $type, $value ?? [], $property->label());

                default:
                    throw new \InvalidArgumentException('Unsuppported list type.');
            }
        }

        $builder = $this->questionManager->getBuilder($property, $config);
        $question = $builder($property, $value ?? null);

        if ($question instanceof Question) {
            $answer = $asker($input, $output, $question);

            if ($answer && ListType::None !== $property->getListType() && !$builder->supportsList()) {
                $validator = $question->getValidator();
                $inputStream = $input instanceof StreamableInputInterface ? ($input->getStream() ?? \STDIN) : \STDIN;

                $answer = [$answer];
                while (true) {
                    try {
                        $raw = trim($this->readInput($inputStream, $question));
                        if (!($tmp = $validator($raw))) {
                            break;
                        }
                        $answer[] = $tmp;
                    } catch (ValueRequiredException) {
                        break;
                    } catch (\InvalidArgumentException $e) {
                        $output->writeln('<error>'.$e->getMessage().'</>');
                        continue;
                    }
                }
            }

            return $answer;
        }

        return $question;
    }

    /**
     * Reads one or more lines of input and returns what is read.
     *
     * @param resource $inputStream The input stream resource.
     * @param Question $question    The question being asked.
     *
     * @return string|false The input stream value or FALSE if unable to get input.
     */
    protected function readInput($inputStream, Question $question): string|false
    {
        if ($question->isMultiline()) {
            $multiLineStreamReader = $this->cloneInputStream($inputStream);
            if (null === $multiLineStreamReader) {
                return false;
            }

            $ret = '';
            $cp = $this->setIOCodepage();
            while (false !== ($char = fgetc($multiLineStreamReader))) {
                if (\PHP_EOL === "{$ret}{$char}") {
                    break;
                }
                $ret .= $char;
            }
        } else {
            $cp = $this->setIOCodepage();
            $ret = fgets($inputStream, 4096);
        }

        return $this->resetIOCodepage($cp, $ret);
    }

    /**
     * Set the IO codepage if the method is available.
     *
     * @return int The IO codepage identifier.
     */
    private function setIOCodepage(): int
    {
        if (\function_exists('sapi_windows_cp_set')) {
            $cp = sapi_windows_cp_get();
            sapi_windows_cp_set(sapi_windows_cp_get('oem'));

            return $cp;
        }

        return 0;
    }

    /**
     * Sets console I/O to the specified code page and converts the user input.
     *
     * @param int          $cp    The IO codepage identifier to use to transform the input value.
     * @param string|false $input The input value to process.
     *
     * @return string|false The return value from the input stream.
     */
    private function resetIOCodepage(int $cp, string|false $input): string|false
    {
        if (0 !== $cp) {
            sapi_windows_cp_set($cp);

            if (false !== $input && '' !== $input) {
                $input = sapi_windows_cp_conv(sapi_windows_cp_get('oem'), $cp, $input);
            }
        }

        return $input;
    }

    /**
     * Clones an input stream in order to act on one instance of the same
     * stream without affecting the other instance.
     *
     * @param resource $inputStream The handler resource
     *
     * @return resource|null The cloned resource, null in case it could not be cloned
     */
    private function cloneInputStream($inputStream)
    {
        $streamMetaData = stream_get_meta_data($inputStream);
        if (empty($streamMetaData['uri'])) {
            return null;
        }

        $mode = $streamMetaData['mode'] ?: 'rb';
        $clone = fopen($streamMetaData['uri'], $mode);

        // For seekable and writable streams, add all the same data to the
        // cloned stream and then seek to the same offset.
        if ($streamMetaData['seekable'] && !\in_array($mode, ['r', 'rb', 'rt'])) {
            $offset = ftell($inputStream);
            rewind($inputStream);
            stream_copy_to_stream($inputStream, $clone);
            fseek($inputStream, $offset);
            fseek($clone, $offset);
        }

        return $clone;
    }
}
