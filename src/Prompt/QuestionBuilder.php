<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Prompt;

use Symfony\Component\Console\Question\Question;
use Tinkersmith\Configurator\PropertyInterface;

/**
 * The default question builder,
 *
 * Question builder is written for simple text entry prompts.
 */
class QuestionBuilder implements QuestionBuilderInterface
{

    /**
     * {@inheritdoc}
     */
    public function supportsList(): bool
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function formatPrompt(PropertyInterface $property, mixed $default): string
    {
        $valueText = '';
        if (!$property->isList()) {
            if (is_string($default) || is_numeric($default)) {
                $valueText = 12 < \strlen($default)
                    ? ' ('.substr($default, 0, 12).'...)'
                    : " ({$default})";
            }

            return sprintf('<question>%s</>%s: ', $property->label(), "<info>{$valueText}</>");
        }

        if (is_array($default) && $default) {
            $valueText = implode(',', $default);
            $valueText = 20 < \strlen($valueText)
                ? ' ['.substr($valueText, 0, 20).'...]'
                : " [{$valueText}]";
        }

        return sprintf("<question>%s - one value per line, empty line terminates</>%s:\n", $property->label(), "<info>{$valueText}</>");
    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function __invoke(PropertyInterface $property, mixed $default = null): mixed
    {
        $prompt = $this->formatPrompt($property, $default);

        // @todo add autocomplete type support for string types.
        $question = new Question($prompt, $this->processDefault($default));

        return $question->setValidator($this->getValidator($property, $question));
    }

    /**
     * Process the default value, for use with the prompt string.
     *
     * @param mixed $default The default value.
     *
     * @return mixed The processed data.
     */
    protected function processDefault(mixed $default): mixed
    {
        return is_string($default) || is_numeric($default) || $default instanceof \Stringable
            ? (string) $default : null;
    }

    /**
     * Get the validator callback for the property and question.
     *
     * @param PropertyInterface $property The property which is being validated.
     * @param Question          $question The question being validated.
     *
     * @return \Closure The validator callback that is compatible with questions.
     */
    protected function getValidator(PropertyInterface $property, Question $question): \Closure
    {
        // Wrap and existing question validator, and then check our property
        // validation also passes. This validation returns user input result.
        $validator = $question->getValidator();
        $propValidate = !$property->isList() || $this->supportsList()
            ? $property->validate(...)
            : $property->validateValue(...);

        // Create a closure that is compatible with the Question validator.
        return function ($value) use ($validator, $propValidate) {
            if ($value && $validator) {
                $value = $validator($value);
            }

            $rs = $propValidate($value);
            if (true !== $rs) {
                throw new \InvalidArgumentException($rs);
            }

            return $value;
        };
    }
}
