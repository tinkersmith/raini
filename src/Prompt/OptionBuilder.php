<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Prompt;

use Symfony\Component\Console\Question\ChoiceQuestion;
use Tinkersmith\Configurator\PropertyInterface;
use Tinkersmith\Configurator\Type\OptionType;

/**
 * Build option type selections.
 */
class OptionBuilder extends QuestionBuilder
{

    /**
     * {@inheritdoc}
     */
    public function supportsList(): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function __invoke(PropertyInterface $property, mixed $default = null): mixed
    {
        $type = $property->getType();

        if ($type instanceof OptionType) {
            $options = $type->getOptions();
            if (count($options) === 1) {
                // Only one possible option, return the value itselfd.
                return key($options);
            }

            $prompt = $this->formatPrompt($property, $default);
            $question = new ChoiceQuestion($prompt, $options, $this->processDefault($default));
            if ($property->isList()) {
                $question->setMultiselect(true);
            }

            // Set the validator only after setting multi-select since this
            // changes the question default validator.
            return $question->setValidator($this->getValidator($property, $question));
        }

        throw new \InvalidArgumentException('The option question builder requires an OptionType property.');
    }

    /**
     * {@inheritdoc}
     */
    public function formatPrompt(PropertyInterface $property, mixed $default): string
    {
        if ($property->isList()) {
            $valueText = empty($default) ? '' : ' <info>('.implode(', ', $default).')</>';

            return sprintf('<question>%s - comma separate values</>%s:', $property->label(), $valueText);
        }

        $valueText = $default ? ' <info>('.(string) $default.')</>' : '';

        return sprintf('<question>%s</>%s: ', $property->label(), $valueText);
    }
}
