<?php

/*
 * This file is part of the Raini package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Core\Prompt;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Tinkersmith\Configurator\ConfiguratorInterface;
use Tinkersmith\Configurator\PropertyInterface;
use Tinkersmith\Configurator\Type\BoolType;
use Tinkersmith\Configurator\Type\IntegerType;
use Tinkersmith\Configurator\Type\OptionType;
use Tinkersmith\Configurator\Type\StringType;
use Raini\Core\DependencyInjection\PrioritizedCollectorTrait;

/**
 * Manages the mapping from property types to question builders.
 *
 * The mapping for question builders can be extended by adding services that are
 * tagged as a "question_resolver". Those services will call ::addHandler to
 * add the handlers.
 */
class QuestionManager implements QuestionManagerInterface
{
    use PrioritizedCollectorTrait;

    /**
     * List of question builder resolvers.
     *
     * @var array<\Closure(PropertyInterface, ?ConfiguratorInterface): ?QuestionBuilderInterface>
     */
    protected array $resolvers = [];

    /**
     * List of initialized question builders.
     *
     * @var QuestionBuilderInterface[]
     */
    private array $builders;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(protected EventDispatcherInterface $eventDispatcher)
    {
        $this->builders = [
            'question' => new QuestionBuilder(),
            'confirm' => new ConfirmationBuilder(),
            'option' => new OptionBuilder(),
        ];
    }

    /**
     * Generates a question to ask the user to get a value for a property.
     *
     * @param PropertyInterface          $property The property to build the question for.
     * @param ConfiguratorInterface|null $config   The configurator instance being prompted for.
     *
     * @return QuestionBuilderInterface The builder to generate a question that the prompter can use to ask the user.
     */
    public function getBuilder(PropertyInterface $property, ?ConfiguratorInterface $config = null): QuestionBuilderInterface
    {
        /** @var callable $resolver */
        foreach ($this->getSortedHandlers() as $resolver) {
            if ($builder = $resolver($property, $config)) {
                return $builder;
            }
        }

        $type = $property->getType();
        switch (true) {
            case $type instanceof StringType:
            case $type instanceof IntegerType:
                return $this->builders['question'];

            case $type instanceof BoolType:
                return $this->builders['confirm'];

            case $type instanceof OptionType:
                return $this->builders['option'];
        }

        throw new \InvalidArgumentException('Unknown property type.');
    }
}
