#!/bin/bash
#
# Installable `raini` command which finds the raini script marker and
# determines which set of cmd.sh files to run.
#
# This script can be placed in the `/usr/local/bin` without the extension,
# so users will be able to just use the command `raini` globally.
#

RAINI_SCRIPT_VERSION_MAJOR=1
RAINI_SCRIPT_VERSION_MINOR=9
RAINI_SCRIPT_VERSION_BUILD=1

FOLDER=$(pwd);

# Find the marker file for the Raini scripts.
while [ -n "$FOLDER" ] && [ "${FOLDER}" != '/' ];
do
  if [ -f "$FOLDER/.raini.env" ]; then
    . "$FOLDER/.raini.env"

    if [ -x "${FOLDER}/${COMPOSER_VENDOR_DIR}/bin/raini" ]; then
      export RAINI_PROJECT_HOME="${FOLDER}"
      "${FOLDER}/${COMPOSER_VENDOR_DIR}/bin/raini" "$@"
      exit 0
    fi
  fi

  FOLDER=$(dirname "$FOLDER")
done;

# if we exited the loop, that means we didn't find the project marker file.
echo -e "Unable to find the \x1B[91m.raini.env\x1B[0m file for this project."
echo -e "Please make sure that you are calling the command from within a project folder."
echo ""
