# Raini

## Introduction
This tool was originally built to support Drupal projects and the name is a play
on quick deployments of many Drupal projects (droplets) to cloud infrastructure.

Raini is a modular project creation, setup and utilities library for 
PHP Composer based projects. The goals for Raini are to allow quick and
consistent generation of project, while supporting flexibility of projects
and long term maintainability.


## Requirements

## Installation

To start a new project


## Droplet


## Create a Project


## Core Commands

### help

### list

### create

### config

### generate

### init

### 

### yaml:lint


## Available Extensions

Though Raini was developed and tested with Drupal originally, it was planned
with extensibility and customization in mind. Below are the extension we are
aware of, and hope that this list can grow.

+ **Raini Dev**    - Code linting and testing tools and commands
+ **Raini Drupal** - Create and manage Drupal projects with Raini
+ **Raini Web-UI** - Provides local web-based UI for project configuration
